using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Threading;
using DG.Tweening;

public class InstBlockMap : MonoBehaviour
{
    public GameObject[] blocks;
    public int Stage1;
    public int Stage2;
    public int Stage3;
    public int Stage4;
    public int StageAfter4;

    List<GameObject> myBlocks = new List<GameObject>();
    private int myBlocksCount;
    private float selfTimer;
    private float startTimer;
    private bool initCheck = false;
    private float speed;
    private int randomCount;
    private float[] stepIndex = new float[10000]; // 1 스텝 가는 인덱스
    private int[] compCheck = new int[36];
    //    private bool 
    private int maxY;
    private bool comp;

    private float instTime;

    // test 변수
    private float max = 1;

    private int gridMax = 9;

    public Transform lastFloor;

    // Use this for initialization
    void Start()
    {
        startTimer = Time.time;
        selfTimer = Time.time + 0.7f;
        maxY = 4;
        for (int i = 0; i < stepIndex.Length; ++i)
            stepIndex[i] = 0;
        instTime = 1.0f;
        StartCoroutine("MyUpdate");
    }

    IEnumerator MyUpdate()
    {
        while (true)
        {
            if (ChallengeGameManager.Instance.failCheck == false)
            {
                if (selfTimer - startTimer < 20) // 20초까진 2개
                {
                    gridMax = 9;
                    ChallengeGameManager.Instance.nowStage = 1;
                    if (selfTimer < Time.time)
                    {
                        comp = false;
                        myBlocks.Clear();



                        // floor로 관리 X 
                        GameObject tempParent = new GameObject();
                        tempParent.transform.position = Vector3.zero;
                        tempParent.name = "Floor";
                        ChallengeGameManager.Instance.floor.Add(tempParent);
                        lastFloor = tempParent.transform;

                        Stage1 = Random.Range(0, 2);

                        blocks[Stage1].transform.position = new Vector3(Random.Range(0, 3), 0, Random.Range(0, 3));


                        GameObject tempObj = Instantiate<Object>(blocks[Stage1]) as GameObject;
                        myBlocks.Add(tempObj);
                        ///////////////// floorfind 맞추기 
                        ChallengeGameManager.Instance.Init();

                        for (int i = 0; i < (int)LengthXZ.Max; ++i)
                        {
                            for (int j = 0; j < (int)LengthXZ.Max; ++j)
                            {
                                if (ChallengeGameManager.Instance.floorFind[i, j] >= 10)
                                {
                                    ChallengeGameManager.Instance.FailUI.SetActive(true);
                                    ChallengeGameManager.Instance.failCheck = true;

                                }
                            }
                        }
                        //FloorSort();
                        FloorEnter();
                        selfTimer = Time.time + instTime;
                    }
                }


                else if (selfTimer - startTimer >= 20 && selfTimer - startTimer < 40)
                {
                    gridMax = 16;
                    ChallengeGameManager.Instance.nowStage = 2;

                    if (selfTimer < Time.time)
                    {
                        comp = false;
                        myBlocks.Clear();



                        // floor로 관리 X 
                        GameObject tempParent = new GameObject();
                        tempParent.transform.position = Vector3.zero;
                        tempParent.name = "Floor";
                        ChallengeGameManager.Instance.floor.Add(tempParent);
                        lastFloor = tempParent.transform;

                        Stage2 = Random.Range(0, 3);


                        blocks[Stage2].transform.position = new Vector3(Random.Range(0, 4), 0, Random.Range(0, 4));

                        GameObject tempObj = Instantiate<Object>(blocks[Stage2]) as GameObject;
                        myBlocks.Add(tempObj);
                        ///////////////// floorfind 맞추기 
                        ChallengeGameManager.Instance.Init();
                        for (int i = 0; i < (int)LengthXZ.Max; ++i)
                        {
                            for (int j = 0; j < (int)LengthXZ.Max; ++j)
                            {
                                if (ChallengeGameManager.Instance.floorFind[i, j] >= 10)
                                {
                                    ChallengeGameManager.Instance.FailUI.SetActive(true);
                                    ChallengeGameManager.Instance.failCheck = true;
                                }
                            }
                        }
                        //FloorSort();
                        FloorEnter();
                        //카메라 사이즈 up
                        //ChallengeGameManager.Instance.zlengthFind = 4;
                        //ChallengeGameManager.Instance.xlengthFind = 4;
                        selfTimer = Time.time + instTime - 0.1f;
                    }
                }
                else if (selfTimer - startTimer >= 40 && selfTimer - startTimer < 60)
                {
                    gridMax = 25;
                    ChallengeGameManager.Instance.nowStage = 3;

                    if (selfTimer < Time.time)
                    {
                        comp = false;
                        myBlocks.Clear();



                        // floor로 관리 X 
                        GameObject tempParent = new GameObject();
                        tempParent.transform.position = Vector3.zero;
                        tempParent.name = "Floor";
                        ChallengeGameManager.Instance.floor.Add(tempParent);
                        lastFloor = tempParent.transform;

                        Stage3 = Random.Range(0, 4);


                        blocks[Stage3].transform.position = new Vector3(Random.Range(0, 5), 0, Random.Range(0, 5));

                        GameObject tempObj = Instantiate<Object>(blocks[Stage3]) as GameObject;
                        myBlocks.Add(tempObj);
                        ///////////////// floorfind 맞추기 
                        ChallengeGameManager.Instance.Init();
                        for (int i = 0; i < (int)LengthXZ.Max; ++i)
                        {
                            for (int j = 0; j < (int)LengthXZ.Max; ++j)
                            {
                                if (ChallengeGameManager.Instance.floorFind[i, j] >= 10)
                                {
                                    ChallengeGameManager.Instance.FailUI.SetActive(true);
                                    ChallengeGameManager.Instance.failCheck = true;


                                }
                            }
                        }
                        //FloorSort();
                        FloorEnter();
                        //카메라 사이즈 up
                        //ChallengeGameManager.Instance.zlengthFind = 5;
                        //ChallengeGameManager.Instance.xlengthFind = 5;
                        selfTimer = Time.time + instTime - 0.2f;
                    }
                }
                else if (selfTimer - startTimer >= 60 && selfTimer - startTimer < 80)
                {
                    gridMax = 36;
                    ChallengeGameManager.Instance.nowStage = 4;

                    Debug.Log("60~80");
                    if (selfTimer < Time.time)
                    {
                        comp = false;
                        myBlocks.Clear();



                        // floor로 관리 X 
                        GameObject tempParent = new GameObject();
                        tempParent.transform.position = Vector3.zero;
                        tempParent.name = "Floor";
                        ChallengeGameManager.Instance.floor.Add(tempParent);
                        lastFloor = tempParent.transform;

                        Stage4 = Random.Range(0, 5);


                        blocks[Stage4].transform.position = new Vector3(Random.Range(0, 6), 0, Random.Range(0, 6));

                        GameObject tempObj = Instantiate<Object>(blocks[Stage4]) as GameObject;
                        myBlocks.Add(tempObj);
                        ///////////////// floorfind 맞추기 
                        ChallengeGameManager.Instance.Init();
                        for (int i = 0; i < (int)LengthXZ.Max; ++i)
                        {
                            for (int j = 0; j < (int)LengthXZ.Max; ++j)
                            {
                                if (ChallengeGameManager.Instance.floorFind[i, j] >= 10)
                                {
                                    ChallengeGameManager.Instance.FailUI.SetActive(true);
                                    ChallengeGameManager.Instance.failCheck = true;
                                }
                            }
                        }
                        //FloorSort();
                        FloorEnter();
                        //카메라 사이즈 up
                        //ChallengeGameManager.Instance.zlengthFind = 6;
                        //ChallengeGameManager.Instance.xlengthFind = 6;
                        selfTimer = Time.time + instTime - 0.3f;
                    }
                }
                else if (selfTimer - startTimer >= 80 && selfTimer - startTimer < 200)
                {
                    gridMax = 49;
                    ChallengeGameManager.Instance.nowStage = 5;

                    if (selfTimer < Time.time)
                    {
                        comp = false;
                        myBlocks.Clear();



                        // floor로 관리 X 
                        GameObject tempParent = new GameObject();
                        tempParent.transform.position = Vector3.zero;
                        tempParent.name = "Floor";
                        ChallengeGameManager.Instance.floor.Add(tempParent);
                        lastFloor = tempParent.transform;

                        StageAfter4 = Random.Range(0, 6);


                        blocks[StageAfter4].transform.position = new Vector3(Random.Range(0, 7), 0, Random.Range(0, 7));

                        GameObject tempObj = Instantiate<Object>(blocks[StageAfter4]) as GameObject;
                        myBlocks.Add(tempObj);
                        ///////////////// floorfind 맞추기 
                        ChallengeGameManager.Instance.Init();
                        for (int i = 0; i < (int)LengthXZ.Max; ++i)
                        {
                            for (int j = 0; j < (int)LengthXZ.Max; ++j)
                            {
                                if (ChallengeGameManager.Instance.floorFind[i, j] >= 10)
                                {
                                    ChallengeGameManager.Instance.FailUI.SetActive(true);
                                    ChallengeGameManager.Instance.failCheck = true;
                                }
                            }
                        }
                        // FloorSort();
                        FloorEnter();
                        //카메라 사이즈 up
                        //ChallengeGameManager.Instance.zlengthFind = 7;
                        //ChallengeGameManager.Instance.xlengthFind = 7;
                        selfTimer = Time.time + instTime - 0.4f;
                    }
                }

            }             //adfadfas
            yield return true;
        }
    }

    void FloorSort()
    {
        for (int i = 0; i < ChallengeGameManager.Instance.floor.Count; ++i) //밑에서 하나 채우기     동시에 움직이면 요거 무시
        {
            if (ChallengeGameManager.Instance.floor[i].transform.position.y < 9 - i)
            {
                speed = 20f;
                float dist = Mathf.Abs(ChallengeGameManager.Instance.floor[i].transform.position.y - (9 - i));
                float duration = dist / speed;
                ChallengeGameManager.Instance.floor[i].transform.DOMoveY(9f - i, duration).SetEase(Ease.Linear);
            }
        }
    }
    void FloorEnter()
    {
        //print("floorFind " + ChallengeGameManager.Instance.floorFind);
        //if (floor[i].transform.position.y < 10 - ChallengeGameManager.Instance.floorFind + max)
        //if (ChallengeGameManager.Instance.floor[ChallengeGameManager.Instance.floorFind - 1].transform.position.y < 11f - ChallengeGameManager.Instance.floorFind)
        //{
        float duration;
        float temp = 0;
        speed = 20f;
        for (int j = 0; j < myBlocks.Count; ++j)
        {
        float dist = Mathf.Abs(ChallengeGameManager.Instance.floor[ChallengeGameManager.Instance.floorFind[(int)myBlocks[j].transform.position.x,(int)myBlocks[j].transform.position.z] - 1].transform.localPosition.y - (10f - ChallengeGameManager.Instance.floorFind[(int)myBlocks[j].transform.position.x, (int)myBlocks[j].transform.position.z]));
            GameObject tempBlock  = myBlocks[j];
            speed -= 2f;
            duration = dist / speed;
            if (duration > temp)
                temp = duration;
           
            tempBlock.transform.SetParent(lastFloor);
            tempBlock.transform.DOMoveY(10f - ChallengeGameManager.Instance.floorFind[(int)myBlocks[j].transform.position.x, (int)myBlocks[j].transform.position.z], duration).SetEase(Ease.Linear);//.OnComplete(() => EnterComplete(tempBlock));
        }

        StartCoroutine("CheckComplete", temp);
        //}
    }

    IEnumerator CheckComplete(float duration)
    {
        yield return new WaitForSeconds(duration);
        //fasdfads

        comp = true;
        myBlocksCount++;
        ChallengeGameManager.Instance.Init();
 //       Debug.Log("asdasd");
    }


    void EnterComplete(GameObject block)
    {
        //ChallengeGameManager.Instance.Init();
        
        comp = true;
        block.transform.SetParent(lastFloor);
        myBlocksCount++;
        ChallengeGameManager.Instance.Init();
        //Debug.Log("*******call*******");
        //Debug.Log("blockY =" + Mathf.Round(block.transform.position.y));

    }

}