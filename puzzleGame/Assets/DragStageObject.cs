﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DragStageObject : MonoBehaviour
{
    Vector3 curScreenSpace;
    Vector3 curPosition;


    private bool triggerCheck = false;
    private GameManager manager;
    private BlockSave bs;
    private float posY = 0;
    private float rotateAngle = 0;

    private int triggerCheckIndex = 0;
    private Transform _transform;


    // audio sound source
    public AudioClip up;
    public AudioClip down;
    void Start()
    {

        manager = GameManager.Instance;
        bs = this.GetComponent<BlockSave>();
        _transform = this.transform;
        StartCoroutine("updateRoutine");
    }

    IEnumerator updateRoutine()
    {
        while (true)
        {

            yield return null;
        }
    }
        IEnumerator BlockClick()
    {
        if ((GameManager.Instance.failCheck == false) && retry.Instance.pause == false)
        {
            AudioSource.PlayClipAtPoint(down, new Vector3(0, 0, 0));

            print("Enter");
            // Vector3 scrSpace = Camera.main.WorldToScreenPoint(transform.position);
            Vector3 offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));

            while (Input.GetMouseButton(0))
            {
                GameManager.Instance.DragOn = true;
                GameManager.Instance.clickedBlock = this.gameObject;
                GameManager.Instance.Init();
                posY = GameManager.Instance.floorFind - 1.0f;
                curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z);
                curPosition = Camera.main.ScreenToWorldPoint(curScreenSpace) + offset;
                // Debug.Log(curPosition);//Mathf.Round(curPosition.y) * 0

                if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 0)
                    transform.position = new Vector3(curPosition.x + ((curPosition.y - posY) * GameManager.Instance.cosX), posY, curPosition.z + ((curPosition.y - posY) * GameManager.Instance.cosZ));
                else if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 90)
                    transform.position = new Vector3(curPosition.x + ((curPosition.y - posY) * GameManager.Instance.cosX), posY, curPosition.z - ((curPosition.y - posY) * GameManager.Instance.cosZ));
                else if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 180)
                    transform.position = new Vector3(curPosition.x - ((curPosition.y - posY) * GameManager.Instance.cosX), posY, curPosition.z - ((curPosition.y - posY) * GameManager.Instance.cosZ));
                else if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 270)
                    transform.position = new Vector3(curPosition.x - ((curPosition.y - posY) * GameManager.Instance.cosX), posY, curPosition.z + ((curPosition.y - posY) * GameManager.Instance.cosZ));

                //transform.position = new Vector3(curPosition.x + GameManager.Instance.cosX * (curPosition.y), posY, curPosition.z + GameManager.Instance.cosZ * (curPosition.y));

                //if (manager.blocks[(int)Mathf.Round(transform.position.x), (int)Mathf.Round(transform.position.y), (int)Mathf.Round(transform.position.z)] != BlockType.Empty)
                //{
                //   // transform.Translate(0,1,0);
                //}
                yield return null;

            }
            AudioSource.PlayClipAtPoint(up, new Vector3(0, 0, 0));
            _transform.DOScale(_transform.localScale * 1.2f, 0.6f).SetLoops(-1, LoopType.Yoyo);
            if ((int)Mathf.Round(transform.position.x) <= GameManager.Instance.maxSize - 1 && (int)Mathf.Round(transform.position.z) <= GameManager.Instance.maxSize - 1 &&
                (int)Mathf.Round(transform.position.x) >= 0 && (int)Mathf.Round(transform.position.z) >= 0)
            {
                Debug.Log("범위초과");

                if (manager.blocks[(int)Mathf.Round(transform.position.x), (int)posY, (int)Mathf.Round(transform.position.z)] != BlockType.Empty)
                {

                    if (gameObject.CompareTag("GrassBlock"))
                    {
                        GameManager.Instance.blockCounts[0]++;
                    }
                    else if (gameObject.CompareTag("JungleBlock"))
                    {
                        print(gameObject.name);
                        GameManager.Instance.blockCounts[1]++;
                    }
                    else if (gameObject.CompareTag("DesertBlock"))
                    {
                        GameManager.Instance.blockCounts[2]++;
                    }
                    else if (gameObject.CompareTag("LavaBlock"))
                    {
                        GameManager.Instance.blockCounts[3]++;
                    }
                    GameManager.Instance.DragOn = false;
                    GameManager.Instance.clickedBlock = null;
                    Destroy(gameObject);
                    //  Debug.Log((int)Mathf.Round(transform.position.x) + " , " + (int)Mathf.Round(transform.position.z));
                    //  Debug.Log(manager.blocks[(int)Mathf.Round(transform.position.x), (int)Mathf.Round(transform.position.z)] +"empty아님!!");
                    //                transform.position = bs.posSave;

                }
                if (manager.blocks[(int)Mathf.Round(transform.position.x), (int)posY, (int)Mathf.Round(transform.position.z)] == BlockType.Empty)
                {
                    //                Mathf.Round(curPosition.y) * 0
                    if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 0)
                        transform.position = new Vector3(Mathf.Round(curPosition.x + ((curPosition.y - posY) * GameManager.Instance.cosX)), posY, Mathf.Round(curPosition.z + ((curPosition.y - posY) * GameManager.Instance.cosZ)));
                    else if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 90)
                        transform.position = new Vector3(Mathf.Round(curPosition.x + ((curPosition.y - posY) * GameManager.Instance.cosX)), posY, Mathf.Round(curPosition.z - ((curPosition.y - posY) * GameManager.Instance.cosZ)));
                    else if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 180)
                        transform.position = new Vector3(Mathf.Round(curPosition.x - ((curPosition.y - posY) * GameManager.Instance.cosX)), posY, Mathf.Round(curPosition.z - ((curPosition.y - posY) * GameManager.Instance.cosZ)));
                    else if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 270)
                        transform.position = new Vector3(Mathf.Round(curPosition.x - ((curPosition.y - posY) * GameManager.Instance.cosX)), posY, Mathf.Round(curPosition.z + ((curPosition.y - posY) * GameManager.Instance.cosZ)));


                    //if (GameManager.Instance.blockUI.Contains(GameManager.Instance.selectBlockUI))
                    //{
                    //    GameManager.Instance.blockUI.Remove(GameManager.Instance.selectBlockUI);
                    //}
                    //Destroy(GameManager.Instance.selectBlockUI);
                    //GameManager.Instance.blockbutton_count--;

                }
                GameManager.Instance.DragOn = false;
                GameManager.Instance.clickedBlock = null;
                GameManager.Instance.Init();
            }
            else
            {
                if (gameObject.CompareTag("GrassBlock"))
                {
                    GameManager.Instance.blockCounts[0]++;
                }
                else if (gameObject.CompareTag("JungleBlock"))
                {
                    print(gameObject.name);
                    GameManager.Instance.blockCounts[1]++;
                }
                else if (gameObject.CompareTag("DesertBlock"))
                {
                    GameManager.Instance.blockCounts[2]++;
                }
                else if (gameObject.CompareTag("LavaBlock"))
                {
                    GameManager.Instance.blockCounts[3]++;
                }
                GameManager.Instance.DragOn = false;
                GameManager.Instance.clickedBlock = null;
                Destroy(gameObject);
            }
            GameManager.Instance.selectBlockUI = null;

            //try {
            //    if (manager.blocks[(int)Mathf.Round(transform.position.x), (int)Mathf.Round(transform.position.z)] != BlockType.Empty)
            //    {
            //        transform.position = bs.posSave;
            //    }
            //}
            //catch (System.IndexOutOfRangeException e)  // CS0168
            //{
            //    System.Console.WriteLine(e.Message);
            //    // Set IndexOutOfRangeException to the new exception's InnerException.
            //    transform.position = bs.posSave;

            //    throw new System.ArgumentOutOfRangeException("index parameter is out of range.", e);
            //}
        }
    }
    IEnumerator OnMouseDown()
    {
        _transform.DOKill();
        _transform.localScale = new Vector3(2f, 2f, 2f);

        if ((GameManager.Instance.failCheck == false) && retry.Instance.pause == false)
        {
            AudioSource.PlayClipAtPoint(down, new Vector3(0, 0, 0));

            // Vector3 scrSpace = Camera.main.WorldToScreenPoint(transform.position);
            Vector3 offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));

            while (Input.GetMouseButton(0))
            {
                GameManager.Instance.DragOn = true;
                GameManager.Instance.clickedBlock = this.gameObject; 
                GameManager.Instance.Init();
                posY = GameManager.Instance.floorFind - 1.0f;
                curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z);
                curPosition = Camera.main.ScreenToWorldPoint(curScreenSpace) + offset;
                // Debug.Log(curPosition);//Mathf.Round(curPosition.y) * 0

                if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 0)
                    transform.position = new Vector3(curPosition.x + ((curPosition.y - posY) * GameManager.Instance.cosX), posY, curPosition.z + ((curPosition.y - posY) * GameManager.Instance.cosZ));
                else if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 90)
                    transform.position = new Vector3(curPosition.x + ((curPosition.y - posY) * GameManager.Instance.cosX), posY, curPosition.z - ((curPosition.y - posY) * GameManager.Instance.cosZ));
                else if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 180)
                    transform.position = new Vector3(curPosition.x - ((curPosition.y - posY) * GameManager.Instance.cosX), posY, curPosition.z - ((curPosition.y - posY) * GameManager.Instance.cosZ));
                else if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 270)
                    transform.position = new Vector3(curPosition.x - ((curPosition.y - posY) * GameManager.Instance.cosX), posY, curPosition.z + ((curPosition.y - posY) * GameManager.Instance.cosZ));
                //transform.position = new Vector3(curPosition.x + GameManager.Instance.cosX * (curPosition.y), posY, curPosition.z + GameManager.Instance.cosZ * (curPosition.y));

                //if (manager.blocks[(int)Mathf.Round(transform.position.x), (int)Mathf.Round(transform.position.y), (int)Mathf.Round(transform.position.z)] != BlockType.Empty)
                //{
                //   // transform.Translate(0,1,0);
                //}
                yield return null;

            }
            AudioSource.PlayClipAtPoint(up, new Vector3(0, 0, 0));
            _transform.DOScale(_transform.localScale * 1.2f, 0.6f).SetLoops(-1, LoopType.Yoyo);

            if ((int)Mathf.Round(transform.position.x) <= GameManager.Instance.maxSize - 1 && (int)Mathf.Round(transform.position.z) <= GameManager.Instance.maxSize - 1 &&
                (int)Mathf.Round(transform.position.x) >= 0 && (int)Mathf.Round(transform.position.z) >= 0)
            {
                if (manager.blocks[(int)Mathf.Round(transform.position.x), (int)posY, (int)Mathf.Round(transform.position.z)] != BlockType.Empty)
                {
                    if (gameObject.CompareTag("GrassBlock"))
                    {
                        GameManager.Instance.blockCounts[0]++;
                    }
                    else if (gameObject.CompareTag("JungleBlock"))
                    {
                        print(gameObject.name);
                        GameManager.Instance.blockCounts[1]++;
                    }
                    else if (gameObject.CompareTag("DesertBlock"))
                    {
                        GameManager.Instance.blockCounts[2]++;
                    }
                    else if (gameObject.CompareTag("LavaBlock"))
                    {
                        GameManager.Instance.blockCounts[3]++;
                    }
                    GameManager.Instance.DragOn = false;
                    GameManager.Instance.clickedBlock = null;
                    Destroy(gameObject);
                    //  Debug.Log((int)Mathf.Round(transform.position.x) + " , " + (int)Mathf.Round(transform.position.z));
                    //  Debug.Log(manager.blocks[(int)Mathf.Round(transform.position.x), (int)Mathf.Round(transform.position.z)] +"empty아님!!");
                    //                transform.position = bs.posSave;

                }
                if (manager.blocks[(int)Mathf.Round(transform.position.x), (int)posY, (int)Mathf.Round(transform.position.z)] == BlockType.Empty)
                {
                    //                Mathf.Round(curPosition.y) * 0
                    if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 0)
                        transform.position = new Vector3(Mathf.Round(curPosition.x + ((curPosition.y - posY) * GameManager.Instance.cosX)), posY, Mathf.Round(curPosition.z + ((curPosition.y - posY) * GameManager.Instance.cosZ)));
                    else if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 90)
                        transform.position = new Vector3(Mathf.Round(curPosition.x + ((curPosition.y - posY) * GameManager.Instance.cosX)), posY, Mathf.Round(curPosition.z - ((curPosition.y - posY) * GameManager.Instance.cosZ)));
                    else if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 180)
                        transform.position = new Vector3(Mathf.Round(curPosition.x - ((curPosition.y - posY) * GameManager.Instance.cosX)), posY, Mathf.Round(curPosition.z - ((curPosition.y - posY) * GameManager.Instance.cosZ)));
                    else if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 270)
                        transform.position = new Vector3(Mathf.Round(curPosition.x - ((curPosition.y - posY) * GameManager.Instance.cosX)), posY, Mathf.Round(curPosition.z + ((curPosition.y - posY) * GameManager.Instance.cosZ)));

                    //if (GameManager.Instance.blockUI.Contains(GameManager.Instance.selectBlockUI))
                    //{
                    //    GameManager.Instance.blockUI.Remove(GameManager.Instance.selectBlockUI);
                    //}
                    //Destroy(GameManager.Instance.selectBlockUI);
                    //GameManager.Instance.blockbutton_count--;
                }

                GameManager.Instance.DragOn = false;
                GameManager.Instance.clickedBlock = null;
                GameManager.Instance.Init();

            }
            else
            {
                if (gameObject.CompareTag("GrassBlock"))
                {
                    GameManager.Instance.blockCounts[0]++;
                }
                else if (gameObject.CompareTag("JungleBlock"))
                {
                    print(gameObject.name);
                    GameManager.Instance.blockCounts[1]++;
                }
                else if (gameObject.CompareTag("DesertBlock"))
                {
                    GameManager.Instance.blockCounts[2]++;
                }
                else if (gameObject.CompareTag("LavaBlock"))
                {
                    GameManager.Instance.blockCounts[3]++;
                }
                GameManager.Instance.DragOn = false;
                GameManager.Instance.clickedBlock = null;
                Destroy(gameObject);
            }
            GameManager.Instance.selectBlockUI = null;
        }
    }
}