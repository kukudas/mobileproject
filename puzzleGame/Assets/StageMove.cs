﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class StageMove : MonoBehaviour {

    private Transform _transform;
	// Use this for initialization
    void Awake()
    {
        _transform = this.transform;
    }

	void Start () {
        _transform.DOMoveY(0, 3f).SetEase(Ease.OutQuint).OnComplete(()=>{ _transform.position = Vector3.zero; GameManager.Instance.Init(); });
    }
}




