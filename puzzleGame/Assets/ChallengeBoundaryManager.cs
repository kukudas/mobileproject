﻿using UnityEngine;
using System.Collections;

public class ChallengeBoundaryManager : MonoBehaviour {
    public GameObject[] boundary_surface;
    private int temp;
    
	// Use this for initialization
	void Start()
    {
        temp = ChallengeGameManager.Instance.nowStage;
    }
	// Update is called once per frame
	void Update () {         
        if (ChallengeGameManager.Instance.changeStage == true)
        {
            for (int i = 0; i < 5; ++i)
            {
                boundary_surface[i].SetActive(false);
            }
            boundary_surface[ChallengeGameManager.Instance.nowStage - 1].SetActive(true);
            ChallengeGameManager.Instance.changeStage = false;
        }
    }
}
