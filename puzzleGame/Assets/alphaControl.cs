﻿using UnityEngine;
using System.Collections;

public class alphaControl : MonoBehaviour
{
    public Renderer rend;
    public float alphaColor;
    private Vector3 last;
    private float alphaMax;
    private float alphaNow;

    public GameObject block;
    Transform _transform;
    string sceneName;
    // Use this for initialization
    void Start()
    {
        sceneName = Application.loadedLevelName;
        _transform = this.transform;
        last = new Vector3(0, 0, 0);
        alphaColor = 0f;
        rend = this.GetComponent<Renderer>();
        alphaMax = 200f / 255f;
        StartCoroutine("updateRoutine");
    }

    IEnumerator updateRoutine()
    {
        while (true)
        {

            rend.material.color = new Color(rend.material.color.r, rend.material.color.g, rend.material.color.b, alphaColor);
            if (alphaColor < alphaMax)
            {
                alphaColor += 0.007f;
            }
            else
            {
                if (sceneName == "Challenge")
                {
                    try
                    {
                        block.transform.position = new Vector3(_transform.position.x, _transform.position.y, _transform.position.z);
                        Instantiate<Object>(block);
                        ChallengeGameManager.Instance.Init();
                    }
                    catch
                    {
                        print("null");
                    }
                    gameObject.SetActive(false);
                }
            }
            yield return null;
        }
    }
}
