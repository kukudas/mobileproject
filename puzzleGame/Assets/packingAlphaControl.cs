﻿using UnityEngine;
using System.Collections;

public class packingAlphaControl : MonoBehaviour {
    public Renderer rend;
    private Material _material;
    public float alphaColor;
    private Vector3 last;
    private BlockSave bs;
    // Use this for initialization
    void Awake()
    {
        bs = this.GetComponent<BlockSave>();
        last = new Vector3(0, 0, 0);
        alphaColor = 74.0f / 255f;
        rend = GetComponent<Renderer>();
        _material = rend.material;
    }

    void Start()
    {

        StartCoroutine("updateRoutine");
  
    }

    // Update is called once per frame
    IEnumerator updateRoutine()
    {
        while (true)
        {
            if (bs.deathCount == 1)
            {
                _material.color = new Color(0, 0, 0, alphaColor);
            }

            yield return null;
        }
    }
}
