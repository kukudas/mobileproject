﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class BlockSave2 : MonoBehaviour
{
    public Vector3 pos;
    public Vector3 posSave;

    private ChallengeGameManager manager;

    public int deathCount;

    public AudioClip destory;

    private float posY;
    private float nowPosY;
    private float selfTimer;
    private float startTimer;
    private Transform _transform;

    public float transLength;
    private bool upCheck;
    private float timePer;
    
    // Use this for initialization
    void Start()
    {
        manager = ChallengeGameManager.Instance;
        upCheck = false;
        _transform = this.transform;
        pos = _transform.position;
        posSave = pos;
        posY = 0;
        nowPosY = posY;
        startTimer = Time.time;
        selfTimer = Time.time + 0.7f;
        StartCoroutine("updateRoutine");

        if(_transform.position.y<0)
            _transform.DOMoveY(0, 1f).SetEase(Ease.InSine).OnComplete(() => _transform.position = new Vector3(_transform.position.x,0,_transform.position.z));
    }

    IEnumerator updateRoutine()
    {
        while (true)
        {
            if (retry.Instance.pause == false)
            {
                if (_transform.position.y >= 0)
                {
                    if (_transform.position.y < 9)
                    {
                        if (ChallengeGameManager.Instance.blocks[(int)_transform.position.x, (int)_transform.position.y + 1, (int)_transform.position.z] == BlockType2.Empty)
                        {
                            if (transLength < ChallengeGameManager.Instance.blockSpeed * 10)
                            {
                                if (upCheck == false)
                                {
                                    ChallengeGameManager.Instance.Init();
                                }
                                upCheck = true;
                                transLength += ChallengeGameManager.Instance.blockSpeed / 10.0f;
                                _transform.Translate(0, ChallengeGameManager.Instance.blockSpeed / 10.0f, 0);
                            }
                            else
                            {
                                upCheck = false;
                                transLength = 0;
                                _transform.position = new Vector3(_transform.position.x, Mathf.Round(_transform.position.y), _transform.position.z);
                            }
                            //_transform.DOMoveY(_transform.position.y + 1.0f, 0.1f);//.OnComplete(() => _transform.position = new Vector3(_transform.position.x, (int)_transform.position.y, _transform.position.z));
                            //.OnComplete(_transform.position = new Vector3(Mathf.Round(_transform.position.x),);
                        }

                    }
                }
            }
            if (_transform.position.y > 9)
            {
                _transform.position = new Vector3(_transform.position.x, 9f, _transform.position.z);
                pos = new Vector3(_transform.position.x, 9f, _transform.position.z);
            }
            //0까지 가라 ... 

            //if (_transform.position.y < 7)
            //{
            //    _transform.position = new Vector3(_transform.position.x, _transform.position.y + 0.01f, _transform.position.z);
            //    if (_transform.position.y >= 7)
            //        _transform.position = new Vector3(_transform.position.x, Mathf.Round(_transform.position.y), _transform.position.z);
            //}
            if (ChallengeGameManager.Instance.item[0] == true)
            {
                if (_transform.tag == "GrassBlock" || _transform.tag == "Item1")
                    this.deathCount = 0;
            }
            else if (ChallengeGameManager.Instance.item[1] == true)
            {
                if (_transform.tag == "DesertBlock" || _transform.tag == "Item2")
                    this.deathCount = 0;
            }
            else if (ChallengeGameManager.Instance.item[2] == true)
            {
                if (_transform.tag == "JungleBlock" || _transform.tag == "Item3")
                    this.deathCount = 0;
            }
            else if (ChallengeGameManager.Instance.item[3] == true)
            {
                if (_transform.tag == "LavaBlock" || _transform.tag == "Item4")
                    this.deathCount = 0;
            }
            else if (ChallengeGameManager.Instance.item[4] == true)
            {
                if (_transform.tag == "IceBlock" || _transform.tag == "Item5")
                    this.deathCount = 0;
            }
            else if (ChallengeGameManager.Instance.item[5] == true)
            {
                if (ChallengeGameManager.Instance.tempObject.transform.position.y == _transform.position.y)
                {
                    if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x)
                        this.deathCount = 0;
                }
                //X 블럭들 파괴 
            }
            else if (ChallengeGameManager.Instance.item[6] == true)
            {
                if (ChallengeGameManager.Instance.tempObject.transform.position.y == _transform.position.y)
                {
                    if (ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z)
                        this.deathCount = 0;
                }
                //Z 블럭들 파괴 
            }
            else if (ChallengeGameManager.Instance.item[7] == true)
            {
                if (ChallengeGameManager.Instance.tempObject.transform.position.y == _transform.position.y)
                    this.deathCount = 0;
                //같은 층 블럭들 파괴
            }
            //ㅁ 모양 파괴 * 1
            else if (ChallengeGameManager.Instance.item[8] == true)
            {
                if (_transform.tag == "Item9")
                    this.deathCount = 0;
                if (ChallengeGameManager.Instance.tempObject.transform.position.y == _transform.position.y)
                {
                    if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x)
                    {
                        if (ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z + 1 ||
                            ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z - 1)
                        {
                            this.deathCount = 0;

                        }
                    }
                    if (ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z)
                    {
                        if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x + 1 ||
                            ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x - 1)
                        {
                            this.deathCount = 0;
                        }
                    }
                    if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x + 1 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z + 1)
                    {
                        this.deathCount = 0;
                    }
                    else if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x - 1 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z + 1)
                    {
                        this.deathCount = 0;
                    }
                    else if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x + 1 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z - 1)
                    {
                        this.deathCount = 0;

                    }
                    else if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x - 1 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z - 1)
                    {
                        this.deathCount = 0;
                    }
                }
            }
            else if (ChallengeGameManager.Instance.item[9] == true)
            {
                if (_transform.tag == "Item10")
                    this.deathCount = 0;
                if (ChallengeGameManager.Instance.tempObject.transform.position.y == _transform.position.y)
                {
                    if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x)
                    {
                        if (ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z + 1 ||
                            ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z - 1 ||
                            ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z + 2 ||
                            ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z - 2)
                        {
                            this.deathCount = 0;
                        }
                    }
                    if (ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z)
                    {
                        if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x + 1 ||
                            ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x - 1 ||
                            ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x + 2 ||
                            ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x - 2)
                        {
                            this.deathCount = 0;
                        }
                    }
                    if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x + 1 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z + 1)
                    {
                        this.deathCount = 0;
                    }
                    else if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x - 1 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z + 1)
                    {
                        this.deathCount = 0;
                    }
                    else if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x + 1 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z - 1)
                    {
                        this.deathCount = 0;

                    }
                    else if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x - 1 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z - 1)
                    {
                        this.deathCount = 0;
                    }


                    else if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x + 2 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z + 1)
                    {
                        this.deathCount = 0;
                    }
                    else if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x + 2 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z + 2)
                    {
                        this.deathCount = 0;
                    }
                    else if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x + 1 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z + 2)
                    {
                        this.deathCount = 0;
                    }


                    else if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x + 2 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z - 1)
                    {
                        this.deathCount = 0;
                    }
                    else if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x + 2 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z - 2)
                    {
                        this.deathCount = 0;
                    }
                    else if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x + 1 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z - 2)
                    {
                        this.deathCount = 0;
                    }

                    else if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x - 1 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z - 2)
                    {
                        this.deathCount = 0;
                    }
                    else if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x - 2 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z - 2)
                    {
                        this.deathCount = 0;
                    }
                    else if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x - 2 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z - 1)
                    {
                        this.deathCount = 0;
                    }

                    else if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x - 1 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z + 2)
                    {
                        this.deathCount = 0;
                    }
                    else if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x - 2 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z + 2)
                    {
                        this.deathCount = 0;
                    }
                    else if (ChallengeGameManager.Instance.tempObject.transform.position.x == _transform.position.x - 2 && ChallengeGameManager.Instance.tempObject.transform.position.z == _transform.position.z + 1)
                    {
                        this.deathCount = 0;
                    }
                }
            }

            //if (ChallengeGameManager.Instance.blocks[_transform.position.x, _transform.position.y + 1, _transform.position.z] == BlockType.Empty)
            //{
            //    _transform.DOMoveY(_transform.position.y + 1, 1.0f);
            //}



            if (pos != transform.position)
                pos = transform.position;

            if (deathCount == 0)
            {
                if (this.tag == "JungleBlock")
                    ChallengeGameManager.Instance.jungleDestory++;
                else if (this.tag == "GrassBlock")
                    ChallengeGameManager.Instance.grassDestory++;
                else if (this.tag == "DesertBlock")
                    ChallengeGameManager.Instance.desertDestory++;
                else if (this.tag == "LavaBlock")
                    ChallengeGameManager.Instance.lavaDestory++;
                else if (this.tag == "IceBlock")
                    ChallengeGameManager.Instance.iceDestory++;

                ChallengeGameManager.Instance.nowBlock--;
                for (int i = 0; i < ChallengeGameManager.Instance.saveBlocks.Count; ++i)
                {
                    if (ChallengeGameManager.Instance.saveBlocks[i].transform.position == _transform.position)
                        ChallengeGameManager.Instance.saveBlocks.RemoveAt(i);
                }
                int nodeNum = (int)_transform.position.x + 7 * (int)_transform.position.z;
                ChallengeGameManager.Instance.nodeGap[nodeNum]++;
                manager.blocks[(int)_transform.position.x, (int)_transform.position.y, (int)_transform.position.z] = BlockType2.Empty;
                //    Debug.Log(_transform.position.x + " , " + (int)_transform.position.z);
                //   Debug.Log("파괴" + manager.blocks[(int)_transform.position.x, (int)_transform.position.z]);
                ChallengeGameManager.Instance.score_position = _transform.position;
                //  ScorePlus();//점수없으면 필요없음

                ChallengeGameManager.Instance.score += 1;

                gameObject.SetActive(false);
                AudioSource.PlayClipAtPoint(destory, new Vector3(0, 0, 0));

                if (ChallengeGameManager.Instance.nodeGap[nodeNum] == ChallengeGameManager.Instance.sum)
                    ChallengeGameManager.Instance.boom = true;

                manager.Init();
                Destroy(gameObject);
            }
            yield return null;
        }
    }
    void ScorePlus()
    {
        Text tmp = Instantiate(ChallengeGameManager.Instance.score_plus);
        tmp.transform.SetParent(ChallengeGameManager.Instance.canvas);
        //tmp.transform.SetParent(null);

        Vector3 pos = Camera.main.WorldToScreenPoint((ChallengeGameManager.Instance.score_position) + Vector3.right * 2.5f) - new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0f);

        tmp.transform.localPosition = pos;
        tmp.transform.localScale = Vector3.one;
        tmp.transform.localEulerAngles = Vector3.zero;

        tmp.SendMessage("ScorePlus");

        //tmp.transform.DOLocalMoveY(255f, 5.5f).SetEase(Ease.Linear);
        //tmp.transform.SetParent(ChallengeGameManager.Instance.canvas);

        //yield return new WaitForSeconds(1f);
        //tmp.transform.SetParent(null);

        //for (float a = 1; a >= 0; a -= 0.2f)
        //{
        //    print("alpha : "+a);
        //    tmp.color = new Vector4(0, 0, 0, a);
        //    yield return new WaitForSeconds(1f);
        //}

        // tmp.transform.SetParent(null);

        //Destroy(tmp.gameObject);
    }
}
