﻿using UnityEngine;
using System.Collections;

public class C_DragGameObject : MonoBehaviour
{ 
    Vector3 curScreenSpace;
    Vector3 curPosition;


    private bool triggerCheck = false;
    private ChallengeGameManager manager;
    private BlockSave bs;
    private float posY = 0;
    private float rotateAngle = 0;

    private int triggerCheckIndex = 0;
    private int rangeNum;


    // audio sound source
    public AudioClip up;
    public AudioClip down;
    void Start()
    {

        manager = ChallengeGameManager.Instance;
        bs = this.GetComponent<BlockSave>();

    }
    //void OnTriggerEnter(Collider col)
    //{
    //    if (col.tag == "GrassBlock" ||
    //        col.tag == "DesertBlock" ||
    //        col.tag == "LavaBlock" ||
    //        col.tag == "WoodBlock")
    //    {
    //        //Debug.Log("트리거 진입");
    //        //triggerCheck = true;
    //        triggerCheckIndex++;
    //        //Debug.Log(triggerCheckIndex);
    //    }
    //}
    //void OnTriggerExit(Collider col)
    //{
    //    if (col.tag == "GrassBlock" ||
    //       col.tag == "DesertBlock" ||
    //       col.tag == "LavaBlock" ||
    //       col.tag == "WoodBlock")
    //    {
    //        //Debug.Log("트리거 탈출");
    //        //triggerCheck = false;
    //        triggerCheckIndex--;
    //        //Debug.Log(triggerCheckIndex);
    //    }
    //}

    IEnumerator BlockClick()
    {
        if ((ChallengeGameManager.Instance.failCheck == false) && retry.Instance.pause == false)
        {
            AudioSource.PlayClipAtPoint(down, new Vector3(0, 0, 0));

 //           print("Enter");
            // Vector3 scrSpace = Camera.main.WorldToScreenPoint(transform.position);
            Vector3 offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));

            while (Input.GetMouseButton(0))
            {
                ChallengeGameManager.Instance.DragOn = true;
                ChallengeGameManager.Instance.clickedBlock = this.gameObject;
                ChallengeGameManager.Instance.Init();
                posY = 9;
                curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z);
                curPosition = Camera.main.ScreenToWorldPoint(curScreenSpace) + offset;
                // Debug.Log(curPosition);//Mathf.Round(curPosition.y) * 0


                if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 0)
                    transform.position = new Vector3(curPosition.x + ((curPosition.y - posY) * ChallengeGameManager.Instance.cosX), posY, curPosition.z + ((curPosition.y - posY) * ChallengeGameManager.Instance.cosZ));
                else if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 90)
                    transform.position = new Vector3(curPosition.x + ((curPosition.y - posY) * ChallengeGameManager.Instance.cosX), posY, curPosition.z - ((curPosition.y - posY) * ChallengeGameManager.Instance.cosZ));
                else if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 180)
                    transform.position = new Vector3(curPosition.x - ((curPosition.y - posY) * ChallengeGameManager.Instance.cosX), posY, curPosition.z - ((curPosition.y - posY) * ChallengeGameManager.Instance.cosZ));
                else if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 270)
                    transform.position = new Vector3(curPosition.x - ((curPosition.y - posY) * ChallengeGameManager.Instance.cosX), posY, curPosition.z + ((curPosition.y - posY) * ChallengeGameManager.Instance.cosZ));

                //transform.position = new Vector3(curPosition.x + ChallengeGameManager.Instance.cosX * (curPosition.y), posY, curPosition.z + ChallengeGameManager.Instance.cosZ * (curPosition.y));

                //if (manager.blocks[(int)Mathf.Round(transform.position.x), (int)Mathf.Round(transform.position.y), (int)Mathf.Round(transform.position.z)] != BlockType2.Empty)
                //{
                //   // transform.Translate(0,1,0);
                //}
                yield return null;

            }
            AudioSource.PlayClipAtPoint(up, new Vector3(0, 0, 0));

            if(ChallengeGameManager.Instance.nowStage == 1)
            {
                rangeNum = 2;
            }
            else if(ChallengeGameManager.Instance.nowStage == 2)
            {
                rangeNum = 3;
            }
            else if (ChallengeGameManager.Instance.nowStage == 3)
            {
                rangeNum = 4;
            }
            else if (ChallengeGameManager.Instance.nowStage == 4)
            {
                rangeNum = 5;
            }
            else if (ChallengeGameManager.Instance.nowStage == 5)
            {
                rangeNum = 6;
            }


            if ((int)Mathf.Round(transform.position.x) <= rangeNum && (int)Mathf.Round(transform.position.z) <= rangeNum &&
                (int)Mathf.Round(transform.position.x) >= 0 && (int)Mathf.Round(transform.position.z) >= 0)
            {

                if (manager.blocks[(int)Mathf.Round(transform.position.x), (int)posY, (int)Mathf.Round(transform.position.z)] != BlockType2.Empty)
                {

                    ChallengeGameManager.Instance.DragOn = false;
                    ChallengeGameManager.Instance.clickedBlock = null;
                    Destroy(gameObject);
                    //  Debug.Log((int)Mathf.Round(transform.position.x) + " , " + (int)Mathf.Round(transform.position.z));
                    //  Debug.Log(manager.blocks[(int)Mathf.Round(transform.position.x), (int)Mathf.Round(transform.position.z)] +"empty아님!!");
                    //                transform.position = bs.posSave;

                }
                if (manager.blocks[(int)Mathf.Round(transform.position.x), (int)posY, (int)Mathf.Round(transform.position.z)] == BlockType2.Empty)
                {
                    ChallengeGameManager.Instance.nowBlock++;
                    //                Mathf.Round(curPosition.y) * 0
                    if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 0)
                        transform.position = new Vector3(Mathf.Round(curPosition.x + ((curPosition.y - posY) * ChallengeGameManager.Instance.cosX)), posY, Mathf.Round(curPosition.z + ((curPosition.y - posY) * ChallengeGameManager.Instance.cosZ)));
                    else if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 90)
                        transform.position = new Vector3(Mathf.Round(curPosition.x + ((curPosition.y - posY) * ChallengeGameManager.Instance.cosX)), posY, Mathf.Round(curPosition.z - ((curPosition.y - posY) * ChallengeGameManager.Instance.cosZ)));
                    else if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 180)
                        transform.position = new Vector3(Mathf.Round(curPosition.x - ((curPosition.y - posY) * ChallengeGameManager.Instance.cosX)), posY, Mathf.Round(curPosition.z - ((curPosition.y - posY) * ChallengeGameManager.Instance.cosZ)));
                    else if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 270)
                        transform.position = new Vector3(Mathf.Round(curPosition.x - ((curPosition.y - posY) * ChallengeGameManager.Instance.cosX)), posY, Mathf.Round(curPosition.z + ((curPosition.y - posY) * ChallengeGameManager.Instance.cosZ)));


                    if (ChallengeGameManager.Instance.blockUI.Contains(ChallengeGameManager.Instance.selectBlockUI))
                    {
                        ChallengeGameManager.Instance.blockUI.Remove(ChallengeGameManager.Instance.selectBlockUI);
                    }
                    Destroy(ChallengeGameManager.Instance.selectBlockUI);
                    if (ChallengeGameManager.Instance.blocksquarecheck)
                    {
                        Destroy(ChallengeGameManager.Instance.imagetemp);
                        ChallengeGameManager.Instance.blocksquarecheck = false;
                    }
                    ChallengeGameManager.Instance.blockbutton_count--;

                }
                ChallengeGameManager.Instance.DragOn = false;
                ChallengeGameManager.Instance.clickedBlock = null;
                ChallengeGameManager.Instance.Init();
            }
            else
            {
                ChallengeGameManager.Instance.DragOn = false;
                ChallengeGameManager.Instance.clickedBlock = null;
                Destroy(gameObject);
            }
            ChallengeGameManager.Instance.selectBlockUI = null;
        }
    }
    //IEnumerator OnMouseDown()
    //{
    //    if ((ChallengeGameManager.Instance.failCheck == false) && retry.Instance.pause == false)
    //    {
    //        AudioSource.PlayClipAtPoint(down, new Vector3(0, 0, 0));

    //        // Vector3 scrSpace = Camera.main.WorldToScreenPoint(transform.position);
    //        Vector3 offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));

    //        while (Input.GetMouseButton(0))
    //        {
    //            ChallengeGameManager.Instance.DragOn = true;
    //            ChallengeGameManager.Instance.clickedBlock = this.gameObject;
    //            ChallengeGameManager.Instance.Init();
    //            posY = 9;
    //            curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z);
    //            curPosition = Camera.main.ScreenToWorldPoint(curScreenSpace) + offset;
    //            // Debug.Log(curPosition);//Mathf.Round(curPosition.y) * 0

    //            if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 0)
    //                transform.position = new Vector3(curPosition.x + ((curPosition.y - posY) * ChallengeGameManager.Instance.cosX), posY, curPosition.z + ((curPosition.y - posY) * ChallengeGameManager.Instance.cosZ));
    //            else if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 90)
    //                transform.position = new Vector3(curPosition.x + ((curPosition.y - posY) * ChallengeGameManager.Instance.cosX), posY, curPosition.z - ((curPosition.y - posY) * ChallengeGameManager.Instance.cosZ));
    //            else if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 180)
    //                transform.position = new Vector3(curPosition.x - ((curPosition.y - posY) * ChallengeGameManager.Instance.cosX), posY, curPosition.z - ((curPosition.y - posY) * ChallengeGameManager.Instance.cosZ));
    //            else if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 270)
    //                transform.position = new Vector3(curPosition.x - ((curPosition.y - posY) * ChallengeGameManager.Instance.cosX), posY, curPosition.z + ((curPosition.y - posY) * ChallengeGameManager.Instance.cosZ));
    //            //transform.position = new Vector3(curPosition.x + ChallengeGameManager.Instance.cosX * (curPosition.y), posY, curPosition.z + ChallengeGameManager.Instance.cosZ * (curPosition.y));

    //            //if (manager.blocks[(int)Mathf.Round(transform.position.x), (int)Mathf.Round(transform.position.y), (int)Mathf.Round(transform.position.z)] != BlockType2.Empty)
    //            //{
    //            //   // transform.Translate(0,1,0);
    //            //}
    //            yield return null;

    //        }
    //        AudioSource.PlayClipAtPoint(up, new Vector3(0, 0, 0));

    //        if ((int)Mathf.Round(transform.position.x) <= 6 && (int)Mathf.Round(transform.position.z) <= 6 &&
    //            (int)Mathf.Round(transform.position.x) >= 0 && (int)Mathf.Round(transform.position.z) >= 0)
    //        {
    //            if (manager.blocks[(int)Mathf.Round(transform.position.x), (int)posY, (int)Mathf.Round(transform.position.z)] != BlockType2.Empty)
    //            {
    //                if (gameObject.CompareTag("GrassBlock"))
    //                {
    //                    ChallengeGameManager.Instance.blockCounts[0]++;
    //                }
    //                else if (gameObject.CompareTag("JungleBlock"))
    //                {
    //                    print(gameObject.name);
    //                    ChallengeGameManager.Instance.blockCounts[1]++;
    //                }
    //                else if (gameObject.CompareTag("DesertBlock"))
    //                {
    //                    ChallengeGameManager.Instance.blockCounts[2]++;
    //                }
    //                else if (gameObject.CompareTag("LavaBlock"))
    //                {
    //                    ChallengeGameManager.Instance.blockCounts[3]++;
    //                }
    //                ChallengeGameManager.Instance.DragOn = false;
    //                ChallengeGameManager.Instance.clickedBlock = null;
    //                Destroy(gameObject);
    //                //  Debug.Log((int)Mathf.Round(transform.position.x) + " , " + (int)Mathf.Round(transform.position.z));
    //                //  Debug.Log(manager.blocks[(int)Mathf.Round(transform.position.x), (int)Mathf.Round(transform.position.z)] +"empty아님!!");
    //                //                transform.position = bs.posSave;

    //            }
    //            if (manager.blocks[(int)Mathf.Round(transform.position.x), (int)posY, (int)Mathf.Round(transform.position.z)] == BlockType2.Empty)
    //            {
    //                //                Mathf.Round(curPosition.y) * 0
    //                if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 0)
    //                    transform.position = new Vector3(Mathf.Round(curPosition.x + ((curPosition.y - posY) * ChallengeGameManager.Instance.cosX)), posY, Mathf.Round(curPosition.z + ((curPosition.y - posY) * ChallengeGameManager.Instance.cosZ)));
    //                else if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 90)
    //                    transform.position = new Vector3(Mathf.Round(curPosition.x + ((curPosition.y - posY) * ChallengeGameManager.Instance.cosX)), posY, Mathf.Round(curPosition.z - ((curPosition.y - posY) * ChallengeGameManager.Instance.cosZ)));
    //                else if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 180)
    //                    transform.position = new Vector3(Mathf.Round(curPosition.x - ((curPosition.y - posY) * ChallengeGameManager.Instance.cosX)), posY, Mathf.Round(curPosition.z - ((curPosition.y - posY) * ChallengeGameManager.Instance.cosZ)));
    //                else if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 270)
    //                    transform.position = new Vector3(Mathf.Round(curPosition.x - ((curPosition.y - posY) * ChallengeGameManager.Instance.cosX)), posY, Mathf.Round(curPosition.z + ((curPosition.y - posY) * ChallengeGameManager.Instance.cosZ)));

    //                if (ChallengeGameManager.Instance.blockUI.Contains(ChallengeGameManager.Instance.selectBlockUI))
    //                {
    //                    ChallengeGameManager.Instance.blockUI.Remove(ChallengeGameManager.Instance.selectBlockUI);
    //                }
    //                Destroy(ChallengeGameManager.Instance.selectBlockUI);
    //                ChallengeGameManager.Instance.blockbutton_count--;
    //            }

    //            ChallengeGameManager.Instance.DragOn = false;
    //            ChallengeGameManager.Instance.clickedBlock = null;
    //            ChallengeGameManager.Instance.Init();

    //        }
    //        else
    //        {
    //            if (gameObject.CompareTag("GrassBlock"))
    //            {
    //                ChallengeGameManager.Instance.blockCounts[0]++;
    //            }
    //            else if (gameObject.CompareTag("JungleBlock"))
    //            {
    //                print(gameObject.name);
    //                ChallengeGameManager.Instance.blockCounts[1]++;
    //            }
    //            else if (gameObject.CompareTag("DesertBlock"))
    //            {
    //                ChallengeGameManager.Instance.blockCounts[2]++;
    //            }
    //            else if (gameObject.CompareTag("LavaBlock"))
    //            {
    //                ChallengeGameManager.Instance.blockCounts[3]++;
    //            }
    //            ChallengeGameManager.Instance.DragOn = false;
    //            ChallengeGameManager.Instance.clickedBlock = null;
    //            Destroy(gameObject);
    //        }
    //        ChallengeGameManager.Instance.selectBlockUI = null;
    //    }
    //}
}