﻿using UnityEngine;
using System.Collections;

public class boundryLine : MonoBehaviour {

    GameManager gm;
    private float pos;
    bool start = false;
    private Transform _transform;
	// Use this for initialization
	void Start () {
        _transform = this.transform;
        _transform.position = new Vector3(0, GameManager.Instance.floorFind - 1.5f,0);
	}
	
	// Update is called once per frame
	void Update () {

        if (start == false)
        {
            pos = GameManager.Instance.floorFind - 1.5f;
            start = true;
        }
        if (GameManager.Instance.floorFind > 0)
        {
            if (pos > GameManager.Instance.floorFind - 1.5f)
            {
                pos -= 0.0175f;
            }
        }

        if (pos <= GameManager.Instance.floorFind - 1.5f)
        {
            pos += 0.0175f;
        }

        _transform.position = new Vector3(0, pos ,0);
        if(_transform.position.y < -0.5f)
        {
            _transform.position = new Vector3(0, -0.5f, 0);
        }
	}

    public void BoundaryInit()
    {
        if (_transform.position.y < GameManager.Instance.floorFind - 1.5f)
            _transform.Translate(0, 0.0175f, 0);
        if(_transform.position.y >= GameManager.Instance.floorFind - 1.5f)
            _transform.position = new Vector3(0, GameManager.Instance.floorFind - 1.5f, 0);
    }
}
