﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using DG.Tweening;

public enum BlockType
{
    Empty, 
    Grass, // 정글
    Desert, // 사막
    Jungle, // 아이스
    Lava, // 용암
    Wood, //  초원 으로 되어야 함
    Stone
    //태그는 정상적으로 관리됨.
}

public class GameManager : Singleton<GameManager>
{
    public Transform canvas;
    public Transform trail;
    public Transform cameraPivot;
    public GameObject startimage;
    public GameObject startimage_button;
    public Transform buttonbounce;

    public int maxSize; // 큐브를 놓을 최대 사이즈.

    public bool GameStart = false;

    bool trailRender = false;
    bool swipe = true;

    /// <summary>
    /// Item ///

    [System.NonSerialized]
    public GameObject tempObject; // 아이템 블록 저장.
    /// </summary>

    RaycastHit hit;
    Ray ray;
    GameObject[] empty;
    GameObject[] grass;
    GameObject[] desert;
    GameObject[] jungle;
    GameObject[] lava;
    GameObject[] wood;
    GameObject[] stone; // 박혀있는 놈
    float test = 1.4f;
    List<BlockSave> selectBlocks = new List<BlockSave>();
    public BlockType[,,] blocks = new BlockType[10, 10, 10];
    [System.NonSerialized]
    public bool DragOn; // 드래그 중일 때 On
    [System.NonSerialized]
    public float cosX;

    [System.NonSerialized]
    public float cosZ;

    [System.NonSerialized]
    public int floorCount; // 각 층에 블록 갯수 카운트
    [System.NonSerialized]
    public int floorFind; // 최고 층 카운트

    public GameObject[] blockobjs;
    public int[] blockCounts;
    public Text[] blockCountsText;

    public GameObject clickedBlock;
    public bool failCheck;
    bool clear = false;
    /// ///////////////

    //public float _time = 0;
    public float __time = 15;

    public GameObject ClearUI;
    public GameObject FailUI;
    public Slider __timeBarSlider;
    public Slider _timeBarSlider;


    public CanvasGroup clearbackground;
    public CanvasGroup cleareffect;

    public GameObject clearui_destroy;
    public GameObject clearui_button;

    public GameObject blockButton;
    public Sprite[] blockSprite;
    public int blockbutton_count;


    public Text scoretext;
    public Text coin_scoretext;

  

    bool over = false;

    public List<GameObject> blockUI = new List<GameObject>();//블럭UI리스트
    public GameObject selectBlockUI = null;

    public int score = 0;
    public int coin_score = 0;

    public Vector3 score_position;
    public Text score_plus;

    Vector3 curClickPos, prevClickPos;
    float distanceX, distanceY;
    public Transform gameCamera;


    //stage manager
    private string sceneName;
    private string[] stage = new string[10];
    private List<GameObject> stageList = new List<GameObject>(); // stageList
    private int stageCount;
    private int stageNum = 9;
    private int stageCheck = 0;

    void Start()
    {

        Init();
        //if(selectBlocks.Contains())

        cosX = Mathf.Cos(Mathf.Deg2Rad * Camera.main.transform.eulerAngles.y);
        cosZ = Mathf.Cos(Mathf.Deg2Rad * Camera.main.transform.eulerAngles.x);

        if (User.Instance != null)
        {
            startimage.SetActive(true);
            startimage_button.SetActive(true);

            buttonbounce.DOScale(buttonbounce.localScale * 1.1f, 0.7f).SetLoops(-1, LoopType.Yoyo);
        }



        //  StartCoroutine("Update");

        stage[0] = "-1";
        stage[1] = "-2";
        stage[2] = "-3";
        stage[3] = "-4";
        stage[4] = "-5";
        stage[5] = "-6";
        stage[6] = "-7";
        stage[7] = "-8";
        stage[8] = "-9";
        stage[9] = "10";
        sceneName = Application.loadedLevelName;
        stageCount = 0;
        stageList.Add(GameObject.Find(sceneName + stage[0]));
        stageList.Add(GameObject.Find(sceneName + stage[1]));
        stageList.Add(GameObject.Find(sceneName + stage[2]));
        stageList.Add(GameObject.Find(sceneName + stage[3]));
        stageList.Add(GameObject.Find(sceneName + stage[4]));
        stageList.Add(GameObject.Find(sceneName + stage[5]));
        stageList.Add(GameObject.Find(sceneName + stage[6]));
        stageList.Add(GameObject.Find(sceneName + stage[7]));
        stageList.Add(GameObject.Find(sceneName + stage[8]));
        stageList.Add(GameObject.Find(sceneName + stage[9]));

        for (int i = 0; i < 10; ++i)
        {
            stageList.Add(GameObject.Find(sceneName + stage[i]));
        }

        for (int i = 1; i < 10; ++i)
        {
            if (stageList[i] != null)
            {
                stageList[i].SetActive(false);
            }
            else
                stageCheck++;
        }
        StartCoroutine("StartScene");

        StartCoroutine("updateRoutine");
    }
    public void clearCheck()
    {
        if (grass.Length == 0 && desert.Length == 0 && jungle.Length == 0 && lava.Length == 0 && wood.Length == 0)
        {
            // SceneManager.Instance.SetNextSceneName("1_Intro");
            //Application.LoadLevel("4_Loading");
        }
    }


    public void Init()
    {
        Debug.Log("Init 호출!!!!!");
        //Empty,
        //Grass,
        //Desert,
        //Jungle,
        //Lava
        empty = GameObject.FindGameObjectsWithTag("EmptyBlock");
        grass = GameObject.FindGameObjectsWithTag("GrassBlock");
        desert = GameObject.FindGameObjectsWithTag("DesertBlock");
        jungle = GameObject.FindGameObjectsWithTag("JungleBlock");
        lava = GameObject.FindGameObjectsWithTag("LavaBlock");
        wood = GameObject.FindGameObjectsWithTag("IceBlock");
        stone = GameObject.FindGameObjectsWithTag("stoneBlock");

        floorCount = 0; // 각 층에 블록 갯수 카운트
        floorFind = 0; // 최고 층 카운트
        int[] emptyCheck = new int[10] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };


        for (int i = 0; i < 10; ++i)
        {
            for (int j = 0; j < 10; ++j)
                for (int k = 0; k < 10; ++k)
                    blocks[i, j, k] = BlockType.Empty;
        }

        for (int i = 0; i < grass.Length; ++i)
        {
            if ((int)grass[i].transform.position.y >= 0)
            {
                if ((int)grass[i].transform.position.x >= 0 && (int)grass[i].transform.position.z >= 0 && grass[i].activeSelf && grass[i] != clickedBlock)
                    blocks[(int)grass[i].transform.position.x, (int)grass[i].transform.position.y, (int)grass[i].transform.position.z] = BlockType.Grass;
            }
        }
        for (int i = 0; i < desert.Length; ++i)
        {
            if ((int)desert[i].transform.position.y >= 0)
            {
                if ((int)desert[i].transform.position.x >= 0 && (int)desert[i].transform.position.z >= 0 && desert[i].activeSelf && desert[i] != clickedBlock)
                    blocks[(int)desert[i].transform.position.x, (int)desert[i].transform.position.y, (int)desert[i].transform.position.z] = BlockType.Desert;
            }
        }
        for (int i = 0; i < jungle.Length; ++i)
        {
            if ((int)jungle[i].transform.position.y >= 0)
            {
                if ((int)jungle[i].transform.position.x >= 0 && (int)jungle[i].transform.position.z >= 0 && jungle[i].activeSelf && jungle[i] != clickedBlock)
                    blocks[(int)jungle[i].transform.position.x, (int)jungle[i].transform.position.y, (int)jungle[i].transform.position.z] = BlockType.Jungle;
            }
        }
        for (int i = 0; i < lava.Length; ++i)
        {
            if ((int)lava[i].transform.position.y >= 0)
            {
                if ((int)lava[i].transform.position.x >= 0 && (int)lava[i].transform.position.z >= 0 && lava[i].activeSelf && lava[i] != clickedBlock)
                    blocks[(int)lava[i].transform.position.x, (int)lava[i].transform.position.y, (int)lava[i].transform.position.z] = BlockType.Lava;
            }
        }
        for (int i = 0; i < wood.Length; ++i)
        {
            if ((int)wood[i].transform.position.y >= 0)
            {
                if ((int)wood[i].transform.position.x >= 0 && (int)wood[i].transform.position.z >= 0 && wood[i].activeSelf && wood[i] != clickedBlock)
                    blocks[(int)wood[i].transform.position.x, (int)wood[i].transform.position.y, (int)wood[i].transform.position.z] = BlockType.Wood;
            }
        }
        for (int i = 0; i < stone.Length; ++i)
        {
            if ((int)stone[i].transform.position.y >= 0)
            {
                if ((int)stone[i].transform.position.x >= 0 && (int)stone[i].transform.position.z >= 0 && stone[i].activeSelf && stone[i] != clickedBlock)
                    blocks[(int)stone[i].transform.position.x, (int)stone[i].transform.position.y, (int)stone[i].transform.position.z] = BlockType.Stone;
            }
        }

        for (int i = 0; i < 10; ++i) // y 최대 찾기
        {
            floorCount = 0;
            for (int j = 0; j < 10; ++j)
            {
                for (int k = 0; k < 10; ++k)
                {
                    if (blocks[j, i, k] != BlockType.Empty)
                    {
                        //yFloor[i] = 1;
                        floorCount++;
                    }
                    if (blocks[j, i, k] == BlockType.Stone)
                    {
                        //yFloor[i] = 1;
                        floorCount--;
                    }
                    if (j < 8 && k < 8)
                    {

                        if (blocks[j, i, k] != BlockType.Empty)
                        {
                            emptyCheck[9 - i]++;
                        } // x = j , z = k 일 때 위에서부터 몇번 째 있는 지
                    }
                }
            }

            if (floorCount > 0)
            {
                floorFind = i + 1;
            }

        }
        for (int i = 0; i < 7; ++i) // x 최대 찾기
        {
            floorCount = 0;
            for (int j = 0; j < 7; ++j)
            {
                for (int k = 0; k < 7; ++k)
                    if (blocks[i, j, k] != BlockType.Empty)
                    {

                        floorCount++;
                    }
            }

        }

        for (int i = 0; i < 7; ++i) // y 최대 찾기
        {
            floorCount = 0;
            for (int j = 0; j < 7; ++j)
            {
                for (int k = 0; k < 7; ++k)
                    if (blocks[k, j, i] != BlockType.Empty)
                    {
                        floorCount++;
                    }
            }

        }
    }

    void OnApplicationPause() // 포커스를 잃었을 때 ( 백그라운드 시 )
    {
        retry.Instance.stageretry();
    }


    public void NEXTStage(int stage)
    {
        SceneManager.Instance.SetNextSceneName("Stage_" + stage);
        Application.LoadLevel("4_Loading");
    }
    public void StageExit()
    {
        startimage.SetActive(true);
        retry.Instance.pause = false;

        if (FailUI.activeSelf)
        {
            FailUI.GetComponent<CanvasGroup>().DOFade(0f, 1f);
            startimage.GetComponent<Image>().DOFade(1f, 1f).SetDelay(1f).OnComplete(() =>
            {
                SceneManager.Instance.SetNextSceneName("1_Intro");
                Application.LoadLevel("4_Loading");
            });
        }
        else
        {
            startimage.GetComponent<Image>().DOFade(1f, 1f).SetDelay(1f).OnComplete(() =>
            {
                SceneManager.Instance.SetNextSceneName("1_Intro");
                Application.LoadLevel("4_Loading");
            });

        }
    }

    void Check(List<BlockSave> lis) // 조건 체크 함수
    {
        int sum = 0;
        int sctCount = 0;
        int overDeathCount = 0;
        for (int i = 1; i < lis.Count; ++i)
        {
            if (lis[i - 1].deathCount == lis[i].deathCount)
            {
                if (lis[i - 1].tag == lis[i].tag)
                {
                    sum++;
                }
                if (lis[i - 1].deathCount > 1 && lis[i].deathCount > 1)
                {
                    overDeathCount++;
                }
            }
        }
        Debug.Log("sum=" + sum);
        if (overDeathCount == lis.Count - 1)
        {
            if (lis.Count > 1)
            {
                for (int i = 0; i < lis.Count; ++i)
                {
                    lis[i].deathCount--;
                }
            }
        }
        else {
            if (sctCount == 0)
            {
                if (lis.Count < 5)
                {

                    if (sum == 2)
                    {
                        //정글일 때 (ㄱ자)
                        if (lis[0].tag == "JungleBlock" && lis[1].tag == "JungleBlock" && lis[2].tag == "JungleBlock")
                        {
                            if (Mathf.Round(lis[0].thisTransform.position.y) == Mathf.Round(lis[1].thisTransform.position.y) && Mathf.Round(lis[1].thisTransform.position.y) == Mathf.Round(lis[2].thisTransform.position.y))
                            {

                                if (Mathf.Abs(lis[0].thisTransform.position.x - lis[2].thisTransform.position.x) == 1 && Mathf.Abs(lis[0].thisTransform.position.z - lis[2].thisTransform.position.z) == 1)
                                {
                                    // 파괴 하라 ( 규칙이 맞으면 )
                                    if (Mathf.Sqrt(Mathf.Pow(lis[1].thisTransform.position.x - lis[2].thisTransform.position.x, 2.0f) + Mathf.Pow(lis[1].thisTransform.position.z - lis[2].thisTransform.position.z, 2.0f)) == 1)
                                    {
                                        for (int j = 0; j < 3; ++j)
                                        {
                                            lis[j].deathCount--;
                                        }
                                    }
                                }
                                else if (Mathf.Sqrt(Mathf.Pow(lis[0].thisTransform.position.x - lis[2].thisTransform.position.x, 2.0f) + Mathf.Pow(lis[0].thisTransform.position.z - lis[2].thisTransform.position.z, 2.0f)) == 1) // ㄴ 자 가운데 에서 시작한 모양
                                {

                                    // 파괴 하라 ( 규칙이 맞으면 )
                                    for (int j = 0; j < 3; ++j)
                                    {
                                        lis[j].deathCount--;
                                    }
                                }
                            }
                            else
                            {
                                // z가 같을 때
                                if ((Mathf.Sqrt(Mathf.Pow(Mathf.Round(lis[0].thisTransform.position.x) - Mathf.Round(lis[2].thisTransform.position.x), 2.0f) + Mathf.Pow(Mathf.Round(lis[0].thisTransform.position.y) - Mathf.Round(lis[2].thisTransform.position.y), 2.0f)) == Mathf.Sqrt(2)))
                                {
                                    if (Mathf.Sqrt(Mathf.Pow(Mathf.Round(lis[1].thisTransform.position.x) - Mathf.Round(lis[2].thisTransform.position.x), 2.0f) + Mathf.Pow(Mathf.Round(lis[1].thisTransform.position.y) - Mathf.Round(lis[2].thisTransform.position.y), 2.0f)) == 1)
                                    {
                                        if (lis[0].thisTransform.position.z == lis[2].thisTransform.position.z || lis[0].thisTransform.position.x == lis[2].thisTransform.position.x)
                                        {
                                            for (int j = 0; j < 3; ++j)
                                            {
                                                lis[j].deathCount--;
                                            }
                                        }
                                    }
                                }
                                // x가 같을 때
                                else if ((Mathf.Sqrt(Mathf.Pow(Mathf.Round(lis[0].thisTransform.position.y) - Mathf.Round(lis[2].thisTransform.position.y), 2.0f) + Mathf.Pow(Mathf.Round(lis[0].thisTransform.position.z) - Mathf.Round(lis[2].thisTransform.position.z), 2.0f)) == Mathf.Sqrt(2)))
                                {
                                    if ((Mathf.Sqrt(Mathf.Pow(Mathf.Round(lis[1].thisTransform.position.y) - Mathf.Round(lis[2].thisTransform.position.y), 2.0f) + Mathf.Pow(Mathf.Round(lis[1].thisTransform.position.z) - Mathf.Round(lis[2].thisTransform.position.z), 2.0f)) == 1))
                                    {
                                        if (lis[0].thisTransform.position.z == lis[2].thisTransform.position.z || lis[0].thisTransform.position.x == lis[2].thisTransform.position.x)
                                        {
                                            for (int j = 0; j < 3; ++j)
                                            {
                                                lis[j].deathCount--;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //사막일 때 (ㅡ자 * 3)
                    if (sum == 2)
                    {

                        if (lis[0].tag == "DesertBlock" && lis[1].tag == "DesertBlock" && lis[2].tag == "DesertBlock") // 모두 같으니 하나만 체크하면 됨. 
                        {
                            if (Mathf.Round(lis[0].thisTransform.position.y) == Mathf.Round(lis[2].thisTransform.position.y) && Mathf.Round(lis[1].thisTransform.position.y) == Mathf.Round(lis[2].thisTransform.position.y))
                            {
                                if ((lis[0].thisTransform.position.x == lis[1].thisTransform.position.x) && (lis[1].thisTransform.position.x == lis[2].thisTransform.position.x))
                                {
                                    if (Mathf.Abs(lis[0].thisTransform.position.z - lis[2].thisTransform.position.z) == 2)
                                    {
                                        // 파괴 하라 ( 규칙이 맞으면 )
                                        for (int j = 0; j < 3; ++j)
                                        {
                                            lis[j].deathCount--;
                                        }
                                    }
                                }
                                else if ((lis[0].thisTransform.position.z == lis[1].thisTransform.position.z) && (lis[1].thisTransform.position.z == lis[2].thisTransform.position.z))
                                {
                                    if (Mathf.Abs(lis[0].thisTransform.position.x - lis[2].thisTransform.position.x) == 2)
                                    {
                                        // 파괴 하라 ( 규칙이 맞으면 )
                                        for (int j = 0; j < 3; ++j)
                                        {
                                            lis[j].deathCount--;
                                        }
                                    }
                                }
                            }
                            else {
                                if (lis[0].thisTransform.position.x == lis[1].thisTransform.position.x && lis[0].thisTransform.position.x == lis[2].thisTransform.position.x && lis[0].thisTransform.position.z == lis[1].thisTransform.position.z && lis[0].thisTransform.position.z == lis[2].thisTransform.position.z)
                                {
                                    if (Mathf.Abs(Mathf.Round(lis[0].thisTransform.position.y) - Mathf.Round(lis[2].thisTransform.position.y)) == 2)
                                    {
                                        for (int j = 0; j < 3; ++j)
                                        {
                                            lis[j].deathCount--;
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
                if (lis.Count < 4)
                {
                    //초원일 때 (ㅡ자 * 2)
                    if (sum == 1)
                    {
                        if (lis[0].tag == "GrassBlock" && lis[1].tag == "GrassBlock")
                        {
                            Debug.Log("초록2개");
                            if ((int)lis[0].thisTransform.position.y == (int)lis[1].thisTransform.position.y)
                            {
                                Debug.Log("y같음");
                                if (Mathf.Abs(lis[0].thisTransform.position.z - lis[1].thisTransform.position.z) == 1 || Mathf.Abs(lis[0].thisTransform.position.x - lis[1].thisTransform.position.x) == 1)
                                {
                                    // 파괴 하라 ( 규칙이 맞으면 )
                                    if ((int)lis[0].thisTransform.position.z == (int)lis[1].thisTransform.position.z || (int)lis[0].thisTransform.position.x == (int)lis[1].thisTransform.position.x)
                                    {
                                        for (int j = 0; j < 2; ++j)
                                        {
                                            lis[j].deathCount--;

                                        }
                                    }
                                }
                            }
                            else
                            {
                                if ((int)lis[0].thisTransform.position.x == (int)lis[1].thisTransform.position.x && (int)lis[0].thisTransform.position.z == (int)lis[1].thisTransform.position.z)
                                {
                                    Debug.Log("y다름");
                                    for (int j = 0; j < 2; ++j)
                                    {
                                        Debug.Log(Mathf.Round(lis[0].thisTransform.position.y) + "," + (Mathf.Round(lis[1].thisTransform.position.y)));
                                        if (Mathf.Abs(Mathf.Round(lis[0].thisTransform.position.y) - Mathf.Round(lis[1].thisTransform.position.y)) == 1)
                                        {
                                            Debug.Log("파괴조건진입");
                                            lis[j].deathCount--;
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
                //아이스일 때
                if (lis.Count < 6)
                {
                    if (sum == 3)
                    {
                        if (lis[0].tag == "IceBlock" && lis[1].tag == "IceBlock" && lis[2].tag == "IceBlock" && lis[3].tag == "IceBlock")
                        {
                            if ((int)lis[0].thisTransform.position.y == (int)lis[1].thisTransform.position.y && (int)lis[1].thisTransform.position.y == (int)lis[2].thisTransform.position.y && (int)lis[2].thisTransform.position.y == (int)lis[3].thisTransform.position.y)
                            {
                                if (Mathf.Sqrt(Mathf.Pow((int)lis[0].thisTransform.position.x - (int)lis[3].thisTransform.position.x, 2.0f) + Mathf.Pow((int)lis[0].thisTransform.position.z - (int)lis[3].thisTransform.position.z, 2.0f)) == Mathf.Sqrt(5))
                                {
                                    if (Mathf.Sqrt(Mathf.Pow((int)lis[0].thisTransform.position.x - (int)lis[2].thisTransform.position.x, 2.0f) + Mathf.Pow((int)lis[0].thisTransform.position.z - (int)lis[2].thisTransform.position.z, 2.0f)) == Mathf.Sqrt(2) &&
                                        Mathf.Sqrt(Mathf.Pow((int)lis[1].thisTransform.position.x - (int)lis[3].thisTransform.position.x, 2.0f) + Mathf.Pow((int)lis[1].thisTransform.position.z - (int)lis[3].thisTransform.position.z, 2.0f)) == Mathf.Sqrt(2) &&
                                        Mathf.Sqrt(Mathf.Pow((int)lis[2].thisTransform.position.x - (int)lis[3].thisTransform.position.x, 2.0f) + Mathf.Pow((int)lis[2].thisTransform.position.z - (int)lis[3].thisTransform.position.z, 2.0f)) == (int)1)
                                    {
                                        for (int j = 0; j < 4; ++j)
                                        {
                                            lis[j].deathCount--;
                                        }
                                    }
                                }
                            }
                            else {
                                if (Mathf.Sqrt(Mathf.Pow((int)lis[0].thisTransform.position.y - (int)lis[3].thisTransform.position.y, 2.0f) + Mathf.Pow((int)lis[0].thisTransform.position.z - (int)lis[3].thisTransform.position.z, 2.0f)) == Mathf.Sqrt(5))
                                {
                                    if (Mathf.Sqrt(Mathf.Pow((int)lis[0].thisTransform.position.y - (int)lis[2].thisTransform.position.y, 2.0f) + Mathf.Pow((int)lis[0].thisTransform.position.z - (int)lis[2].thisTransform.position.z, 2.0f)) == Mathf.Sqrt(2) &&
                                        Mathf.Sqrt(Mathf.Pow((int)lis[1].thisTransform.position.y - (int)lis[3].thisTransform.position.y, 2.0f) + Mathf.Pow((int)lis[1].thisTransform.position.z - (int)lis[3].thisTransform.position.z, 2.0f)) == Mathf.Sqrt(2) &&
                                        Mathf.Sqrt(Mathf.Pow((int)lis[2].thisTransform.position.y - (int)lis[3].thisTransform.position.y, 2.0f) + Mathf.Pow((int)lis[2].thisTransform.position.z - (int)lis[3].thisTransform.position.z, 2.0f)) == (int)1)
                                    {
                                        // 파괴 하라 ( 규칙이 맞으면 )
                                        for (int j = 0; j < 4; ++j)
                                        {
                                            lis[j].deathCount--;
                                        }
                                    }
                                }
                                else if (Mathf.Sqrt(Mathf.Pow((int)lis[0].thisTransform.position.x - (int)lis[3].thisTransform.position.x, 2.0f) + Mathf.Pow((int)lis[0].thisTransform.position.y - (int)lis[3].thisTransform.position.y, 2.0f)) == Mathf.Sqrt(5))
                                {
                                    if (Mathf.Sqrt(Mathf.Pow((int)lis[0].thisTransform.position.x - (int)lis[2].thisTransform.position.x, 2.0f) + Mathf.Pow((int)lis[0].thisTransform.position.y - (int)lis[2].thisTransform.position.y, 2.0f)) == Mathf.Sqrt(2) &&
                                        Mathf.Sqrt(Mathf.Pow((int)lis[1].thisTransform.position.x - (int)lis[3].thisTransform.position.x, 2.0f) + Mathf.Pow((int)lis[1].thisTransform.position.y - (int)lis[3].thisTransform.position.y, 2.0f)) == Mathf.Sqrt(2) &&
                                        Mathf.Sqrt(Mathf.Pow((int)lis[2].thisTransform.position.x - (int)lis[3].thisTransform.position.x, 2.0f) + Mathf.Pow((int)lis[2].thisTransform.position.y - (int)lis[3].thisTransform.position.y, 2.0f)) == 1)
                                    {
                                        // 파괴 하라 ( 규칙이 맞으면 )
                                        for (int j = 0; j < 4; ++j)
                                        {
                                            lis[j].deathCount--;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //용암일 때 (ㅁ자)
                if (lis.Count < 6)
                {
                    if (sum == 3)
                    {
                        if (lis[0].tag == "LavaBlock" && lis[1].tag == "LavaBlock" && lis[2].tag == "LavaBlock" && lis[3].tag == "LavaBlock")
                        {
                            if ((int)lis[0].thisTransform.position.y == (int)lis[1].thisTransform.position.y && (int)lis[1].thisTransform.position.y == (int)lis[2].thisTransform.position.y && (int)lis[2].thisTransform.position.y == (int)lis[3].thisTransform.position.y)
                            {
                                if (Mathf.Sqrt(Mathf.Pow((int)lis[0].thisTransform.position.x - (int)lis[3].thisTransform.position.x, 2.0f) + Mathf.Pow((int)lis[0].thisTransform.position.z - (int)lis[3].thisTransform.position.z, 2.0f)) == 1)
                                {
                                    // 파괴 하라 ( 규칙이 맞으면 )
                                    for (int j = 0; j < 4; ++j)
                                    {
                                        lis[j].deathCount--;
                                    }
                                }
                            }
                            else {
                                if (Mathf.Sqrt(Mathf.Pow((int)lis[0].thisTransform.position.y - (int)lis[3].thisTransform.position.y, 2.0f) + Mathf.Pow((int)lis[0].thisTransform.position.z - (int)lis[3].thisTransform.position.z, 2.0f)) == 1)
                                {
                                    // 파괴 하라 ( 규칙이 맞으면 )
                                    for (int j = 0; j < 4; ++j)
                                    {
                                        lis[j].deathCount--;
                                    }
                                }
                                else if (Mathf.Sqrt(Mathf.Pow((int)lis[0].thisTransform.position.x - (int)lis[3].thisTransform.position.x, 2.0f) + Mathf.Pow((int)lis[0].thisTransform.position.y - (int)lis[3].thisTransform.position.y, 2.0f)) == 1)
                                {
                                    // 파괴 하라 ( 규칙이 맞으면 )
                                    for (int j = 0; j < 4; ++j)
                                    {
                                        lis[j].deathCount--;
                                    }
                                }
                            }
                        }
                    }//색깔이 다르면 ..
                }
            }
        }

        //}
        //else
        //    Debug.Log("다름");
    }

    void ClickBlockUI(int i)
    {
        if ((Instance.failCheck == false) && retry.Instance.pause == false)
        {
            if (blockCounts[i] > 0)
            {
                blockCounts[i]--;



                GameObject tempBlock = GameObject.Instantiate(blockobjs[i]) as GameObject;
                Vector3 temp = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 0)
                    tempBlock.transform.position = new Vector3(temp.x + cosX * (temp.y), 0f, temp.z + cosZ * (temp.y));
                else if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 90)
                    tempBlock.transform.position = new Vector3(temp.x + cosX * (temp.y), 0f, temp.z - cosZ * (temp.y));
                else if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 180)
                    tempBlock.transform.position = new Vector3(temp.x - cosX * (temp.y), 0f, temp.z - cosZ * (temp.y));
                else if (Mathf.Round(GameManager.Instance.cameraPivot.eulerAngles.y) == 270)
                    tempBlock.transform.position = new Vector3(temp.x - cosX * (temp.y), 0f, temp.z + cosZ * (temp.y));

                //tempBlock.transform.position = new Vector3(temp.x + cosX * (temp.y ),0f, temp.z + cosZ * (temp.y));

                tempBlock.SendMessage("BlockClick");
            }
        }
    }
    IEnumerator StartScene()
    {
        //print("또");

        yield return new WaitForSeconds(1f);
        try
        {
            startimage_button.GetComponent<CanvasGroup>().DOFade(1f, 1f);
            //  startimage_button.SetActive(true);
        }
        catch
        { }

    }
    IEnumerator Startclearui()
    {
        //print("또");

        yield return new WaitForSeconds(2f);
        clearui_destroy.SetActive(false);
        cleareffect.DOFade(1f, 2f);
        clearui_button.SetActive(true);
        clearui_button.transform.DOScale(clearui_button.transform.localScale * 1.1f, 0.7f).SetLoops(-1, LoopType.Yoyo);

    }

    public void startimagebuttonset()
    {
        StartCoroutine("startimagebuttoncoroutine");

        //GameStart = true;
    }
    IEnumerator startimagebuttoncoroutine()
    {
        startimage_button.GetComponent<CanvasGroup>().DOFade(0f, 1f);
        yield return new WaitForSeconds(0.5f);
        startimage.GetComponent<Image>().DOFade(0f, 1f);
        yield return new WaitForSeconds(1f);
        startimage_button.SetActive(false);
        startimage.SetActive(false);
        GameStart = true;
        yield return null;
    }

    public bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
    IEnumerator ImageAlpha(Image image, float value)
    {
        if (image.color.a < value)
        {
            while (image.color.a < value)
            {
                image.color += new Color(0, 0, 0, 1f * Time.deltaTime);
                yield return null;
            }
        }
        else
        {
            while (image.color.a > value)
            {
                image.color -= new Color(0, 0, 0, 1f * Time.deltaTime);
                yield return null;
            }
        }
    }



    IEnumerator updateRoutine()
    {

        while (true)
        {

            if (clear == false && grass.Length == 0 && desert.Length == 0 && jungle.Length == 0 && lava.Length == 0 && wood.Length == 0 && __time >= 0 && stageCount + stageCheck < stageNum)
            {
                if (stageCount < stageNum)
                {
                    Debug.Log(8 - stageCount);
                    stageList[stageCount].SetActive(false);
                    stageList[stageCount + 1].SetActive(true);
                    Init();
                    stageCount++;
                }
            }

            //for (int i = 0; i < blockCounts.Length; ++i)
            //{
            //    try
            //    {
            //        blockCountsText[i].text = blockCounts[i].ToString();
            //    }
            //    catch
            //    { }
            //}
            for (int i = 0; i < blockCounts.Length; ++i)
            {
                if(blockCountsText[i])
                    blockCountsText[i].text = blockCounts[i].ToString();
                
            }
            //if (coin_scoretext != null)
            //    coin_scoretext.text = coin_score.ToString();

            hit = new RaycastHit();
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //clean
            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(ray, out hit)) // HIT
                {
                    trailRender = true;

                    if (hit.collider.tag == "GrassBlockUI")
                    {
                        selectBlockUI = hit.transform.gameObject;
                        ClickBlockUI(0);
                    }
                    else if (hit.collider.tag == "JungleBlockUI")
                    {
                        selectBlockUI = hit.transform.gameObject;
                        ClickBlockUI(1);
                    }
                    else if (hit.collider.tag == "DesertBlockUI")
                    {
                        selectBlockUI = hit.transform.gameObject;
                        ClickBlockUI(2);
                    }
                    else if (hit.collider.tag == "LavaBlockUI")
                    {
                        selectBlockUI = hit.transform.gameObject;
                        ClickBlockUI(3);
                    }
                    else if (hit.collider.tag == "IceBlockUI")
                    {
                        selectBlockUI = hit.transform.gameObject;
                        ClickBlockUI(4);

                    }
                }
            }//clean
            else if (Input.GetMouseButton(0))
            {
                if (IsPointerOverUIObject() == false && trailRender)
                {
                    Vector3 temp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    if (Mathf.Round(cameraPivot.eulerAngles.y) == 0)
                        trail.position = new Vector3(temp.x + ((temp.y - 10f) * cosX), 10f, temp.z + ((temp.y - 10f) * cosZ));
                    else if (Mathf.Round(cameraPivot.eulerAngles.y) == 90)
                        trail.position = new Vector3(temp.x + ((temp.y - 10f) * cosX), 10f, temp.z - ((temp.y - 10f) * cosZ));
                    else if (Mathf.Round(cameraPivot.eulerAngles.y) == 180)
                        trail.position = new Vector3(temp.x - ((temp.y - 10f) * cosX), 10f, temp.z - ((temp.y - 10f) * cosZ));
                    else if (Mathf.Round(cameraPivot.eulerAngles.y) == 270)
                        trail.position = new Vector3(temp.x - ((temp.y - 10f) * cosX), 10f, temp.z + ((temp.y - 10f) * cosZ));

                    //trail.position = new Vector3(temp.x + cosX * (temp.y - 10f), 10f, temp.z + cosZ * (temp.y - 10f));
                }
                //clean
                if (over == false && Physics.Raycast(ray, out hit) && (failCheck == false) && retry.Instance.pause == false) // HIT
                {
                    if (hit.collider.tag == "GrassBlock" ||
                        hit.collider.tag == "DesertBlock" ||
                        hit.collider.tag == "JungleBlock" ||
                        hit.collider.tag == "LavaBlock" ||
                        hit.collider.tag == "IceBlock" ||
                        hit.collider.tag == "BLOCK")
                    {
                        BlockSave bs = hit.collider.GetComponent<BlockSave>();

                        if (selectBlocks.Contains(bs) == false)
                        {
                            if (DragOn == false)
                            {
                                selectBlocks.Add(bs);
                                //  sctBlocks.Add(hit.collider.tag);
                                bs.transform.localScale += new Vector3(0.4f, 0.4f, 0.4f);
                                bs.GetComponent<BoxCollider>().size -= new Vector3(0.1f, 0.1f, 0.1f);

                            }

                        }
                    }
                    else
                    {
                        Debug.Log("TouchMiss");
                    }
                }
                else
                {
                    over = true;

                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                trailRender = false;
                ///

                over = false;
                Check(selectBlocks);
                clearCheck();
                if (DragOn == false)
                {
                    for (int i = 0; i < selectBlocks.Count; ++i)
                    {
                        selectBlocks[i].transform.localScale -= new Vector3(0.4f, 0.4f, 0.4f);
                        selectBlocks[i].GetComponent<BoxCollider>().size += new Vector3(0.1f, 0.1f, 0.1f);



                    }
                }
                selectBlocks.Clear();

            }
            if (GameStart && (grass.Length != 0 || desert.Length != 0 || jungle.Length != 0 || lava.Length != 0 || wood.Length != 0))
            {
                if (__time > 0)
                    __time -= Time.deltaTime;
                __timeBarSlider.value = __time;
            }
            //클리어
            if (clear == false && grass.Length == 0 && desert.Length == 0 && jungle.Length == 0 && lava.Length == 0 && wood.Length == 0 && __time >= 0 && stageCount + stageCheck == stageNum)
            {
                if (blockCounts[0] == 0 && blockCounts[1] == 0 && blockCounts[2] == 0 && blockCounts[3] == 0 && blockCounts[4] == 0)
                {
                    clear = true;
                    User.Instance.Stage[SceneManager.Instance.selectStageIndex - 1] = 2;
                    if (User.Instance.Stage[SceneManager.Instance.selectStageIndex] == 0)
                        User.Instance.Stage[SceneManager.Instance.selectStageIndex] = 1;

                    //selectStageIndex 다음스테이지로 바로 넘어가려면 ++selectStageIndex;
                    string stageInfo = User.Instance.StageInfoIntToStr();//stageInfo에 int->string으로 바꾼걸 저장
                    PlayerPrefs.SetString("StageInfo", stageInfo);//키값으로 찾아서 저장
                    ClearUI.SetActive(true);
                    clearbackground.DOFade(1f, 1f);
                    StartCoroutine("Startclearui");
                }
                    
                // User.Instance.Sta

            }
            if (__time <= 0 && failCheck == false)
            {
                __time = 0;
                failCheck = true;

                FailUI.SetActive(true);
                FailUI.GetComponent<CanvasGroup>().DOFade(1f, 1f);

            }


            //yield return null;


            ray = Camera.main.ScreenPointToRay(Input.mousePosition);


            if (Input.GetMouseButtonDown(0) && (Physics.Raycast(ray, out hit) == false && IsPointerOverUIObject() == false))
            {
                curClickPos = Input.mousePosition;
                prevClickPos = curClickPos;
            }
            else if (Input.GetMouseButtonDown(0))
            {
                swipe = true;
            }

            if (Input.GetMouseButton(0))
            {
                if (Application.platform == RuntimePlatform.WindowsEditor || Input.touchCount == 1)
                {
                    curClickPos = Input.mousePosition;
                    distanceX = curClickPos.x - prevClickPos.x;
                    distanceY = curClickPos.y - prevClickPos.y;
                    if (distanceX > 12f && Mathf.Abs(distanceY) < 10f)
                    {
                        if (swipe == false)
                        {
                            RotateRight();
                            swipe = true;
                        }
                    }
                    else if (distanceX < -12f && Mathf.Abs(distanceY) < 10f)
                    {
                        if (swipe == false)
                        {
                            RotateLeft();
                            swipe = true;
                        }
                    }
                    prevClickPos = curClickPos;
                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                swipe = false;
            }

            yield return null;
        }
    }

    public void RotateRight()
    {
        if (DOTween.IsTweening(cameraPivot) == false)
        {
            gameCamera.parent = cameraPivot;
            cameraPivot.DORotate(cameraPivot.eulerAngles + Vector3.up * 90f, 0.3f).SetEase(Ease.Linear).OnComplete(() => { gameCamera.parent = null; print("Right"); });
        }
    }
    public void RotateLeft()
    {
        if (DOTween.IsTweening(cameraPivot) == false)
        {
            gameCamera.parent = cameraPivot;
            cameraPivot.DORotate(cameraPivot.eulerAngles - Vector3.up * 90f, 0.3f).SetEase(Ease.Linear).OnComplete(() => { gameCamera.parent = null; print("Left"); });
        }
    }

}