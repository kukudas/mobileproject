﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class ScoreText : MonoBehaviour {

    public Text _text;
    
    void ScorePlus()
    {
        StartCoroutine("ScorePlusRoutine");
    }

    IEnumerator ScorePlusRoutine()
    {
        transform.DOLocalMoveY(1000f, 20f).SetEase(Ease.Linear);
        transform.SetParent(GameManager.Instance.canvas);

        //yield return new WaitForSeconds(1f);
        //tmp.transform.SetParent(null);

        for (float a = 1; a >= 0; a -= 0.05f)
        {
            print("alpha : " + a);
            _text.color = new Vector4(255, 255, 255, a);
            yield return new WaitForSeconds(0.1f);
        }

        // tmp.transform.SetParent(null);

        Destroy(gameObject);
        yield return null;
    }
}
