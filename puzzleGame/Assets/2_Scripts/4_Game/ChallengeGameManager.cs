﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using DG.Tweening;

public enum BlockType2
{
    Empty,
    Grass, // 정글
    Desert, // 사막
    Jungle, // 아이스
    Lava, // 용암
    Wood, //  초원 으로 되어야 함
    Perfect
    
    //태그는 정상적으로 관리됨.
}
public enum LengthXZ
{
   Max = 7
}


public class ChallengeGameManager : Singleton<ChallengeGameManager>
{
    public float blockSpeed;

    public Transform canvas;
    public Transform trail;
    public Transform cameraPivot;
    public bool alphaComp;
    public Image background;

    public bool gamestart = false;
    public int sum;

    bool trailRender = false;
    bool swipe = true;
    bool over = false;

    public bool changeStage = false;

    // item1)grass All clear
    // item2)desert All clear
    // item3)jungle
    // item4)lava 
    // item5)Ice
    // item6)X 가로
    // item7)Z 세로
    // item8)Y 층
    // item9)ㅁ  주변 1
    // item10)ㅁ*2 주변2
    [System.NonSerialized]
    public GameObject tempObject; // 아이템 블록 저장.

    RaycastHit hit;
    Ray ray;
    Vector3 v3;
    GameObject[] empty;
    GameObject[] grass;
    GameObject[] desert;
    GameObject[] jungle;
    GameObject[] lava;
    GameObject[] wood;
    GameObject[] perfect;

    [System.NonSerialized]
    public int grassDestory;
    [System.NonSerialized]
    public int desertDestory;
    [System.NonSerialized]
    public int jungleDestory;
    [System.NonSerialized]
    public int lavaDestory;
    [System.NonSerialized]
    public int iceDestory;

    float test = 1.4f;
    List<BlockSave2> selectBlocks = new List<BlockSave2>();
    public List<GameObject> floor = new List<GameObject>();
    public List<GameObject> saveBlocks = new List<GameObject>();

    [System.NonSerialized]
    public int nowBlock;

    public Text NowBlock_Text;
    public Text Coin_Text;
    public int maxBlock;

    public BlockType2[,,] blocks = new BlockType2[10, 10, 10];
    BlockSave2 bs;
    public bool DragOn; // 드래그 중일 때 On
    public float cosX;
    public float cosZ;

    private int lastIndex;
    public int[,] floorCount = new int[(int)LengthXZ.Max, (int)LengthXZ.Max]; // 각 층에 블록 갯수 카운트
    public int[,] floorFind = new int[(int)LengthXZ.Max, (int)LengthXZ.Max]; // 최고 층 카운트
    public int[,] xlengthFind = new int[(int)LengthXZ.Max, (int)LengthXZ.Max]; // 블록 길이 카운트
    public int[,] zlengthFind = new int[(int)LengthXZ.Max, (int)LengthXZ.Max];
    public int MaxFloorCheck;
    public int floorZero;
    public int[,] MaxFloorCheck2 = new int[8, 8]; // 최고층에 꽉 차있는지 체크.
    bool[] intheFloor = new bool[10] { false, false, false, false, false, false, false, false, false, false }; // floor에 자식이 들어왔나 체크
    //[i][j][k]별로 관리
    public bool[,,] nowStart = new bool[(int)LengthXZ.Max, 10, (int)LengthXZ.Max]; // init 시작
    public int[,,] floorByCoodinate = new int[(int)LengthXZ.Max, 10, (int)LengthXZ.Max];


    public GameObject[] blockobjs;
    public int[] blockCounts;
    public Text[] blockCountsText;

    public GameObject clickedBlock;
    public bool failCheck;
    /// ///////////////

    public float _time = 0;
    public float __time = 15;

    public GameObject ClearUI;
    public GameObject FailUI;
    public CanvasGroup gameover;
    public Slider __timeBarSlider;
    public Slider _timeBarSlider;

    public GameObject blockButton;
    public Image blocksquare;
    public bool blocksquarecheck = false;

    public Sprite greensquare;
    public Sprite purplesquare;
    public Sprite yellowsquare;
    public Sprite redsquare;
    public Sprite bluesquare;

    public Sprite[] blockSprite;
    public Sprite[] howToBlock;


    public int blockbutton_count;

    public Text scoretext;
    public Text coin_scoretext;



    public List<GameObject> blockUI = new List<GameObject>();//블럭UI리스트
    public GameObject selectBlockUI = null;
    public Image imagetemp = null;

    public int score = 0;
    public int coin_score = 0;

    public Vector3 score_position;
    public Text score_plus;

    Vector3 curClickPos, prevClickPos;
    float distanceX, distanceY;
    public Transform gameCamera;

    /// <summary>
    /// Time별 변수
    public int nowStage;
    /// </summary>
    public bool[] item = new bool[10];

    // 갭 - 각 노드마다 파괴된 층의 수
    public int[] nodeGap = new int[49];
    public int[] nodeFloor = new int[49];
    public bool boom = false;


    void Start()
    {

        nowBlock = 0;
        background.gameObject.SetActive(true);
        background.DOFade(0f, 1f).SetDelay(0.4f).OnComplete(() => { background.gameObject.SetActive(false); gamestart = true; });
        for (int i = 0; i < item.Length; ++i)
        {
            item[i] = false;
        }

        for (int i = 0; i < nodeGap.Length; ++i)
        {
            nodeGap[i] = 0;
            nodeFloor[i] = 10;// - floorFind[(int)myBlocks[i].transform.position.x, (int)myBlocks[i].transform.position.z]];
        }

        Init();
        //if(selectBlocks.Contains())

        cosX = Mathf.Cos(Mathf.Deg2Rad * Camera.main.transform.eulerAngles.y);
        cosZ = Mathf.Cos(Mathf.Deg2Rad * Camera.main.transform.eulerAngles.x);
        StartCoroutine("updateRoutine");

        StartCoroutine("CreateBlockButtonRoutine");
    }
    public void clearCheck()
    {
        if (grass.Length == 0 && desert.Length == 0 && jungle.Length == 0 && lava.Length == 0 && wood.Length == 0)
        {
            // SceneManager.Instance.SetNextSceneName("1_Intro");
            //Application.LoadLevel("4_Loading");
        }
    }


    public void Init()
    {
        //Empty,
        //Grass,
        //Desert,
        //Jungle,
        //Lava
        empty = GameObject.FindGameObjectsWithTag("EmptyBlock");
        grass = GameObject.FindGameObjectsWithTag("GrassBlock");
        desert = GameObject.FindGameObjectsWithTag("DesertBlock");
        jungle = GameObject.FindGameObjectsWithTag("JungleBlock");
        lava = GameObject.FindGameObjectsWithTag("LavaBlock");
        wood = GameObject.FindGameObjectsWithTag("IceBlock");
        perfect = GameObject.FindGameObjectsWithTag("PerfectBlock");
        for (int i = 0; i < (int)LengthXZ.Max; ++i)
        {
            for (int j = 0; j < (int)LengthXZ.Max; ++j)
            {
                floorCount[i, j] = 0; // 각 층에 블록 갯수 카운트
                floorFind[i, j] = 0; // 최고 층 카운트
                xlengthFind[i, j] = 0; // 길이 카운트
                zlengthFind[i, j] = 0; // 길이 카운트
                for (int k = 0; k < 10; ++k)
                {
                    floorByCoodinate[i, k, j] = 0;
                }
            }
        }
        int[] emptyCheck = new int[10] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
        for (int i = 0; i < 8; ++i)
        {
            for (int j = 0; j < 8; ++j)
            {
                MaxFloorCheck = 0;
                MaxFloorCheck2[i, j] = 0; // 최고층 끝쪽이 비었나
            }

        }
        //for (int i = 1; i < 9; ++i)
        //{
        //    Debug.Log(i);
        //    MaxFloorCheck[i] = 0; // 최고층 있나 없나 ( 0이면 없음 1이상이면 있음 )
        //                          //Debug.Log(grass.Length);
        //}


        for (int i = 0; i < 10; ++i)
        {
            for (int j = 0; j < 10; ++j)
                for (int k = 0; k < 10; ++k)
                    blocks[i, j, k] = BlockType2.Empty;
        }

        for (int i = 0; i < grass.Length; ++i)
        {
            if ((int)grass[i].transform.position.y >= 0)
            {
                if ((int)grass[i].transform.position.x >= 0 && (int)grass[i].transform.position.z >= 0 && grass[i].activeSelf && grass[i] != clickedBlock)
                    blocks[(int)grass[i].transform.position.x, (int)grass[i].transform.position.y, (int)grass[i].transform.position.z] = BlockType2.Grass;
            }
        }
        for (int i = 0; i < desert.Length; ++i)
        {
            if ((int)desert[i].transform.position.y >= 0)
            {
                if ((int)desert[i].transform.position.x >= 0 && (int)desert[i].transform.position.z >= 0 && desert[i].activeSelf && desert[i] != clickedBlock)
                    blocks[(int)desert[i].transform.position.x, (int)desert[i].transform.position.y, (int)desert[i].transform.position.z] = BlockType2.Desert;
            }
        }
        for (int i = 0; i < jungle.Length; ++i)
        {
            if ((int)jungle[i].transform.position.y >= 0)
            {
                if ((int)jungle[i].transform.position.x >= 0 && (int)jungle[i].transform.position.z >= 0 && jungle[i].activeSelf && jungle[i] != clickedBlock)
                    blocks[(int)jungle[i].transform.position.x, (int)jungle[i].transform.position.y, (int)jungle[i].transform.position.z] = BlockType2.Jungle;
            }

        }
        for (int i = 0; i < lava.Length; ++i)
        {
            if ((int)lava[i].transform.position.y >= 0)
            {
                if ((int)lava[i].transform.position.x >= 0 && (int)lava[i].transform.position.z >= 0 && lava[i].activeSelf && lava[i] != clickedBlock)
                    blocks[(int)lava[i].transform.position.x, (int)lava[i].transform.position.y, (int)lava[i].transform.position.z] = BlockType2.Lava;
            }
        }
        for (int i = 0; i < wood.Length; ++i)
        {
            if ((int)wood[i].transform.position.y >= 0)
            {
                if ((int)wood[i].transform.position.x >= 0 && (int)wood[i].transform.position.z >= 0 && wood[i].activeSelf && wood[i] != clickedBlock)
                    blocks[(int)wood[i].transform.position.x, (int)wood[i].transform.position.y, (int)wood[i].transform.position.z] = BlockType2.Wood;
            }
        }
        for (int i = 0; i < perfect.Length; ++i)
        {
            if ((int)perfect[i].transform.position.y >= 0)
            {
                if ((int)perfect[i].transform.position.x >= 0 && (int)perfect[i].transform.position.z >= 0 && perfect[i].activeSelf && perfect[i] != clickedBlock)
                    blocks[(int)perfect[i].transform.position.x, (int)perfect[i].transform.position.y, (int)perfect[i].transform.position.z] = BlockType2.Perfect;
            }
        }

        floorZero = 0;
        for (int i = 0; i < 10; ++i) // y 최대 찾기
        {
            for (int j = 0; j < (int)LengthXZ.Max; ++j)
            {
                for (int k = 0; k < (int)LengthXZ.Max; ++k)
                {
                    floorCount[j, k] = 0; // 각 층에 블록 갯수 카운트
                }
            }


            for (int j = 0; j < (int)LengthXZ.Max; ++j)
            {
                for (int k = 0; k < (int)LengthXZ.Max; ++k)
                {
                    if (blocks[j, i, k] != BlockType2.Empty)
                    {
                        //yFloor[i] = 1;
                        floorCount[j, k]++;
                    }


                    if (j < (int)LengthXZ.Max && k < (int)LengthXZ.Max)
                    {

                        if (blocks[j, i, k] != BlockType2.Empty)
                        {
                            emptyCheck[9 - i]++;
                            floorByCoodinate[j, i, k] = 1;
                            if (i == 9)
                            {

                                MaxFloorCheck++;
                                MaxFloorCheck2[j, k]++;
                            }
                        } // x = j , z = k 일 때 위에서부터 몇번 째 있는 지
                    }
                }
                //Debug.Log("yFloor" + i + "=" + yFloor[i]);
            }


            for (int j = 0; j < (int)LengthXZ.Max; ++j)
            {
                for (int k = 0; k < (int)LengthXZ.Max; ++k)
                {
                    if (floorCount[j, k] != 0)
                    {
                        floorFind[j, k]++;
                    }
                }
            }
        }



        for (int i = 0; i < (int)LengthXZ.Max; ++i) // x 최대 찾기
        {
            for (int j = 0; j < (int)LengthXZ.Max; ++j)
            {
                for (int k = 0; k < (int)LengthXZ.Max; ++k)
                {
                    floorCount[j, k] = 0; // 각 층에 블록 갯수 카운트
                }
            }
            for (int j = 0; j < (int)LengthXZ.Max; ++j)
            {
                for (int k = 0; k < (int)LengthXZ.Max; ++k)
                    if (blocks[i, j, k] != BlockType2.Empty)
                    {

                        floorCount[j, k]++;
                    }
            }
            for (int j = 0; j < (int)LengthXZ.Max; ++j)
            {
                for (int k = 0; k < (int)LengthXZ.Max; ++k)
                {
                    if (floorCount[j, k] != 0)
                    {
                        xlengthFind[j, k]++;
                    }
                }
            }
        }

        for (int i = 0; i < (int)LengthXZ.Max; ++i) // y 최대 찾기
        {
            for (int j = 0; j < (int)LengthXZ.Max; ++j)
            {
                for (int k = 0; k < (int)LengthXZ.Max; ++k)
                {
                    floorCount[j, k] = 0; // 각 층에 블록 갯수 카운트
                }
            }
            for (int j = 0; j < (int)LengthXZ.Max; ++j)
            {
                for (int k = 0; k < (int)LengthXZ.Max; ++k)
                    if (blocks[i, j, k] != BlockType2.Empty)
                    {

                        floorCount[j, k]++;
                    }
            }
            for (int j = 0; j < (int)LengthXZ.Max; ++j)
            {
                for (int k = 0; k < (int)LengthXZ.Max; ++k)
                {
                    if (floorCount[j, k] != 0)
                    {
                        zlengthFind[j, k]++;
                    }
                }
            }
        }
        // 수정
        for (int i = 0; i < (int)LengthXZ.Max; ++i)
        {
            for (int j = 0; j < 10; ++j)
            {
                for (int k = 0; k < (int)LengthXZ.Max; ++k)
                {
                    if (floorByCoodinate[i, j, k] != 0)
                    {
                        nowStart[i, j, k] = true;
                    }
                }
            }
        }

        // 카메라의 orthographicSize가 최소 3에서 최대 (int)LengthXZ.Max까지 변환


    }

    void OnApplicationPause() // 포커스를 잃었을 때 ( 백그라운드 시 )
    {
        retry.Instance.stageretry();
    }

    // Update is called once per frame
    IEnumerator updateRoutine()
    {
        while (true)
        {
            if (nowStage == 4)
            {
                if (Camera.main.orthographicSize < 9)
                {
                    Camera.main.orthographicSize += 0.05f;
                    Camera.main.transform.Translate(0, 0.05f,0);
                }
            }
            else if (nowStage == 5)
            {
                if (Camera.main.orthographicSize < 10)
                {
                    Camera.main.orthographicSize += 0.05f;
                    Camera.main.transform.Translate(0, 0.05f, 0);
                }
            }

            NowBlock_Text.text = nowBlock.ToString() + "/" + maxBlock.ToString();
            Coin_Text.text = "Coin : " + coin_score;
            //NowBlock_Text.text = nowBlock.ToString();
            //MaxBlock_Text.text = maxBlock.ToString(); 

            if (scoretext != null)
                scoretext.text = score.ToString();
            if (coin_scoretext != null)
                coin_scoretext.text = coin_score.ToString();
            hit = new RaycastHit();
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(ray, out hit)) // HIT
                {
                    trailRender = true;
                    if (blockUI[0] == hit.transform.gameObject)
                    {
                        if (hit.collider.tag == "GrassBlockUI")
                        {
                            selectBlockUI = hit.transform.gameObject;
                            ClickBlockUI(0);
                        }
                        else if (hit.collider.tag == "DesertBlockUI")
                        {
                            selectBlockUI = hit.transform.gameObject;
                            ClickBlockUI(1);
                        }
                        else if (hit.collider.tag == "JungleBlockUI")
                        {
                            selectBlockUI = hit.transform.gameObject;
                            ClickBlockUI(2);
                        }
                        else if (hit.collider.tag == "LavaBlockUI")
                        {
                            selectBlockUI = hit.transform.gameObject;
                            ClickBlockUI(3);
                        }
                        else if (hit.collider.tag == "IceBlockUI")
                        {
                            selectBlockUI = hit.transform.gameObject;
                            ClickBlockUI(4);
                        }
                    }
                }
            }
            else if (Input.GetMouseButton(0))
            {
                Debug.Log("2");
                if (IsPointerOverUIObject() == false && trailRender)
                {
                    Vector3 temp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    if (Mathf.Round(cameraPivot.eulerAngles.y) == 0)
                        trail.position = new Vector3(temp.x + ((temp.y - 10f) * cosX), 10f, temp.z + ((temp.y - 10f) * cosZ));
                    else if (Mathf.Round(cameraPivot.eulerAngles.y) == 90)
                        trail.position = new Vector3(temp.x + ((temp.y - 10f) * cosX), 10f, temp.z - ((temp.y - 10f) * cosZ));
                    else if (Mathf.Round(cameraPivot.eulerAngles.y) == 180)
                        trail.position = new Vector3(temp.x - ((temp.y - 10f) * cosX), 10f, temp.z - ((temp.y - 10f) * cosZ));
                    else if (Mathf.Round(cameraPivot.eulerAngles.y) == 270)
                        trail.position = new Vector3(temp.x - ((temp.y - 10f) * cosX), 10f, temp.z + ((temp.y - 10f) * cosZ));

                    //trail.position = new Vector3(temp.x + cosX * (temp.y - 10f), 10f, temp.z + cosZ * (temp.y - 10f));
                }

                if (over == false && Physics.Raycast(ray, out hit) && (failCheck == false) && retry.Instance.pause == false) // HIT
                {
                    if (hit.collider.tag == "GrassBlock" ||
                        hit.collider.tag == "DesertBlock" ||
                        hit.collider.tag == "JungleBlock" ||
                        hit.collider.tag == "LavaBlock" ||
                        hit.collider.tag == "IceBlock" ||
                        hit.collider.tag == "PerfectBlock")
                    {
                        BlockSave2 bs = hit.collider.GetComponent<BlockSave2>();
                        v3 = bs.pos;

                        if (selectBlocks.Contains(bs) == false)
                        {
                            if (DragOn == false)
                            {
                                selectBlocks.Add(bs);
                                //  sctBlocks.Add(hit.collider.tag);
                                bs.transform.localScale += new Vector3(0.4f, 0.4f, 0.4f);
                                bs.GetComponent<BoxCollider>().size -= new Vector3(0.1f, 0.1f, 0.1f);
                            }

                        }
                    }
                    else if (hit.collider.tag == "Item1")
                    {
                        item[0] = true;
                    }
                    else if (hit.collider.tag == "Item2")
                    {
                        item[1] = true;
                    }
                    else if (hit.collider.tag == "Item3")
                    {
                        item[2] = true;
                    }
                    else if (hit.collider.tag == "Item4")
                    {
                        item[3] = true;
                    }
                    else if (hit.collider.tag == "Item5")
                    {
                        item[4] = true;
                    }
                    else if (hit.collider.tag == "X_Clear")
                    {
                        item[5] = true;
                        tempObject = hit.transform.gameObject;
                    }
                    else if (hit.collider.tag == "Z_Clear")
                    {
                        item[6] = true;
                        tempObject = hit.transform.gameObject;
                    }
                    else if (hit.collider.tag == "FloorClear")
                    {
                        item[7] = true;
                        tempObject = hit.transform.gameObject;
                    }
                    else if (hit.collider.tag == "Surrounding1")
                    {
                        item[8] = true;
                        tempObject = hit.transform.gameObject;
                    }
                    else if (hit.collider.tag == "Surrounding2")
                    {
                        item[9] = true;
                        tempObject = hit.transform.gameObject;
                    }
                    else
                    {
                        // item6)X 가로
                        // item7)Z 세로
                        // item8)Y 층
                        // item9)ㅁ  주변 1
                        // item10)ㅁ*2 주변2
                        //Debug.Log("TouchMiss");
                    }
                }
                else
                {
                    over = true;
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                trailRender = false;
                for (int i = 0; i < item.Length; ++i)
                {
                    item[i] = false;
                }
                over = false;

                Check(selectBlocks);
                clearCheck();
                if (DragOn == false)
                {
                    for (int i = 0; i < selectBlocks.Count; ++i)
                    {
                        selectBlocks[i].transform.localScale -= new Vector3(0.4f, 0.4f, 0.4f);
                        selectBlocks[i].GetComponent<BoxCollider>().size += new Vector3(0.1f, 0.1f, 0.1f);

                    }
                }
                selectBlocks.Clear();

            }

            if (grass.Length != 0 || desert.Length != 0 || jungle.Length != 0 || lava.Length != 0 || wood.Length != 0)
            {

                if (__time > 0)
                    __time -= Time.deltaTime;
            }
            if (nowBlock > maxBlock)
            {

                failCheck = true;
                if (FailUI.activeSelf == false)
                {
                    FailUI.SetActive(true);
                    FailUI.GetComponent<CanvasGroup>().DOFade(1f, 1f);
                }
            }
            //__time -= Time.deltaTime;
            _time += Time.deltaTime;
            _timeBarSlider.value = _time;
            yield return null;

            if (Input.GetMouseButtonDown(0) && (Physics.Raycast(ray, out hit) == false && IsPointerOverUIObject() == false))
            {
                curClickPos = Input.mousePosition;
                prevClickPos = curClickPos;
            }
            else if (Input.GetMouseButtonDown(0))
            {
                swipe = true;
            }

            if (Input.GetMouseButton(0))
            {
                if (Application.platform == RuntimePlatform.WindowsEditor || Input.touchCount == 1)
                {
                    curClickPos = Input.mousePosition;
                    distanceX = curClickPos.x - prevClickPos.x;
                    distanceY = curClickPos.y - prevClickPos.y;
                    if (distanceX > 12f && Mathf.Abs(distanceY) < 10f)
                    {
                        if (swipe == false)
                        {
                            RotateRight();
                            swipe = true;
                        }
                    }
                    else if (distanceX < -12f && Mathf.Abs(distanceY) < 10f)
                    {
                        if (swipe == false)
                        {
                            RotateLeft();
                            swipe = true;
                        }
                    }
                    prevClickPos = curClickPos;
                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                swipe = false;
            }

        }
    }
    public void NEXTStage(int stage)
    {
        SceneManager.Instance.SetNextSceneName("Stage_" + stage);
        Application.LoadLevel("4_Loading");
    }
    public void StageExit()
    {
        background.gameObject.SetActive(true);
        //background.DOFade(1f, 1f);
        // background.DOFade(1f, 1f).SetDelay(1f)
        retry.Instance.pause = false;

        if (FailUI.activeSelf)
        {
            gameover.DOFade(0f, 1f);
            //FailUI.GetComponent<CanvasGroup>().DOFade(0f, 1f).SetDelay(1f);
            background.GetComponent<Image>().DOFade(1f, 1f).SetDelay(1f).OnComplete(() =>
            {
                SceneManager.Instance.SetNextSceneName("1_Intro");
                Application.LoadLevel("4_Loading");
            });
        }
        //else
        //{
        //    background.GetComponent<Image>().DOFade(1f, 1f).SetDelay(1f).OnComplete(() =>
        //    {
        //        SceneManager.Instance.SetNextSceneName("1_Intro");
        //        Application.LoadLevel("4_Loading");
        //    });

        // }


        //retry.Instance.pause = false;
        //SceneManager.Instance.SetNextSceneName("1_Intro");
        //Application.LoadLevel("4_Loading");
    }

    void Check(List<BlockSave2> lis) // 조건 체크 함수
    {
        sum = 0;
        int sctCount = 0;
        //if (lis[lis.Count].tag == "PerfectBlock")
        //    sum++;
        for (int i = 1; i < lis.Count; ++i)
        {
            if(lis[i].tag == "PerfectBlock")
            {
                lis[i].tag = lis[i-1].tag;
            }
            if(lis[i-1].tag == "PerfectBlock")
            {
                lis[i - 1].tag = lis[i].tag;
            }


            if (lis[i - 1].tag == lis[i].tag)
                sum++;
            print("sum=" + sum);
        }

        //if (lis.Count - 1 == sum)
        //{
        //    Debug.Log("모두 같음");
        if (sctCount == 0)
        {
            if (lis.Count < 5)
            {
                print("lisCount Okay");
                if (sum == 2)
                {
                    print("SUM Okay");
                    //정글일 때 (ㄱ자)
                    if ((lis[0].tag == "JungleBlock" || lis[0].tag == "PerfectBlock") && (lis[1].tag == "JungleBlock" || lis[1].tag == "PerfectBlock") && (lis[2].tag == "JungleBlock"|| lis[2].tag == "PerfectBlock"))
                    {
                        print("tag Okay");
                        if (Mathf.Round(lis[0].pos.y) == Mathf.Round(lis[1].pos.y) && Mathf.Round(lis[1].pos.y) == Mathf.Round(lis[2].pos.y))
                        {

                            if (Mathf.Abs(lis[0].pos.x - lis[2].pos.x) == 1 && Mathf.Abs(lis[0].pos.z - lis[2].pos.z) == 1)
                            {
                                // 파괴 하라 ( 규칙이 맞으면 )
                                if (Mathf.Sqrt(Mathf.Pow(lis[1].pos.x - lis[2].pos.x, 2.0f) + Mathf.Pow(lis[1].pos.z - lis[2].pos.z, 2.0f)) == 1)
                                {
                                    for (int j = 0; j < 3; ++j)
                                    {
                                        lis[j].deathCount--;
                                    }
                                }
                            }
                            else if (Mathf.Sqrt(Mathf.Pow(lis[0].pos.x - lis[2].pos.x, 2.0f) + Mathf.Pow(lis[0].pos.z - lis[2].pos.z, 2.0f)) == 1) // ㄴ 자 가운데 에서 시작한 모양
                            {

                                // 파괴 하라 ( 규칙이 맞으면 )
                                for (int j = 0; j < 3; ++j)
                                {
                                    lis[j].deathCount--;
                                }
                            }
                        }
                        else
                        {
                            // z가 같을 때
                            if ((Mathf.Sqrt(Mathf.Pow(Mathf.Round(lis[0].pos.x) - Mathf.Round(lis[2].pos.x), 2.0f) + Mathf.Pow(Mathf.Round(lis[0].pos.y) - Mathf.Round(lis[2].pos.y), 2.0f)) == Mathf.Sqrt(2)))
                            {
                                if (Mathf.Sqrt(Mathf.Pow(Mathf.Round(lis[1].pos.x) - Mathf.Round(lis[2].pos.x), 2.0f) + Mathf.Pow(Mathf.Round(lis[1].pos.y) - Mathf.Round(lis[2].pos.y), 2.0f)) == 1)
                                {
                                    if (lis[0].pos.z == lis[2].pos.z || lis[0].pos.x == lis[2].pos.x)
                                    {
                                        for (int j = 0; j < 3; ++j)
                                        {
                                            lis[j].deathCount--;
                                        }
                                    }
                                }
                            }
                            // x가 같을 때
                            else if ((Mathf.Sqrt(Mathf.Pow(Mathf.Round(lis[0].pos.y) - Mathf.Round(lis[2].pos.y), 2.0f) + Mathf.Pow(Mathf.Round(lis[0].pos.z) - Mathf.Round(lis[2].pos.z), 2.0f)) == Mathf.Sqrt(2)))
                            {
                                if ((Mathf.Sqrt(Mathf.Pow(Mathf.Round(lis[1].pos.y) - Mathf.Round(lis[2].pos.y), 2.0f) + Mathf.Pow(Mathf.Round(lis[1].pos.z) - Mathf.Round(lis[2].pos.z), 2.0f)) == 1))
                                {
                                    if (lis[0].pos.z == lis[2].pos.z || lis[0].pos.x == lis[2].pos.x)
                                    {
                                        for (int j = 0; j < 3; ++j)
                                        {
                                            lis[j].deathCount--;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //사막일 때 (ㅡ자 * 3)
                if (sum == 2)
                {

                    if (lis[0].tag == "DesertBlock" && lis[1].tag == "DesertBlock" && lis[2].tag == "DesertBlock") // 모두 같으니 하나만 체크하면 됨. 
                    {
                        if (Mathf.Round(lis[0].pos.y) == Mathf.Round(lis[2].pos.y) && Mathf.Round(lis[1].pos.y) == Mathf.Round(lis[2].pos.y))
                        {
                            if ((lis[0].pos.x == lis[1].pos.x) && (lis[1].pos.x == lis[2].pos.x))
                            {
                                if (Mathf.Abs(lis[0].pos.z - lis[2].pos.z) == 2)
                                {
                                    // 파괴 하라 ( 규칙이 맞으면 )
                                    for (int j = 0; j < 3; ++j)
                                    {
                                        lis[j].deathCount--;
                                    }
                                }
                            }
                            else if ((lis[0].pos.z == lis[1].pos.z) && (lis[1].pos.z == lis[2].pos.z))
                            {
                                if (Mathf.Abs(lis[0].pos.x - lis[2].pos.x) == 2)
                                {
                                    // 파괴 하라 ( 규칙이 맞으면 )
                                    for (int j = 0; j < 3; ++j)
                                    {
                                        lis[j].deathCount--;
                                    }
                                }
                            }
                        }
                        else {
                            if (lis[0].pos.x == lis[1].pos.x && lis[0].pos.x == lis[2].pos.x && lis[0].pos.z == lis[1].pos.z && lis[0].pos.z == lis[2].pos.z)
                            {
                                if (Mathf.Abs(Mathf.Round(lis[0].pos.y) - Mathf.Round(lis[2].pos.y)) == 2)
                                {
                                    for (int j = 0; j < 3; ++j)
                                    {
                                        lis[j].deathCount--;
                                    }
                                }
                            }
                        }

                    }
                }
            }
            if (lis.Count < 4)
            {
                //초원일 때 (ㅡ자 * 2)
                if (sum == 1)
                {
                    if (lis[0].tag == "GrassBlock" && lis[1].tag == "GrassBlock")
                    {
                        if ((int)lis[0].pos.y == (int)lis[1].pos.y)
                        {
                            if (Mathf.Abs(lis[0].pos.z - lis[1].pos.z) == 1 || Mathf.Abs(lis[0].pos.x - lis[1].pos.x) == 1)
                            {
                                // 파괴 하라 ( 규칙이 맞으면 )
                                if (lis[0].pos.z == lis[1].pos.z || lis[0].pos.x == lis[1].pos.x)
                                {
                                    for (int j = 0; j < 2; ++j)
                                    {
                                        lis[j].deathCount--;

                                    }
                                }
                            }
                        }
                        else
                        {
                            if (lis[0].pos.x == lis[1].pos.x && lis[0].pos.z == lis[1].pos.z)
                            {
                                for (int j = 0; j < 2; ++j)
                                {
                                    if (Mathf.Abs(Mathf.Round(lis[0].pos.y) - Mathf.Round(lis[1].pos.y)) == 1)
                                    {
                                        lis[j].deathCount--;
                                    }
                                }

                            }
                        }
                    }
                }
            }
            //아이스일 때
            if (lis.Count < 6)
            {
                if (sum == 3)
                {
                    if (lis[0].tag == "IceBlock" && lis[1].tag == "IceBlock" && lis[2].tag == "IceBlock" && lis[3].tag == "IceBlock")
                    {
                        if ((int)lis[0].pos.y == (int)lis[1].pos.y && (int)lis[1].pos.y == (int)lis[2].pos.y && (int)lis[2].pos.y == (int)lis[3].pos.y)
                        {
                            if (Mathf.Sqrt(Mathf.Pow((int)lis[0].pos.x - (int)lis[3].pos.x, 2.0f) + Mathf.Pow((int)lis[0].pos.z - (int)lis[3].pos.z, 2.0f)) == Mathf.Sqrt(5))
                            {
                                if (Mathf.Sqrt(Mathf.Pow((int)lis[0].pos.x - (int)lis[2].pos.x, 2.0f) + Mathf.Pow((int)lis[0].pos.z - (int)lis[2].pos.z, 2.0f)) == Mathf.Sqrt(2) &&
                                    Mathf.Sqrt(Mathf.Pow((int)lis[1].pos.x - (int)lis[3].pos.x, 2.0f) + Mathf.Pow((int)lis[1].pos.z - (int)lis[3].pos.z, 2.0f)) == Mathf.Sqrt(2) &&
                                    Mathf.Sqrt(Mathf.Pow((int)lis[2].pos.x - (int)lis[3].pos.x, 2.0f) + Mathf.Pow((int)lis[2].pos.z - (int)lis[3].pos.z, 2.0f)) == (int)1)
                                {
                                    for (int j = 0; j < 4; ++j)
                                    {
                                        lis[j].deathCount--;
                                    }
                                }
                            }
                        }
                        else {
                            if (Mathf.Sqrt(Mathf.Pow((int)lis[0].pos.y - (int)lis[3].pos.y, 2.0f) + Mathf.Pow((int)lis[0].pos.z - (int)lis[3].pos.z, 2.0f)) == Mathf.Sqrt(5))
                            {
                                if (Mathf.Sqrt(Mathf.Pow((int)lis[0].pos.y - (int)lis[2].pos.y, 2.0f) + Mathf.Pow((int)lis[0].pos.z - (int)lis[2].pos.z, 2.0f)) == Mathf.Sqrt(2) &&
                                    Mathf.Sqrt(Mathf.Pow((int)lis[1].pos.y - (int)lis[3].pos.y, 2.0f) + Mathf.Pow((int)lis[1].pos.z - (int)lis[3].pos.z, 2.0f)) == Mathf.Sqrt(2) &&
                                    Mathf.Sqrt(Mathf.Pow((int)lis[2].pos.y - (int)lis[3].pos.y, 2.0f) + Mathf.Pow((int)lis[2].pos.z - (int)lis[3].pos.z, 2.0f)) == (int)1)
                                {
                                    // 파괴 하라 ( 규칙이 맞으면 )
                                    for (int j = 0; j < 4; ++j)
                                    {
                                        lis[j].deathCount--;
                                    }
                                }
                            }
                            else if (Mathf.Sqrt(Mathf.Pow((int)lis[0].pos.x - (int)lis[3].pos.x, 2.0f) + Mathf.Pow((int)lis[0].pos.y - (int)lis[3].pos.y, 2.0f)) == Mathf.Sqrt(5))
                            {
                                if (Mathf.Sqrt(Mathf.Pow((int)lis[0].pos.x - (int)lis[2].pos.x, 2.0f) + Mathf.Pow((int)lis[0].pos.y - (int)lis[2].pos.y, 2.0f)) == Mathf.Sqrt(2) &&
                                    Mathf.Sqrt(Mathf.Pow((int)lis[1].pos.x - (int)lis[3].pos.x, 2.0f) + Mathf.Pow((int)lis[1].pos.y - (int)lis[3].pos.y, 2.0f)) == Mathf.Sqrt(2) &&
                                    Mathf.Sqrt(Mathf.Pow((int)lis[2].pos.x - (int)lis[3].pos.x, 2.0f) + Mathf.Pow((int)lis[2].pos.y - (int)lis[3].pos.y, 2.0f)) == 1)
                                {
                                    // 파괴 하라 ( 규칙이 맞으면 )
                                    for (int j = 0; j < 4; ++j)
                                    {
                                        lis[j].deathCount--;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //용암일 때 (ㅁ자)
            if (lis.Count < 6)
            {
                if (sum == 3)
                {
                    if (lis[0].tag == "LavaBlock" && lis[1].tag == "LavaBlock" && lis[2].tag == "LavaBlock" && lis[3].tag == "LavaBlock")
                    {
                        if ((int)lis[0].pos.y == (int)lis[1].pos.y && (int)lis[1].pos.y == (int)lis[2].pos.y && (int)lis[2].pos.y == (int)lis[3].pos.y)
                        {
                            if (Mathf.Sqrt(Mathf.Pow((int)lis[0].pos.x - (int)lis[3].pos.x, 2.0f) + Mathf.Pow((int)lis[0].pos.z - (int)lis[3].pos.z, 2.0f)) == 1)
                            {
                                // 파괴 하라 ( 규칙이 맞으면 )
                                for (int j = 0; j < 4; ++j)
                                {
                                    lis[j].deathCount--;
                                }
                            }
                        }
                        else {
                            if (Mathf.Sqrt(Mathf.Pow((int)lis[0].pos.y - (int)lis[3].pos.y, 2.0f) + Mathf.Pow((int)lis[0].pos.z - (int)lis[3].pos.z, 2.0f)) == 1)
                            {
                                // 파괴 하라 ( 규칙이 맞으면 )
                                for (int j = 0; j < 4; ++j)
                                {
                                    lis[j].deathCount--;
                                }
                            }
                            else if (Mathf.Sqrt(Mathf.Pow((int)lis[0].pos.x - (int)lis[3].pos.x, 2.0f) + Mathf.Pow((int)lis[0].pos.y - (int)lis[3].pos.y, 2.0f)) == 1)
                            {
                                // 파괴 하라 ( 규칙이 맞으면 )
                                for (int j = 0; j < 4; ++j)
                                {
                                    lis[j].deathCount--;
                                }
                            }
                        }
                    }
                }//색깔이 다르면 ..
            }
        }

        //}
        //else
        //    Debug.Log("다름");
    }

    void ClickBlockUI(int i)
    {
        if ((Instance.failCheck == false) && retry.Instance.pause == false)
        {
            //if (blockCounts[i] > 0)
            //{
            //    blockCounts[i]--;



            GameObject tempBlock = GameObject.Instantiate(blockobjs[i]) as GameObject;
            Vector3 temp = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 0)
                tempBlock.transform.position = new Vector3(temp.x + cosX * (temp.y), 0f, temp.z + cosZ * (temp.y));
            else if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 90)
                tempBlock.transform.position = new Vector3(temp.x + cosX * (temp.y), 0f, temp.z - cosZ * (temp.y));
            else if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 180)
                tempBlock.transform.position = new Vector3(temp.x - cosX * (temp.y), 0f, temp.z - cosZ * (temp.y));
            else if (Mathf.Round(ChallengeGameManager.Instance.cameraPivot.eulerAngles.y) == 270)
                tempBlock.transform.position = new Vector3(temp.x - cosX * (temp.y), 0f, temp.z + cosZ * (temp.y));

            //tempBlock.transform.position = new Vector3(temp.x + cosX * (temp.y ),0f, temp.z + cosZ * (temp.y));

            tempBlock.SendMessage("BlockClick");
            //}
        }
    }

    public void CreateBlockButton()
    {
        int type;
        GameObject temp = Instantiate(blockButton);


        if (blocksquarecheck == false)
        {
            imagetemp = Instantiate(blocksquare);
        }
        blockUI.Add(temp);//리스트에 집어넣음
        blockbutton_count++;


        if (nowStage == 1)
        {

            type = Random.Range(0, 2);
            switch (type)
            {
                case 0:
                    temp.tag = "GrassBlockUI";
                    break;

                case 1:
                    temp.tag = "DesertBlockUI";
                    break;
            }

        }
        else if (nowStage == 2)
        {
            type = Random.Range(0, 3);
            switch (type)
            {
                case 0:
                    temp.tag = "GrassBlockUI";
                    break;
                case 1:
                    temp.tag = "DesertBlockUI";
                    break;
                case 2:
                    temp.tag = "JungleBlockUI";
                    break;
            }
        }
        else if (nowStage == 3)
        {
            type = Random.Range(0, 4);
            switch (type)
            {
                case 0:
                    temp.tag = "GrassBlockUI";
                    break;
                case 1:
                    temp.tag = "DesertBlockUI";
                    break;
                case 2:
                    temp.tag = "JungleBlockUI";
                    break;
                case 3:
                    temp.tag = "LavaBlockUI";
                    break;

            }
        }
        else
        {
            type = Random.Range(0, 5);
            switch (type)
            {
                case 0:
                    temp.tag = "GrassBlockUI";
                    break;
                case 1:
                    temp.tag = "DesertBlockUI";
                    break;
                case 2:
                    temp.tag = "JungleBlockUI";
                    break;
                case 3:
                    temp.tag = "LavaBlockUI";
                    break;
                case 4:
                    temp.tag = "IceBlockUI";
                    break;

            }
        }

        if (blockUI[0].tag == "GrassBlockUI" && blocksquarecheck == false)
        {
            imagetemp.sprite = greensquare;
            imagetemp.transform.SetParent(canvas);
            imagetemp.transform.localPosition = new Vector3(-40f, 395f, 0f);
            imagetemp.transform.localScale = new Vector3(0f, 1f);
            imagetemp.transform.localEulerAngles = Vector3.zero;
            blocksquarecheck = true;
        }
        else if (blockUI[0].tag == "JungleBlockUI" && blocksquarecheck == false)
        {
            imagetemp.sprite = purplesquare;
            imagetemp.transform.SetParent(canvas);
            imagetemp.transform.localPosition = new Vector3(-40f, 395f, 0f);
            imagetemp.transform.localScale = new Vector3(0f, 1f);
            imagetemp.transform.localEulerAngles = Vector3.zero;
            blocksquarecheck = true;
        }
        else if (blockUI[0].tag == "DesertBlockUI" && blocksquarecheck == false)
        {
            imagetemp.sprite = yellowsquare;
            imagetemp.transform.SetParent(canvas);
            imagetemp.transform.localPosition = new Vector3(-40f, 395f, 0f);
            imagetemp.transform.localScale = new Vector3(0f, 1f);
            imagetemp.transform.localEulerAngles = Vector3.zero;
            blocksquarecheck = true;
        }
        else if (blockUI[0].tag == "LavaBlockUI" && blocksquarecheck == false)
        {
            imagetemp.sprite = redsquare;
            imagetemp.transform.SetParent(canvas);
            imagetemp.transform.localPosition = new Vector3(-40f, 395f, 0f);
            imagetemp.transform.localScale = new Vector3(0f, 1f);
            imagetemp.transform.localEulerAngles = Vector3.zero;
            blocksquarecheck = true;
        }
        else if (blockUI[0].tag == "IceBlockUI" && blocksquarecheck == false)//iceblock생기면추가해야함..위에변수도
        {
            imagetemp.sprite = bluesquare;
            imagetemp.transform.SetParent(canvas);
            imagetemp.transform.localPosition = new Vector3(-40f, 395f, 0f);
            imagetemp.transform.localScale = new Vector3(0f, 1f);
            imagetemp.transform.localEulerAngles = Vector3.zero;
            blocksquarecheck = true;
        }



        // if (Random.Range(0f, 1f) <=0.2f)//User.Instance.multiRate[User.Instance.level]
        // {
        //     type = 4;
        //     blockCounts[4]++;
        //     temp.tag = "MultiBlockUI";
        // }
        // else
        // {
        //     type = Random.Range(0, 4);

        //     switch (type)
        //     {
        //         case 0:
        //             blockCounts[0]++;
        //             temp.tag = "GrassBlockUI";
        //             break;
        //         case 1:
        //             blockCounts[1]++;
        //             temp.tag = "JungleBlockUI";
        //             break;
        //         case 2:
        //             blockCounts[2]++;
        //             temp.tag = "DesertBlockUI";
        //             break;
        //         case 3:
        //             blockCounts[3]++;
        //             temp.tag = "LavaBlockUI";
        //             break;

        //     }
        //}


        temp.GetComponent<Image>().sprite = blockSprite[type];
        temp.transform.SetParent(canvas);
        temp.transform.localPosition = new Vector3(110f, 525f, 0f);
        temp.transform.localScale = new Vector3(0f, 0.5f);
        temp.transform.localEulerAngles = Vector3.zero;

        StartCoroutine("blockUIMove");
    }
    IEnumerator blockUIMove()
    {
        int emptyIndex = 0;
        //if (blockUI.Count > 5)
        //{
        //    if(Mathf.Round(blockUI[5].transform.localPosition.x) != 289f)
        //    {
        //        emptyIndex = 5;
        //    }
        //}
        //if (blockUI.Count > 4)
        //{
        //    if (Mathf.Round(blockUI[4].transform.localPosition.x) != 179f)
        //    {
        //        emptyIndex = 4;
        //    }
        //}
        if (blockUI.Count > 3)
        {
            emptyIndex = 3;
            if (Mathf.Round(blockUI[3].transform.localPosition.y) != 525f)
            {
            }
        }
        if (blockUI.Count > 2)
        {
            if (Mathf.Round(blockUI[2].transform.localPosition.y) != 460f)
            {
                emptyIndex = 2;
            }
        }
        if (blockUI.Count > 1)
        {
            if (Mathf.Round(blockUI[1].transform.localPosition.y) != 395f)
            {
                emptyIndex = 1;
            }
        }
        if (blockUI.Count > 0)
        {
            if (Mathf.Round(blockUI[0].transform.localPosition.y) != 490f)
            {
                emptyIndex = 0;
            }
        }
        // print(emptyIndex);

        //if (emptyIndex <= 5 && blockUI.Count > 5)
        //{
        //    blockUI[5].transform.DOScale(0f, 0.3f);
        //}
        //if (emptyIndex <= 4 && blockUI.Count > 4)
        //{
        //    blockUI[4].transform.DOScale(0f, 0.3f);
        //}
        //if (emptyIndex <= 3 && blockUI.Count > 3)
        //{
        //    blockUI[3].transform.DOScale(0f, 0.3f);


        //    float dist = Mathf.Abs(blockUI[3].transform.localPosition.y + 235f);
        //    blockUI[3].transform.DOLocalMoveY(-235f, dist / 160f).SetEase(Ease.Linear);
        //    blockUI[3].transform.DOLocalMoveY(525f, dist / 160f).SetEase(Ease.Linear);

        //}
        //if (emptyIndex <= 2 && blockUI.Count > 2)
        //{
        //      blockUI[2].transform.DOScale(0f, 0.3f);

        //    float dist = Mathf.Abs(blockUI[2].transform.localPosition.y + 85f);
        //   blockUI[2].transform.DOLocalMoveY(460f, dist / 160f).SetEase(Ease.Linear);
        //}
        //if (emptyIndex <= 1 && blockUI.Count > 1)
        //{
        //     blockUI[1].transform.DOScale(0f, 0.3f);


        //    float dist = Mathf.Abs(blockUI[2].transform.localPosition.y + 85f);
        //    blockUI[2].transform.DOLocalMoveY(395f, dist / 160f).SetEase(Ease.Linear);
        //}
        if (emptyIndex <= 0 && blockUI.Count > 0)
        {
            blockUI[0].transform.DOScale(0f, 0.3f);
            imagetemp.transform.DOScale(0f, 0.3f);
        }
        //축소 되었다가 ... 
        //yield return new WaitForSeconds(0.3f);

        if (blockUI.Count > 0)
        {
            float dist = Mathf.Abs(blockUI[0].transform.localPosition.y - 225f);
            blockUI[0].transform.DOLocalMoveX(-40f, 0f).SetEase(Ease.Linear);//dist / 160f
            blockUI[0].transform.DOLocalMoveY(490f, 0f).SetEase(Ease.Linear);//dist / 160f
            blockUI[0].transform.DOScale(1.2f, 0.3f);
            imagetemp.transform.DOScale(0.7f, 0.3f);
        }
        if (blockUI.Count > 1)
        {
            float dist = Mathf.Abs(blockUI[1].transform.localPosition.y - 75f);
            //blockUI[1].transform.DOLocalMoveX(110f, 0.5f).SetEase(Ease.Linear);
            blockUI[1].transform.DOLocalMoveY(395f, 0.5f).SetEase(Ease.Linear);
            blockUI[1].transform.DOScale(0.7f, 0.3f);
        }
        if (blockUI.Count > 2)
        {
            float dist = Mathf.Abs(blockUI[2].transform.localPosition.y + 85f);
            //blockUI[2].transform.DOLocalMoveX(110f, 1f).SetEase(Ease.Linear);
            blockUI[2].transform.DOLocalMoveY(460f, 0.5f).SetEase(Ease.Linear);
            blockUI[2].transform.DOScale(0.7f, 0.3f);
        }
        if (blockUI.Count > 3)
        {
            yield return new WaitForSeconds(0.4f);
            //blockUI[3].transform.DOLocalMoveX(110f, 1f).SetEase(Ease.Linear);
            blockUI[3].transform.DOLocalMoveY(525f, 0.5f).SetEase(Ease.Linear);
            blockUI[3].transform.DOScale(0.7f, 0.3f);
        }
    }

    IEnumerator CreateBlockButtonRoutine()
    {
        while (true)
        {
            if (gamestart)
            {
                //if (blockbutton_count <= 3)
                if (blockbutton_count <= 3)
                    CreateBlockButton();
                yield return null;
 //               yield return new WaitForSeconds(0.5f);//블럭유아이생기는시간
            }
            else
            {
                yield return null;
            }
        }
    }


    public bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    public void RotateRight()
    {
        if (DOTween.IsTweening(cameraPivot) == false)
        {
            gameCamera.parent = cameraPivot;
            cameraPivot.DORotate(cameraPivot.eulerAngles + Vector3.up * 90f, 0.3f).SetEase(Ease.Linear).OnComplete(() => { gameCamera.parent = null; print("Right"); });
        }
    }
    public void RotateLeft()
    {
        if (DOTween.IsTweening(cameraPivot) == false)
        {
            gameCamera.parent = cameraPivot;
            cameraPivot.DORotate(cameraPivot.eulerAngles - Vector3.up * 90f, 0.3f).SetEase(Ease.Linear).OnComplete(() => { gameCamera.parent = null; print("Left"); });
        }
    }
}