﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CameraPivot : MonoBehaviour {
    
    public Transform gameCamera;
    
    Transform _transform;
    void Start()
    {
        _transform = this.transform;
        StartCoroutine("MyUpdate");

    }
    IEnumerator MyUpdate()
    {
        while (true)
        {
            if (ChallengeGameManager.Instance != null)
            {
                if (ChallengeGameManager.Instance.nowStage == 1)
                {
                    _transform.position = new Vector3(ChallengeGameManager.Instance.nowStage, 0, ChallengeGameManager.Instance.nowStage);
                }
                else if (ChallengeGameManager.Instance.nowStage == 2)
                {
                    _transform.position = new Vector3(ChallengeGameManager.Instance.nowStage - 0.5f, 0, ChallengeGameManager.Instance.nowStage - 0.5f);
                }
                else if (ChallengeGameManager.Instance.nowStage == 3)
                {
                    _transform.position = new Vector3(ChallengeGameManager.Instance.nowStage - 1f, 0, ChallengeGameManager.Instance.nowStage - 1f);
                }
                else if (ChallengeGameManager.Instance.nowStage == 4)
                {
                    _transform.position = new Vector3(ChallengeGameManager.Instance.nowStage - 1.5f, 0, ChallengeGameManager.Instance.nowStage - 1.5f);
                }
                else if (ChallengeGameManager.Instance.nowStage == 5)
                {
                    _transform.position = new Vector3(ChallengeGameManager.Instance.nowStage - 2f, 0, ChallengeGameManager.Instance.nowStage - 2f);
                }
            }

            if(GameManager.Instance != null)
            {
                if(GameManager.Instance.maxSize == 3)
                {
                    _transform.position = new Vector3(1, 0, 1);
                }
                else if(GameManager.Instance.maxSize == 4)
                {
                    _transform.position = new Vector3(1.5f, 0, 1.5f);
                }
                else if (GameManager.Instance.maxSize == 5)
                {
                    _transform.position = new Vector3(2, 0, 2);
                }
                else if (GameManager.Instance.maxSize == 6)
                {

                    _transform.position = new Vector3(2.5f, 0, 2.5f);
                }
                else if (GameManager.Instance.maxSize == 7)
                {

                    _transform.position = new Vector3(3, 0, 3);
                }
            }

            yield return true;
        }
    }
    public void RotateRight()
    {
        if (DOTween.IsTweening(_transform) == false)
        {
            gameCamera.parent = _transform;
            _transform.DORotate(_transform.eulerAngles + Vector3.up * 90f, 0.3f).SetEase(Ease.Linear).OnComplete(()=> { gameCamera.parent = null; print("Right"); });
        }
    }
    public void RotateLeft()
    {
        if (DOTween.IsTweening(_transform) == false)
        {
            gameCamera.parent = _transform;
            _transform.DORotate(_transform.eulerAngles - Vector3.up * 90f, 0.3f).SetEase(Ease.Linear).OnComplete(() => { gameCamera.parent = null; print("Left"); });
        }
    }
}
