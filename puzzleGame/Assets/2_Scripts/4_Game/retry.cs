﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

using System.Collections.Generic;
using UnityEngine.EventSystems;


public class retry : Singleton<retry>
{
    public GameObject Stop;
    public Image pause1;
    public Image pause2;
    public Image pause3;
    public Image pause4;
    public Image pause5;
    public Image pause6;
    public Image pause7;

    public CanvasGroup gameover;
    public Image startimage;

    public bool pause = false;
    void Start()
    {
        StartCoroutine("updateRoutine");
    }
    IEnumerator updateRoutine()
    {
        while (true)
        {
            if (Input.GetKeyDown(KeyCode.Escape))//뒤로
            {
                stageretry();
            }
            yield return null;
        }
    }

    void Update()
    {
        if (pause == true)
        {
            Time.timeScale = 0.0f;
        }
        else
        {
            Time.timeScale = 1.0f;
        }
    }

    public void stageretry()
    {
        if (Stop.activeSelf == false)
        {
            if (GameManager.Instance != null && GameManager.Instance.failCheck == false)
            {
                GameManager.Instance.GameStart = false;

                Stop.SetActive(true);

                pause1.DOFade(1f, 0.3f);
                pause2.DOFade(1f, 0.3f).SetDelay(0.1f);
                pause3.GetComponent<CanvasGroup>().DOFade(1f, 0.3f).SetDelay(0.2f);
                pause4.GetComponent<CanvasGroup>().DOFade(1f, 0.3f).SetDelay(0.3f);
                pause5.GetComponent<CanvasGroup>().DOFade(1f, 0.3f).SetDelay(0.4f);
                pause6.DOFade(1f, 0.3f).SetDelay(0.5f);
                pause7.GetComponent<CanvasGroup>().DOFade(1f, 0.3f).SetDelay(0.6f).OnComplete(() => pause = true);
            }
            else if(ChallengeGameManager.Instance != null && ChallengeGameManager.Instance.failCheck == false)
            {
                Stop.SetActive(true);

                pause1.DOFade(1f, 0.3f);
                pause2.DOFade(1f, 0.3f).SetDelay(0.1f);
                pause3.GetComponent<CanvasGroup>().DOFade(1f, 0.3f).SetDelay(0.2f);
                pause4.GetComponent<CanvasGroup>().DOFade(1f, 0.3f).SetDelay(0.3f);
                pause5.GetComponent<CanvasGroup>().DOFade(1f, 0.3f).SetDelay(0.4f);
                pause6.DOFade(1f, 0.3f).SetDelay(0.5f);
                pause7.GetComponent<CanvasGroup>().DOFade(1f, 0.3f).SetDelay(0.6f).OnComplete(() => pause = true);
            }

            //pause = true;
        }
    }
    public void replay()
    {
        if (pause)
        {
            pause = false;

            pause7.GetComponent<CanvasGroup>().DOFade(0f, 0.3f);
            pause6.DOFade(0f, 0.3f).SetDelay(0.1f);
            pause5.GetComponent<CanvasGroup>().DOFade(0f, 0.3f).SetDelay(0.2f);
            pause4.GetComponent<CanvasGroup>().DOFade(0f, 0.3f).SetDelay(0.3f);
            pause3.GetComponent<CanvasGroup>().DOFade(0f, 0.3f).SetDelay(0.4f);
            pause2.DOFade(0f, 0.3f).SetDelay(0.5f);
            pause1.DOFade(0f, 0.3f).SetDelay(0.6f);
             startimage.gameObject.SetActive(true);
            startimage.DOFade(1f, 1f).SetDelay(1f).OnComplete(() => { if (GameManager.Instance != null) GameManager.Instance.GameStart = true;  Application.LoadLevel(Application.loadedLevelName); });
        }
        else
        {
            gameover.DOFade(0f, 1f);
            startimage.gameObject.SetActive(true);
            startimage.DOFade(1f, 1f).SetDelay(1f).OnComplete(()=> {
                if (GameManager.Instance != null)
                    GameManager.Instance.GameStart = true;
                Application.LoadLevel(Application.loadedLevelName);
        });
         

        }
    }
    public void retrycontinue()
    {
        if (pause)
        {
            pause = false;

            pause7.GetComponent<CanvasGroup>().DOFade(0f, 0.3f);
            pause6.DOFade(0f, 0.3f).SetDelay(0.1f);
            pause5.GetComponent<CanvasGroup>().DOFade(0f, 0.3f).SetDelay(0.2f);
            pause4.GetComponent<CanvasGroup>().DOFade(0f, 0.3f).SetDelay(0.3f);
            pause3.GetComponent<CanvasGroup>().DOFade(0f, 0.3f).SetDelay(0.4f);
            pause2.DOFade(0f, 0.3f).SetDelay(0.5f);
            pause1.DOFade(0f, 0.3f).SetDelay(0.6f).OnComplete(() => { if (GameManager.Instance != null) GameManager.Instance.GameStart = true; Stop.SetActive(false); });
        }
    }
    public void Homebutton()
    {
        if (pause)
        {
            if(GameManager.Instance != null)
                GameManager.Instance.startimage.SetActive(true);
            else if (ChallengeGameManager.Instance != null)
                ChallengeGameManager.Instance.background.gameObject.SetActive(true);

            pause = false;

            pause7.GetComponent<CanvasGroup>().DOFade(0f, 0.3f);
            pause6.DOFade(0f, 0.3f).SetDelay(0.1f);
            pause5.GetComponent<CanvasGroup>().DOFade(0f, 0.3f).SetDelay(0.2f);
            pause4.GetComponent<CanvasGroup>().DOFade(0f, 0.3f).SetDelay(0.3f);
            pause3.GetComponent<CanvasGroup>().DOFade(0f, 0.3f).SetDelay(0.4f);
            pause2.DOFade(0f, 0.3f).SetDelay(0.5f);
            pause1.DOFade(0f, 0.3f).SetDelay(0.6f);

            if (GameManager.Instance != null)
            {
                GameManager.Instance.startimage.GetComponent<Image>().DOFade(1f, 1f).SetDelay(1f).OnComplete(() =>
                {
                    if (GameManager.Instance != null)
                        GameManager.Instance.GameStart = true;
                    Stop.SetActive(false);
                    SceneManager.Instance.SetNextSceneName("1_Intro");
                    Application.LoadLevel("4_Loading");
                });
            }
            else if(ChallengeGameManager.Instance != null)
            {
                ChallengeGameManager.Instance.background.DOFade(1f, 1f).SetDelay(1f).OnComplete(() =>
                {
                    if (GameManager.Instance != null)
                        GameManager.Instance.GameStart = true;
                    Stop.SetActive(false);
                    SceneManager.Instance.SetNextSceneName("1_Intro");
                    Application.LoadLevel("4_Loading");
                });
            }
        }

    }
    public void clearHomebutton()
    {
        if (GameManager.Instance.cleareffect.alpha > 0.9f)
        {
            GameManager.Instance.cleareffect.DOKill();

            GameManager.Instance.startimage.SetActive(true);
            GameManager.Instance.startimage.GetComponent<Image>().DOFade(1f, 0.5f);
            GameManager.Instance.clearbackground.DOFade(0f, 2f);
            GameManager.Instance.cleareffect.DOFade(0f, 1f).SetDelay(1f).OnComplete(() =>
            {
                if (GameManager.Instance != null)
                    GameManager.Instance.GameStart = true;
                Stop.SetActive(false);
                SceneManager.Instance.SetNextSceneName("1_Intro");
                Application.LoadLevel("4_Loading");
            });
        }
    }
}
