﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class StageLobbyManager : Singleton<StageLobbyManager>
{
    public Transform background;

    #region Swipe

    Vector3 curClickPos, prevClickPos;
    float distanceX, distanceY;

    #endregion

    void Start()
    {
        StartCoroutine("updateRoutine");
    }

    IEnumerator updateRoutine()
    {
        while (true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                curClickPos = Input.mousePosition;
                prevClickPos = curClickPos;
            }
            else if (Input.GetMouseButton(0))
            {
                if (curClickPos.y > Screen.height * 0.9f)
                {
                    if (Application.platform == RuntimePlatform.WindowsEditor || Input.touchCount == 1)
                    {
                        curClickPos = Input.mousePosition;
                        distanceX = curClickPos.x - prevClickPos.x;
                        distanceY = curClickPos.y - prevClickPos.y;

                        if (distanceY < -12f && Mathf.Abs(distanceX) < 10f)
                        {
                            //Down
                            WholeView();
                            print("distanceX:" + distanceX);
                            print("distanceY:" + distanceY);
                        }
                        prevClickPos = curClickPos;
                    }
                }
                else
                {
                    if (Application.platform == RuntimePlatform.WindowsEditor || Input.touchCount == 1)
                    {
                        curClickPos = Input.mousePosition;
                        distanceX = curClickPos.x - prevClickPos.x;
                        distanceY = curClickPos.y - prevClickPos.y;

                        if (distanceY > 12f && Mathf.Abs(distanceX) < 10f)
                        {
                            //Up
                            ViewReturn();

                        }
                        else if (distanceX > 12f && Mathf.Abs(distanceY) < 10f)
                        {
                            if (DOTween.IsTweening(background) == false)
                            {
                                background.DORotate(new Vector3(0f, 0f, 30f), 1f, RotateMode.WorldAxisAdd);

                                print("Rotate_distanceX:" + distanceX);
                                print("Rotate_distanceY:" + distanceY);
                            }
                        }
                        else if (distanceX < -12f && Mathf.Abs(distanceY) < 10f)
                        {
                            if (DOTween.IsTweening(background) == false)
                            {
                                background.DORotate(new Vector3(0f, 0f, -30f), 1f, RotateMode.WorldAxisAdd);
                            }
                        }
                        prevClickPos = curClickPos;
                    }
                }
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.LoadLevel("2_Lobby");
            }
            yield return null; 

        }
    }

    public void EnterStage(int stage)
    {
        if (DOTween.IsTweening(background) == false)
        {
            SceneManager.Instance.SetNextSceneName("Stage_" + stage);
            Application.LoadLevel("4_Loading");
        }
    }
    public void Infinity()
    {
        Application.LoadLevel("Challenge");
    }

    public void WholeView()
    {
        if (DOTween.IsTweening(background) == false)
        {
            background.DOLocalMoveY(0f, 1f);
            background.DOScale(0.2f, 1f);
        }
    }
    public void ViewReturn()
    {
        if (DOTween.IsTweening(background) == false)
        {
            background.DOLocalMoveY(820f, 1f);
            background.DOScale(1f, 1f);
        }
    }
}
