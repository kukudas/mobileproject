﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class IntroManager : Singleton<IntroManager> {

    public GameObject obj;
    public Transform background;
    public Transform zodiac;
    public GameObject sun2;
    public Transform intro_text;
    public Transform intro_text2;
    public Image[] stagebutton;
    public Image[] stagebutton_;
    public Image[] blacklock;
    public GameObject[] stagestartbutton;
    public Image challengebackground;
    public Image challengebackgroundbutton;
    public Transform C_buttonbounce;
    public Image loadingBackground;
    public Sprite[] loadingSprite;


    bool click = false;
    bool click2 = false;
    bool click3 = false;
    float speed;
    bool swipe = false;
    

    Vector3 curClickPos, prevClickPos;
    float distanceX, distanceY;

    void Start()
    {
        SceneManager.Instance.selectStageIndex = -1;
        speed = 10f;
        StartCoroutine("updateRoutine");

        sun2.SetActive(false);


        if(SceneManager.Instance.GetIsFirst())
        {
            intro_text.gameObject.SetActive(true);
            intro_text2.gameObject.SetActive(true);
            zodiac.localScale = Vector3.one;
            obj.SetActive(false);
            click = false;
            SceneManager.Instance.SetIsFirst(false);
        }
        else
        {
            intro_text.gameObject.SetActive(false);
            intro_text2.gameObject.SetActive(false);

            zodiac.DOScale(1.3f, 1f).OnComplete(() => { obj.SetActive(true); print("OnComplete"); });
            for (int i = 0; i < 12; ++i)
            {
                print("User.Instance.Stage[" + i + "]" + User.Instance.Stage[i]);
                if (User.Instance.Stage[i] == 0)
                {
                    //stagebutton[i].SetActive(false);
                    //stagebutton_[i].SetActive(true);

                    StartCoroutine(ImageAlpha(stagebutton_[i], 1));
                    StartCoroutine(ImageAlpha(stagebutton[i], 0));
                    if (User.Instance.Stage[i - 1] != 0 && User.Instance.Stage[i] == 0)
                        StartCoroutine(ImageAlpha(blacklock[i - 1], 1));

                    // blacklock.SetActive(true);
                }
                else if (User.Instance.Stage[i] == 1)
                {
                    StartCoroutine(ImageAlpha(stagebutton_[i], 1));
                    StartCoroutine(ImageAlpha(stagebutton[i], 0));
                    StartCoroutine(ImageAlpha(blacklock[i], 0));

                    stagestartbutton[i].SetActive(true);
                    // blacklock.SetActive(false);

                }
                else if (User.Instance.Stage[i] == 2)
                {
                    StartCoroutine(ImageAlpha(stagebutton_[i], 0));
                    StartCoroutine(ImageAlpha(stagebutton[i], 1));
                    StartCoroutine(ImageAlpha(blacklock[i], 0));

                    stagestartbutton[i].SetActive(true);
                }
            }
            stagestartbutton[12].SetActive(true);
            //obj.SetActive(true);
            click = true;
        }
    }

    IEnumerator updateRoutine()
    {
        while(true)
        {
            background.transform.Rotate(Vector3.back, Time.smoothDeltaTime * speed);
            zodiac.transform.Rotate(Vector3.forward, Time.smoothDeltaTime * speed);

            if(Input.GetKeyDown(KeyCode.Escape))
            {
                if (click2)
                {
                    click2 = false;
                    background.DOLocalMoveY(0f, 1f);
                    background.DOScale(1f, 1f);
                    zodiac.DOLocalMoveY(0f, 1f);
                    zodiac.DOScale(1.3f, 1f).OnComplete(()=> { obj.SetActive(true);});
                    speed = 10f;
                    SceneManager.Instance.selectStageIndex = -1;
                }
                else if (click3)
                {
                    StartCoroutine("ChallengebackCoroutine");
                }
                else if (click == true && speed != 0)
                {
                    print("종료");
                    Application.Quit();
                }
            }

            if (Input.GetMouseButtonUp(0) && click==false)//첫번째 확대
            {
                //Application.LoadLevel("2_Lobby");
                sun2.SetActive(true);
                intro_text.gameObject.SetActive(false);
                intro_text2.gameObject.SetActive(false);

                zodiac.DOScale(1.3f, 1f).OnComplete(() => { obj.SetActive(true); print("OnComplete"); });
                for (int i = 0; i < 12; ++i)
                {
                    print("User.Instance.Stage[" + i + "]" + User.Instance.Stage[i]);
                    if (User.Instance.Stage[i] == 0)
                    {
                        //stagebutton[i].SetActive(false);
                        //stagebutton_[i].SetActive(true);

                        stagebutton[i].DOFade(0f, 1f);
                        stagebutton_[i].DOFade(1f, 1f);
                        //StartCoroutine(ImageAlpha(stagebutton_[i], 1));
                        //StartCoroutine(ImageAlpha(stagebutton[i], 0));
                        if (User.Instance.Stage[i - 1] != 0 && User.Instance.Stage[i] == 0)
                        {
                            //StartCoroutine(ImageAlpha(blacklock[i - 1], 1));
                            blacklock[i-1].DOFade(1f, 1f);
                        }
                        // blacklock.SetActive(true);
                    }
                    else if (User.Instance.Stage[i] == 1)
                    {
                        stagebutton[i].DOFade(0f, 1f);
                        stagebutton_[i].DOFade(1f, 1f);
                        blacklock[i].DOFade(0f, 1f);
                        StartCoroutine(ImageAlpha(stagebutton_[i], 1));
                        StartCoroutine(ImageAlpha(stagebutton[i], 0));
                        StartCoroutine(ImageAlpha(blacklock[i], 0));

                        stagestartbutton[i].SetActive(true);
                        // blacklock.SetActive(false);

                    }
                    else if (User.Instance.Stage[i] == 2)
                    {
                        print(i);
                        stagebutton[i].DOFade(1f,1f);
                        stagebutton_[i].DOFade(0f, 1f);
                        blacklock[i].DOFade(0f, 1f);
                        //StartCoroutine(ImageAlpha(stagebutton_[i], 0));
                        //StartCoroutine(ImageAlpha(stagebutton[i], 1));
                        //StartCoroutine(ImageAlpha(blacklock[i], 0));

                        stagestartbutton[i].SetActive(true);
                    }
                }
                stagestartbutton[12].SetActive(true);
                //obj.SetActive(true);
                click = true;
            }

            if (Input.GetMouseButtonDown(0) && click == true && click2 == true)
            {
                curClickPos = Input.mousePosition;
                prevClickPos = curClickPos;
            }

            if (Input.GetMouseButton(0) && click == true && click2 == true)
            {
                if (Application.platform == RuntimePlatform.WindowsEditor || Input.touchCount == 1)
                {
                    curClickPos = Input.mousePosition;
                    distanceX = curClickPos.x - prevClickPos.x;
                    distanceY = curClickPos.y - prevClickPos.y;
                    if (distanceX > 12f && Mathf.Abs(distanceY) < 10f)
                    {
                        if (DOTween.IsTweening(background) == false&&swipe==false)
                        {
                            swipe = true;
                            background.DORotate(new Vector3(0f, 0f, 30f), 1f, RotateMode.WorldAxisAdd);
                            zodiac.DORotate(new Vector3(0f, 0f, 30f), 1f, RotateMode.WorldAxisAdd);
                            
                            if(SceneManager.Instance.selectStageIndex < 12)
                            {
                                SceneManager.Instance.selectStageIndex++;
                            }
                            else
                            {
                                SceneManager.Instance.selectStageIndex = 1;
                            }
                        }
                    }
                    else if (distanceX < -12f && Mathf.Abs(distanceY) < 10f)
                    {
                        if (DOTween.IsTweening(background) == false && swipe == false)
                        {
                            swipe = true;
                            background.DORotate(new Vector3(0f, 0f, -30f), 1f, RotateMode.WorldAxisAdd);
                            zodiac.DORotate(new Vector3(0f, 0f, -30f), 1f, RotateMode.WorldAxisAdd);
                            if (SceneManager.Instance.selectStageIndex > 1)
                            {
                                SceneManager.Instance.selectStageIndex--;
                            }
                            else
                            {
                                SceneManager.Instance.selectStageIndex = 12;
                            }
                        }
                    }
                    prevClickPos = curClickPos;
                }
            }
            if (Input.GetMouseButtonUp(0) && click == true && click2 == true)
                swipe = false;
            yield return null;
        }
    }
    IEnumerator ChallengebackCoroutine()
    {

        challengebackgroundbutton.GetComponent<CanvasGroup>().DOFade(0f, 1f);
        yield return new WaitForSeconds(0.5f);
        challengebackground.GetComponent<CanvasGroup>().DOFade(0f, 1f);
        yield return new WaitForSeconds(0.8f);
        challengebackground.gameObject.SetActive(false);
        background.GetComponent<Image>().DOFade(1f, 1f);
        yield return new WaitForSeconds(0.2f);
        zodiac.GetComponent<CanvasGroup>().DOFade(1f, 0.5f);
        yield return new WaitForSeconds(0.1f);
      

    }
    public void select(int index)
    {

        if (DOTween.IsTweening(zodiac) == false)
        {
            if (index > 0)
            {
                click2 = true;
                loadingBackground.sprite = loadingSprite[0];
                if (click == true && SceneManager.Instance.selectStageIndex != index)
                {
                    float z = ((360f + (30f * (index-1))) - ((zodiac.eulerAngles.z) % 360)) % 360;
                    zodiac.DORotate(new Vector3(0, 0, z), z / 360f * 2f, RotateMode.WorldAxisAdd).OnComplete( ()=> ZoomIn(index));
                    speed = 0;
                    SceneManager.Instance.selectStageIndex = index;
                }
                else if (click2 == true && SceneManager.Instance.selectStageIndex == index)
                {

                    // sun.GetComponent<Image>().DOFade(0f, 1f).OnComplete(() => EnterStage(index));
                    StartCoroutine(EnterstageFade(index));

                    // EnterStage(index);
                }
            }
            else
            {
                click3 = true;
                obj.SetActive(false);
                loadingBackground.sprite = loadingSprite[1];
                StartCoroutine(EnterstageFade(index));
            }
        }
    }
    IEnumerator EnterstageFade(int index)
    {

        zodiac.GetComponent<CanvasGroup>().DOFade(0f, 0.5f);
        yield return new WaitForSeconds(0.3f);
        background.GetComponent<Image>().DOFade(0f, 1f);
        yield return new WaitForSeconds(1f);
        EnterStage(index);
    }

    void ZoomIn(int index)
    {
        StartCoroutine(EnterstageFade(index));
    }

    IEnumerator ImageAlpha(Image image, float value)
    {
        if (image.color.a < value)
        {
            while (image.color.a < value)
            {
                image.color += new Color(0, 0, 0, 1f * Time.deltaTime);
                yield return null;
            }
        }
        else
        {
            while (image.color.a > value)
            {
                image.color -= new Color(0, 0, 0, 1f * Time.deltaTime);
                yield return null;
            }
        }
    }

    public void EnterStage(int stage)
    {
        Debug.Log("stage = "+stage);
        if (stage > 0)
        {
            SceneManager.Instance.SetNextSceneName("Stage_" + stage);
            Application.LoadLevel("4_Loading");
        }
        else
        {
            StartCoroutine("ChallengeCoroutine");
        }
    }
    IEnumerator ChallengeCoroutine()
    {
        challengebackground.gameObject.SetActive(true);
        challengebackground.GetComponent<CanvasGroup>().DOFade(1f, 1f);
        yield return new WaitForSeconds(0.5f);
        challengebackgroundbutton.GetComponent<CanvasGroup>().DOFade(1f, 1f);
        C_buttonbounce.DOScale(C_buttonbounce.localScale * 1.1f, 0.7f).SetLoops(-1, LoopType.Yoyo);


    }
    public void ChallengeStartButtonSet()
    {
        StartCoroutine("ChallengeStartButtonCoroutine");
       
    }
    IEnumerator ChallengeStartButtonCoroutine()
    {
        challengebackgroundbutton.GetComponent<CanvasGroup>().DOFade(0f, 1f);
        yield return new WaitForSeconds(1f);
        Application.LoadLevel("Challenge");
    }
}
