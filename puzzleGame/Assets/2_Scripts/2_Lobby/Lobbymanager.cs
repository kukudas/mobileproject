﻿using UnityEngine;
using System.Collections;

public class Lobbymanager : Singleton<Lobbymanager>
{
    void Start()
    {
        StartCoroutine("updateRoutine");
    }

    IEnumerator updateRoutine()
    {
        while (true)
        {
            if (Input.GetKeyDown(KeyCode.Escape))//뒤로
            {
                Application.LoadLevel("1_Intro");
            }
           yield return null;
        }
    }

    public void StageLobby()
    {
        Application.LoadLevel("1_Intro");
    }

    public void Store()
    {
        Application.LoadLevel("6_Store");
    }
}
