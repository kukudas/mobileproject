﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Threading;
using DG.Tweening;

public class InstBlockMap : MonoBehaviour
{
    public GameObject[] blocks;
    [System.NonSerialized]
    public int Stage1;
    [System.NonSerialized]
    public int Stage2;
    [System.NonSerialized]
    public int Stage3;
    [System.NonSerialized]
    public int Stage4;
    [System.NonSerialized]
    public int StageAfter4;

    public int stage1_Time;
    public int stage2_Time;
    public int stage3_Time;
    public int stage4_Time;
    public float stage1_BlockSpeed;
    public float stage2_BlockSpeed;
    public float stage3_BlockSpeed;
    public float stage4_BlockSpeed;
    public float stage5_BlockSpeed;


    List<GameObject> myBlocks = new List<GameObject>();
    private int myBlocksCount;
    private float selfTimer;
    private float startTimer;
    private bool initCheck = false;
    private float speed;
    private int randomCount;
    private float[] stepIndex = new float[10000]; // 1 스텝 가는 인덱스
    private int[] compCheck = new int[36];
    //    private bool 
    private int maxY;
    private bool comp;

    private float instTime;

    // test 변수
    private float max = 1;

    private int gridMax = 9;

    // Use this for initialization
    void Start()
    {
        startTimer = Time.time;
        selfTimer = Time.time + 0.7f;
        maxY = 4;
        for (int i = 0; i < stepIndex.Length; ++i)
            stepIndex[i] = 0;
        instTime = 1.0f;
        StartCoroutine("MyUpdate");
    }

    IEnumerator MyUpdate()
    {
        while (true)
        {
            if (ChallengeGameManager.Instance.failCheck == false && ChallengeGameManager.Instance.gamestart)
            {
                if (selfTimer - startTimer < stage1_Time) // 20초까진 2개
                {

                    gridMax = 9;
                    ChallengeGameManager.Instance.nowStage = 1;
                    ChallengeGameManager.Instance.changeStage = true;
                    if (selfTimer < Time.time)
                    {
                        comp = false;
                        myBlocks.Clear();

                        Stage1 = Random.Range(0, 2);
                        blocks[Stage1].transform.position = new Vector3(Random.Range(0, 3), -4, Random.Range(0, 3));
                        GameObject tempObj = Instantiate<Object>(blocks[Stage1]) as GameObject;
                        ChallengeGameManager.Instance.nowBlock++;
                        myBlocks.Add(tempObj);
                        ChallengeGameManager.Instance.saveBlocks.Add(tempObj);
                        ///////////////// floorfind 맞추기 
                        StateInit();
                        selfTimer = Time.time + stage1_BlockSpeed;
                    }
                }


                else if (selfTimer - startTimer >= stage1_Time && selfTimer - startTimer < stage2_Time+stage1_Time)
                {
                    gridMax = 16;
                    ChallengeGameManager.Instance.nowStage = 2;
                    ChallengeGameManager.Instance.changeStage = true;

                    if (selfTimer < Time.time)
                    {
                        comp = false;
                        myBlocks.Clear();

                        Stage2 = Random.Range(0, 3);
                        blocks[Stage2].transform.position = new Vector3(Random.Range(0, 4), 0, Random.Range(0, 4));
                        GameObject tempObj = Instantiate<Object>(blocks[Stage2]) as GameObject;
                        ChallengeGameManager.Instance.nowBlock++;

                        myBlocks.Add(tempObj);
                        ChallengeGameManager.Instance.saveBlocks.Add(tempObj);
                        ///////////////// floorfind 맞추기 
                        StateInit();
                        //카메라 사이즈 up
                        //ChallengeGameManager.Instance.zlengthFind = 4;
                        //ChallengeGameManager.Instance.xlengthFind = 4;
                        selfTimer = Time.time + stage2_BlockSpeed;// - 0.1f;
                    }
                }
                else if (selfTimer - startTimer >= stage2_Time && selfTimer - startTimer < stage3_Time + stage2_Time + stage1_Time)
                {
                    gridMax = 25;
                    ChallengeGameManager.Instance.nowStage = 3;
                    ChallengeGameManager.Instance.changeStage = true;

                    if (selfTimer < Time.time)
                    {
                        comp = false;
                        myBlocks.Clear();

                        Stage3 = Random.Range(0, 4);


                        blocks[Stage3].transform.position = new Vector3(Random.Range(0, 5), 0, Random.Range(0, 5));

                        GameObject tempObj = Instantiate<Object>(blocks[Stage3]) as GameObject;
                        ChallengeGameManager.Instance.nowBlock++;

                        myBlocks.Add(tempObj);
                        ///////////////// floorfind 맞추기 
                        StateInit();
                        //카메라 사이즈 up
                        //ChallengeGameManager.Instance.zlengthFind = 5;
                        //ChallengeGameManager.Instance.xlengthFind = 5;
                        selfTimer = Time.time + stage3_BlockSpeed;// - 0.2f;
                    }
                }
                else if (selfTimer - startTimer >= stage3_Time && selfTimer - startTimer < stage4_Time+ stage3_Time + stage2_Time + stage1_Time)
                {
                    gridMax = 36;
                    ChallengeGameManager.Instance.nowStage = 4;
                    ChallengeGameManager.Instance.changeStage = true;

                    Debug.Log("60~80");
                    if (selfTimer < Time.time)
                    {
                        comp = false;
                        myBlocks.Clear();

                        Stage4 = Random.Range(0, 5);


                        blocks[Stage4].transform.position = new Vector3(Random.Range(0, 6), 0, Random.Range(0, 6));

                        GameObject tempObj = Instantiate<Object>(blocks[Stage4]) as GameObject;
                        ChallengeGameManager.Instance.nowBlock++;

                        myBlocks.Add(tempObj);
                        ///////////////// floorfind 맞추기 
                        StateInit();
                        //카메라 사이즈 up
                        //ChallengeGameManager.Instance.zlengthFind = 6;
                        //ChallengeGameManager.Instance.xlengthFind = 6;
                        selfTimer = Time.time + stage4_BlockSpeed;// - 0.3f;
                    }
                }
                else if (selfTimer - startTimer >= stage4_Time + stage3_Time + stage2_Time + stage1_Time)// && selfTimer - startTimer < stage5_Time)
                {
                    gridMax = 49;
                    ChallengeGameManager.Instance.nowStage = 5;
                    ChallengeGameManager.Instance.changeStage = true;

                    if (selfTimer < Time.time)
                    {
                        comp = false;
                        myBlocks.Clear();
                        StageAfter4 = Random.Range(0, 5);


                        blocks[StageAfter4].transform.position = new Vector3(Random.Range(0, 7), 0, Random.Range(0, 7));

                        GameObject tempObj = Instantiate<Object>(blocks[StageAfter4]) as GameObject;
                        ChallengeGameManager.Instance.nowBlock++;

                        myBlocks.Add(tempObj);
                        ///////////////// floorfind 맞추기 
                        StateInit();
                        //카메라 사이즈 up
                        //ChallengeGameManager.Instance.zlengthFind = 7;
                        //ChallengeGameManager.Instance.xlengthFind = 7;
                        selfTimer = Time.time + stage5_BlockSpeed;// - 0.4f;
                    }
                }
                // 여기에 한칸 씩 올라오는 거 ... 
                //if (ChallengeGameManager.Instance.boom == true)
                ////{
                //for (int i = 0; i < ChallengeGameManager.Instance.saveBloc    ks.Count; ++i)
                //{
                //    int x = (int)ChallengeGameManager.Instance.saveBlocks[i].transform.position.x;
                //    int z = (int)ChallengeGameManager.Instance.saveBlocks[i].transform.position.z;
                //    int dist = ChallengeGameManager.Instance.nodeGap[x + (7 * z)]; // == nodeGap[속의 인수];
                //    //print("count:" + ChallengeGameManager.Instance.saveBlocks.Count + "," + x + "," + z + "," + ChallengeGameManager.Instance.nodeGap[x + (7 * z)]);
                //    if (ChallengeGameManager.Instance.nodeGap[x + (7 * z)] != 0)
                //    {
                //        //if(10f - ChallengeGameManager.Instance.floorFind[x, z] <= 9)
                //        if ((int)ChallengeGameManager.Instance.saveBlocks[i].transform.position.y < 9)
                //        {
                //            print("첫번째");
                //            if (ChallengeGameManager.Instance.blocks[(int)ChallengeGameManager.Instance.saveBlocks[i].transform.position.x, (int)ChallengeGameManager.Instance.saveBlocks[i].transform.position.y + 1, (int)ChallengeGameManager.Instance.saveBlocks[i].transform.position.z] == BlockType2.Empty)
                //            {
                //                print("두번째");
                //                ChallengeGameManager.Instance.saveBlocks[i].transform.DOMoveY(ChallengeGameManager.Instance.saveBlocks[i].transform.position.y + 1, 1.0f);
                //            }
                //        }
                //        //print("x" + x);
                //        //print("z" + z);
                //        //print("nodeGap[" + (x + (7 * z)) + "]" + "dist" + dist + ", floorFind" + ChallengeGameManager.Instance.floorFind[x, z]);
                //    }
                //}
             //   ChallengeGameManager.Instance.boom = false;
            //}
                //if (ChallengeGameManager.Instance.MaxFloorCheck == 0 && ChallengeGameManager.Instance.nowStart == true && comp == true) //전체 같이 한칸 위로
                //{
                //    for (int i = 0; i < ChallengeGameManager.Instance.floor.Count; ++i)
                //    {
                //        //   Debug.Log("step = " + stepIndex);

                //        if (stepIndex[i] < 1)
                //        {
                //            ChallengeGameManager.Instance.floor[i].transform.position = new Vector3(ChallengeGameManager.Instance.floor[i].transform.position.x, ChallengeGameManager.Instance.floor[i].transform.position.y + 0.05f, ChallengeGameManager.Instance.floor[i].transform.position.z);
                //            stepIndex[i] += 0.05f;
                //        }
                //        else
                //        {
                //            ChallengeGameManager.Instance.Init();
                //            ChallengeGameManager.Instance.floor[i].transform.position = new Vector3(Mathf.Round(ChallengeGameManager.Instance.floor[i].transform.position.x), Mathf.Round(ChallengeGameManager.Instance.floor[i].transform.position.y), Mathf.Round(ChallengeGameManager.Instance.floor[i].transform.position.z));
                //            stepIndex[i] = 0;
                //            ChallengeGameManager.Instance.nowStart = false;


                //        }
                //    }
                //}

                }             //adfadfas
            yield return true;
        }
    }

    void FloorSort()
    {
        for (int i = 0; i < ChallengeGameManager.Instance.floor.Count; ++i) //밑에서 하나 채우기     동시에 움직이면 요거 무시
        {
            if (ChallengeGameManager.Instance.floor[i].transform.position.y < 9 - i)
            {
                speed = 20f;
                float dist = Mathf.Abs(ChallengeGameManager.Instance.floor[i].transform.position.y - (9 - i));
                float duration = dist / speed;
                ChallengeGameManager.Instance.floor[i].transform.DOMoveY(9f - i, duration).SetEase(Ease.Linear);
            }
        }
    }
    void FloorEnter()
    {
        //print("floorFind " + ChallengeGameManager.Instance.floorFind);
        //if (floor[i].transform.position.y < 10 - ChallengeGameManager.Instance.floorFind + max)
        //if (ChallengeGameManager.Instance.floor[ChallengeGameManager.Instance.floorFind - 1].transform.position.y < 11f - ChallengeGameManager.Instance.floorFind)
        //{
        float duration;
        float temp = 0;
        speed = 20f;
        for (int j = 0; j < myBlocks.Count; ++j)
        {
            float dist = 18;
            GameObject tempBlock  = myBlocks[j];
            speed -= 2f;
            duration = dist / speed;
            if (duration > temp)
                temp = duration;
            if(ChallengeGameManager.Instance.blocks[(int)tempBlock.transform.position.x, (int)tempBlock.transform.position.y+1, (int)tempBlock.transform.position.x] == BlockType2.Empty)
                tempBlock.transform.DOMoveY(10f - ChallengeGameManager.Instance.floorFind[(int)myBlocks[j].transform.position.x, (int)myBlocks[j].transform.position.z], duration).SetEase(Ease.Linear);//.OnComplete(() => EnterComplete(tempBlock));

            //tempBlock.transform.DOMoveY(10f - ChallengeGameManager.Instance.floorFind[(int)myBlocks[j].transform.position.x, (int)myBlocks[j].transform.position.z], duration).SetEase(Ease.Linear);//.OnComplete(() => EnterComplete(tempBlock));
        }

        StartCoroutine("CheckComplete", temp);
        //}
    }
    void StateInit()
    {
        ChallengeGameManager.Instance.Init();

        //for (int i = 0; i < (int)LengthXZ.Max; ++i)
        //{
        //    for (int j = 0; j < (int)LengthXZ.Max; ++j)
        //    {
        //        if (ChallengeGameManager.Instance.floorFind[i, j] >= 10)
        //        {
        //            ChallengeGameManager.Instance.FailUI.SetActive(true);
        //            ChallengeGameManager.Instance.failCheck = true;

        //        }
        //    }
        //}
        //FloorSort();
        //FloorEnter();
    }
    IEnumerator CheckComplete(float duration)
    {
        yield return new WaitForSeconds(duration);
        //fasdfads

        comp = true;
        myBlocksCount++;
        ChallengeGameManager.Instance.Init();
 //       Debug.Log("asdasd");
    }


    void EnterComplete(GameObject block)
    {
        //ChallengeGameManager.Instance.Init();
        
        comp = true;
        myBlocksCount++;
        ChallengeGameManager.Instance.Init();
        //Debug.Log("*******call*******");
        //Debug.Log("blockY =" + Mathf.Round(block.transform.position.y));

    }

}