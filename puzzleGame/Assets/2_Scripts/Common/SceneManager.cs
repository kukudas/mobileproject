﻿using UnityEngine;
using System.Collections;

public class SceneManager : Singleton<SceneManager> {

    private string _nextSceneName;
    public void SetNextSceneName(string name) { _nextSceneName = name; }
    public string GetNextSceneName() { return _nextSceneName; }

    bool _isFirst = true;
    public void SetIsFirst(bool first) { _isFirst = first; }
    public bool GetIsFirst() { return _isFirst; }
    public int selectStageIndex = -1;

    // Use this for initialization
    void Start () {
        DontDestroyOnLoad(this);
        Application.LoadLevel("1_Intro");
	}
}
