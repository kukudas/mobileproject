﻿using UnityEngine;
using System.Collections;

public class User : Singleton<User> {

    [System.NonSerialized]
    public int level;
    [System.NonSerialized]
    public int gold;
    [System.NonSerialized]
    public int[] Stage = new int[12];
    [System.NonSerialized]
    public string curStage;

    [System.NonSerialized]
    public float[] multiRate = new float[6] { 0f, 0.02f, 0.04f, 0.06f, 0.08f, 0.16f };
    [System.NonSerialized]
    public float[] goldRate = new float[6] { 0f, 0.1f, 0.12f, 0.15f, 0.17f, 0.2f };

    void Start()
    {
        //PlayerPrefs.DeleteKey("UserLevel");   //지우는거
        if(PlayerPrefs.HasKey("UserLevel"))
        {
            level = PlayerPrefs.GetInt("UserLevel");    // 불러오는거
        }
        else
        {
            level = 0;
            PlayerPrefs.SetInt("UserLevel", level); // 저장하는거
        }

        //PlayerPrefs.DeleteKey("UserGold");   //지우는거
        if (PlayerPrefs.HasKey("UserGold"))
        {
            gold = PlayerPrefs.GetInt("UserGold");    // 불러오는거
        }
        else
        {
            gold = 0;
            PlayerPrefs.SetInt("UserGold", gold); // 저장하는거
        }

        //PlayerPrefs.DeleteKey("StageInfo");   //지우는거
        if (PlayerPrefs.HasKey("StageInfo"))
        {
            curStage = PlayerPrefs.GetString("StageInfo");    // 불러오는거
        }
        else
        {
            curStage = "1;0;0;0;0;0;0;0;0;0;0;0";

            PlayerPrefs.SetString("StageInfo", curStage); // 저장하는거
        }
        StageInfoStrToInt();
    }    


    void StageInfoStrToInt()
    {
        string[] temp;
        print(curStage);
        temp=curStage.Split(';');
        for (int i = 0; i < temp.Length; ++i)
        {
            Stage[i] = System.Convert.ToInt32(temp[i]);
        }
    }

    public string StageInfoIntToStr()
    {
        curStage = "";
        for (int i = 0; i < Stage.Length; ++i)
        {
            if (i < Stage.Length - 1)
                curStage += (Stage[i] + ";");
            else
                curStage += Stage[i];

        }
        return curStage;
    }

}
