﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class BlockSave : MonoBehaviour {

    public Transform thisTransform;
   
    private GameManager manager;
    
    public int deathCount;

    public AudioClip destory;
    

    // Use this for initialization
    void Start () {
        manager = GameManager.Instance;
        thisTransform = transform;
        StartCoroutine("updateRoutine");

    }
    IEnumerator updateRoutine()
    {

        while (true)
        {

            if (deathCount == 0)
            {
                manager.blocks[(int)thisTransform.position.x, (int)thisTransform.position.y, (int)thisTransform.position.z] = BlockType.Empty;
                //    Debug.Log(this.transform.position.x + " , " + (int)this.transform.position.z);
                //   Debug.Log("파괴" + manager.blocks[(int)this.transform.position.x, (int)this.transform.position.z]);

                GameManager.Instance.score_position = thisTransform.position;
                //  ScorePlus();//점수없으면 필요없음

                GameManager.Instance.score += 1;

                gameObject.SetActive(false);
                AudioSource.PlayClipAtPoint(destory, new Vector3(0, 0, 0));
                GameManager.Instance.Init();
                Destroy(gameObject);
            }
            yield return null;
        }
    }

    void ScorePlus()
    {
        Text tmp = Instantiate(GameManager.Instance.score_plus);
        tmp.transform.SetParent(GameManager.Instance.canvas);
        //tmp.transform.SetParent(null);

        Vector3 pos = Camera.main.WorldToScreenPoint((GameManager.Instance.score_position) + Vector3.right*2.5f) - new Vector3(Screen.width*0.5f, Screen.height*0.5f,0f);

        tmp.transform.localPosition = pos;
        tmp.transform.localScale = Vector3.one;
        tmp.transform.localEulerAngles = Vector3.zero;
        
        tmp.SendMessage("ScorePlus");

        //tmp.transform.DOLocalMoveY(255f, 5.5f).SetEase(Ease.Linear);
        //tmp.transform.SetParent(GameManager.Instance.canvas);

        //yield return new WaitForSeconds(1f);
        //tmp.transform.SetParent(null);
        
        //for (float a = 1; a >= 0; a -= 0.2f)
        //{
        //    print("alpha : "+a);
        //    tmp.color = new Vector4(0, 0, 0, a);
        //    yield return new WaitForSeconds(1f);
        //}

        // tmp.transform.SetParent(null);

        //Destroy(tmp.gameObject);
    }
}
