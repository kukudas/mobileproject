﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

using System.Collections.Generic;
using UnityEngine.EventSystems;

public class Loading : MonoBehaviour {

    public GameObject loadingtext;
    public Image[] stageBackground;	

	void Start () {
        if(SceneManager.Instance.GetNextSceneName() != "1_Intro")
        {
            StartCoroutine("LoadingScene");
        }
        else
        {
            StartCoroutine("LoadingSceneToIntro");
        }
	}

    IEnumerator LoadingScene()
    {
        loadingtext.GetComponent<CanvasGroup>().alpha = 0;
        loadingtext.GetComponent<CanvasGroup>().DOFade(1f, 1f);
        yield return new WaitForSeconds(2f);
        loadingtext.GetComponent<CanvasGroup>().DOFade(0f, 1f);
        print("SceneManager.Instance.selectStageIndex"+ SceneManager.Instance.selectStageIndex);
        yield return new WaitForSeconds(1f);
        int index = SceneManager.Instance.selectStageIndex - 1;
        if (index < 0) index = 12;
        stageBackground[index].gameObject.SetActive(true);
        stageBackground[index].DOFade(1f, 1f);
        yield return new WaitForSeconds(1f);
        Application.LoadLevel(SceneManager.Instance.GetNextSceneName());
    }
    IEnumerator LoadingSceneToIntro()
    {
        int index = SceneManager.Instance.selectStageIndex - 1;
        if (index < 0) index = 12;
        stageBackground[index].gameObject.SetActive(true);
        stageBackground[index].color = Color.white;
        stageBackground[index].DOFade(0f, 1f);
        yield return new WaitForSeconds(1f);
        loadingtext.GetComponent<CanvasGroup>().alpha = 0;
        loadingtext.GetComponent<CanvasGroup>().DOFade(1f, 1f);
        yield return new WaitForSeconds(2f);
        loadingtext.GetComponent<CanvasGroup>().DOFade(0f, 1f);
        yield return new WaitForSeconds(1f);
        Application.LoadLevel(SceneManager.Instance.GetNextSceneName());
    }
}
