using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Threading;
using DG.Tweening;

public class InstBlockMap : MonoBehaviour
{
    public GameObject[] blocks;
    public int Stage1;
    public int Stage2;
    public int Stage3;
    public int Stage4;
    public int StageAfter4;

    List<GameObject> myBlocks = new List<GameObject>();
    private int myBlocksCount;
    private float selfTimer;
    private float startTimer;
    private bool initCheck = false;
    private float speed;
    private int randomCount;
    private float[] stepIndex = new float[10000]; // 1 스텝 가는 인덱스
    private int[] compCheck = new int[36];
    //    private bool 
    private int maxY;
    private bool comp;

    // test 변수
    private float max = 1;

    private int gridMax = 9;

    public Transform lastFloor;

    // Use this for initialization
    void Start()
    {
        startTimer = Time.time;
        selfTimer = Time.time + 0.7f;
        maxY = 4;
        for (int i = 0; i < stepIndex.Length; ++i)
            stepIndex[i] = 0;

        StartCoroutine("MyUpdate");
    }

    IEnumerator MyUpdate()
    {
        while(true)
        {
            if (ChallengeGameManager.Instance.failCheck == false)
            {
                if (selfTimer - startTimer < 20) // 20초까진 2개
                {
                    gridMax = 9;
                    if (selfTimer < Time.time)
                    {
                        comp = false;
                        myBlocks.Clear();



                        // floor로 관리 X 
                        GameObject tempParent = new GameObject();
                        tempParent.transform.position = Vector3.zero;
                        tempParent.name = "Floor";
                        ChallengeGameManager.Instance.floor.Add(tempParent);
                        lastFloor = tempParent.transform;
                        for (int i = 0; i < 3; ++i)
                        {
                            for (int k = 0; k < 3; ++k)
                            {
                                randomCount = Random.Range(0, 4);

                                Stage1 = Random.Range(0, 2);
                                if (randomCount < 3)
                                {
                                    blocks[Stage1].transform.position = new Vector3(i, 0, k);

                                    GameObject tempObj = Instantiate<Object>(blocks[Stage1]) as GameObject;
                                    myBlocks.Add(tempObj);
                                    //if (randomCount == 3)
                                    //    Destroy(tempObj);

                                }
                                //tempObj.transform.SetParent(tempParent.transform);
                            }
                        }
                        ///////////////// floorfind 맞추기 
                        ChallengeGameManager.Instance.Init();
                        if (ChallengeGameManager.Instance.floorFind >= 10)
                        {
                            ChallengeGameManager.Instance.FailUI.SetActive(true);
                            ChallengeGameManager.Instance.failCheck = true;

                        }
                        //FloorSort();
                        FloorEnter();
                        selfTimer = Time.time + 5.0f;
                    }
                }


                else if (selfTimer - startTimer >= 20 && selfTimer - startTimer < 40)
                {
                    gridMax = 16;
                    if (selfTimer < Time.time)
                    {
                        comp = false;
                        myBlocks.Clear();

                        GameObject tempParent = new GameObject();
                        tempParent.transform.position = Vector3.zero;
                        tempParent.name = "Floor";
                        ChallengeGameManager.Instance.floor.Add(tempParent);
                        lastFloor = tempParent.transform;
                        for (int i = 0; i < 4; ++i)
                        {
                            for (int k = 0; k < 4; ++k)
                            {
                                randomCount = Random.Range(0, 4);

                                Stage2 = Random.Range(0, 3);
                                blocks[Stage2].transform.position = new Vector3(i, 0, k);
                                GameObject tempObj = Instantiate<Object>(blocks[Stage2]) as GameObject;
                                print("생성함ㅁ");
                                if (randomCount == 3)
                                    Destroy(tempObj);

                                myBlocks.Add(tempObj);

                                //tempObj.transform.SetParent(tempParent.transform);
                            }
                        }
                        ChallengeGameManager.Instance.Init();
                        if (ChallengeGameManager.Instance.floorFind >= 10)
                        {
                            ChallengeGameManager.Instance.FailUI.SetActive(true);
                            ChallengeGameManager.Instance.failCheck = true;

                        }
                        //FloorSort();
                        FloorEnter();
                        //카메라 사이즈 up
                        //ChallengeGameManager.Instance.zlengthFind = 4;
                        //ChallengeGameManager.Instance.xlengthFind = 4;
                        selfTimer = Time.time + 5.0f;
                    }
                }
                else if (selfTimer - startTimer >= 40 && selfTimer - startTimer < 60)
                {
                    gridMax = 25;
                    if (selfTimer < Time.time)
                    {
                        GameObject tempParent = new GameObject();
                        tempParent.transform.position = Vector3.zero;
                        tempParent.name = "Floor";
                        ChallengeGameManager.Instance.floor.Add(tempParent);
                        lastFloor = tempParent.transform;
                        for (int i = 0; i < 5; ++i)
                        {
                            for (int k = 0; k < 5; ++k)
                            {
                                Stage3 = Random.Range(0, 4);
                                blocks[Stage3].transform.position = new Vector3(i, 0, k);
                                GameObject tempObj = Instantiate<Object>(blocks[Stage3]) as GameObject;
                                //tempObj.transform.SetParent(tempParent.transform);
                            }
                        }
                        ChallengeGameManager.Instance.Init();
                        if (ChallengeGameManager.Instance.floorFind >= 10)
                        {
                            ChallengeGameManager.Instance.FailUI.SetActive(true);
                            ChallengeGameManager.Instance.failCheck = true;

                        }
                        //FloorSort();
                        FloorEnter();
                        //카메라 사이즈 up
                        //ChallengeGameManager.Instance.zlengthFind = 5;
                        //ChallengeGameManager.Instance.xlengthFind = 5;
                        selfTimer = Time.time + 5.0f;
                    }
                }
                else if (selfTimer - startTimer >= 60 && selfTimer - startTimer < 80)
                {
                    gridMax = 36;
                    Debug.Log("60~80");
                    if (selfTimer < Time.time)
                    {
                        GameObject tempParent = new GameObject();
                        tempParent.transform.position = Vector3.zero;
                        tempParent.name = "Floor";
                        ChallengeGameManager.Instance.floor.Add(tempParent);
                        lastFloor = tempParent.transform;
                        for (int i = 0; i < 6; ++i)
                        {
                            for (int k = 0; k < 6; ++k)
                            {
                                Stage4 = Random.Range(0, 5);
                                blocks[Stage4].transform.position = new Vector3(i, 0, k);
                                GameObject tempObj = Instantiate<Object>(blocks[Stage4]) as GameObject;
                                //tempObj.transform.SetParent(tempParent.transform);
                            }
                        }
                        ChallengeGameManager.Instance.Init();
                        if (ChallengeGameManager.Instance.floorFind >= 10)
                        {
                            ChallengeGameManager.Instance.FailUI.SetActive(true);
                            ChallengeGameManager.Instance.failCheck = true;
                        }
                        //FloorSort();
                        FloorEnter();
                        //카메라 사이즈 up
                        //ChallengeGameManager.Instance.zlengthFind = 6;
                        //ChallengeGameManager.Instance.xlengthFind = 6;
                        selfTimer = Time.time + 5.0f;
                    }
                }
                else if (selfTimer - startTimer >= 80 && selfTimer - startTimer < 200)
                {
                    gridMax = 49;
                    if (selfTimer < Time.time)
                    {
                        GameObject tempParent = new GameObject();
                        tempParent.transform.position = Vector3.zero;
                        tempParent.name = "Floor";
                        ChallengeGameManager.Instance.floor.Add(tempParent);
                        lastFloor = tempParent.transform;
                        for (int i = 0; i < 7; ++i)
                        {
                            for (int k = 0; k < 7; ++k)
                            {
                                StageAfter4 = Random.Range(0, 5);
                                blocks[StageAfter4].transform.position = new Vector3(i, 0, k);
                                GameObject tempObj = Instantiate<Object>(blocks[StageAfter4]) as GameObject;
                                //tempObj.transform.SetParent(tempParent.transform);
                            }
                        }
                        ChallengeGameManager.Instance.Init();
                        if (ChallengeGameManager.Instance.floorFind >= 10)
                        {
                            ChallengeGameManager.Instance.FailUI.SetActive(true);
                            ChallengeGameManager.Instance.failCheck = true;

                        }
                        // FloorSort();
                        FloorEnter();
                        //카메라 사이즈 up
                        //ChallengeGameManager.Instance.zlengthFind = 7;
                        //ChallengeGameManager.Instance.xlengthFind = 7;
                        selfTimer = Time.time + 5.0f;
                    }
                }

                /// 내일 할 일 ----> 각 각 하나로 처리 
                //for(int i = 0; i < 8; ++i)
                //{
                //    for(int j = 0; j < 8; ++j)
                //    {

                //    }
                //}
                if (ChallengeGameManager.Instance.MaxFloorCheck == 0 && ChallengeGameManager.Instance.nowStart == true && comp == true) //전체 같이 한칸 위로
                {
                    for (int i = 0; i < ChallengeGameManager.Instance.floor.Count; ++i)
                    {
                        //   Debug.Log("step = " + stepIndex);

                        if (stepIndex[i] < 1)
                        {
                            ChallengeGameManager.Instance.floor[i].transform.position = new Vector3(ChallengeGameManager.Instance.floor[i].transform.position.x, ChallengeGameManager.Instance.floor[i].transform.position.y + 0.05f, ChallengeGameManager.Instance.floor[i].transform.position.z);
                            stepIndex[i] += 0.05f;
                        }
                        else
                        {
                            ChallengeGameManager.Instance.Init();
                            ChallengeGameManager.Instance.floor[i].transform.position = new Vector3(Mathf.Round(ChallengeGameManager.Instance.floor[i].transform.position.x), Mathf.Round(ChallengeGameManager.Instance.floor[i].transform.position.y), Mathf.Round(ChallengeGameManager.Instance.floor[i].transform.position.z));
                            stepIndex[i] = 0;
                            ChallengeGameManager.Instance.nowStart = false;


                        }
                    }
                    //for (int i = 0; i < myBlocks.Count; ++i)
                    //{
                    //    if (stepIndex[i] < 1)
                    //    {
                    //        myBlocks[i].transform.position = new Vector3(myBlocks[i].transform.position.x, myBlocks[i].transform.position.y + 0.05f, myBlocks[i].transform.position.z);
                    //        stepIndex[i] += 0.05f;
                    //    }
                    //    else
                    //    {
                    //        ChallengeGameManager.Instance.Init();
                    //        myBlocks[i].transform.position = new Vector3(Mathf.Round(myBlocks[i].transform.position.x), Mathf.Round(myBlocks[i].transform.position.y), Mathf.Round(myBlocks[i].transform.position.z));
                    //        stepIndex[i] = 0;
                    //        ChallengeGameManager.Instance.nowStart = false;
                    //    }
                    //}
                }

            }             //adfadfas
                yield return true;
        }
    }

    void FloorSort()
    {
        for (int i = 0; i < ChallengeGameManager.Instance.floor.Count; ++i) //밑에서 하나 채우기     동시에 움직이면 요거 무시
        {
            if (ChallengeGameManager.Instance.floor[i].transform.position.y < 9 - i)
            {
                speed = 20f;
                float dist = Mathf.Abs(ChallengeGameManager.Instance.floor[i].transform.position.y - (9 - i));
                float duration = dist / speed;
                ChallengeGameManager.Instance.floor[i].transform.DOMoveY(9f - i, duration).SetEase(Ease.Linear);
            }
        }
    }
    void FloorEnter()
    {
        //print("floorFind " + ChallengeGameManager.Instance.floorFind);
        //if (floor[i].transform.position.y < 10 - ChallengeGameManager.Instance.floorFind + max)
        //if (ChallengeGameManager.Instance.floor[ChallengeGameManager.Instance.floorFind - 1].transform.position.y < 11f - ChallengeGameManager.Instance.floorFind)
        //{
        float duration;
        float temp = 0;
        speed = 20f;
        float dist = Mathf.Abs(ChallengeGameManager.Instance.floor[ChallengeGameManager.Instance.floorFind - 1].transform.localPosition.y - (10f - ChallengeGameManager.Instance.floorFind));
        for (int j = 0; j < myBlocks.Count; ++j)
        {
            GameObject tempBlock  = myBlocks[j];
            speed -= 2f;
            duration = dist / speed;
            if (duration > temp)
                temp = duration;

            tempBlock.transform.SetParent(lastFloor);
            tempBlock.transform.DOMoveY(10f - ChallengeGameManager.Instance.floorFind, duration).SetEase(Ease.Linear);//.OnComplete(() => EnterComplete(tempBlock));
        }

        StartCoroutine("CheckComplete", temp);
        //}
    }

    IEnumerator CheckComplete(float duration)
    {
        yield return new WaitForSeconds(duration);
        //fasdfads

        comp = true;
        myBlocksCount++;
        ChallengeGameManager.Instance.Init();
 //       Debug.Log("asdasd");
    }


    void EnterComplete(GameObject block)
    {
        //ChallengeGameManager.Instance.Init();
        
        comp = true;
        block.transform.SetParent(lastFloor);
        myBlocksCount++;
        ChallengeGameManager.Instance.Init();
        //Debug.Log("*******call*******");
        //Debug.Log("blockY =" + Mathf.Round(block.transform.position.y));

    }

}